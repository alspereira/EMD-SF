import java.util.Calendar;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;
import emdsf.Core.DataAcquisition.LabJack.U6DataAcquisition;
import emdsf.Core.DataAcquisition.LabJack.U6StreamerWaitMode;
import emdsf.Core.PowerComputation.PowerComputationFreq;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.calculator.PowerCalculator;
import emdsf.Core.util.DAQSampleAdapter;
import util.PublishSubscribe;
import util.database.jdbc.dao.PowerSampleDAO;
import util.file.emddf.Writer;

public class SustDataED2 {

	// DAQ AND FILE WRITER VARIABLES
	static String 	path_prefix 			= "D:/SustDataED2/";
	static int[] 	channels 				= {0,2};
	static double 	scanRate				= 12800;
	static int		bitsPerSample			= 16;
	static long		maxFileSizeInSamples	= (long) (3600 * scanRate); // 1 hour files (number of seconds * samples per second)	
	static int		streamerQueueSize		= 120; 						// 10 seconds
	static int 		daqQueueSize			= (int) (scanRate * 10); 	// also 10 seconds
	static float[]	calibrationConstants	= {1f, 1.75f}; // SQRT(2) * 1V RMSs -> 1.414 + 20% margin
	
	static U6StreamerWaitMode	U6Streamer;
	static U6DataAcquisition	U6DataAcquisition;
	static Writer				emddfWriter;
	
	static ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> daqSamples;
	
	// RAW DATA - PUBLISH AND SUBSCRIBE VARIABLES
	static PublishSubscribe<DAQSampleDTO<float[]>> 				rawDataPubSub;
	static ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> 	samplesToWriteRaw;
	static ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> 	samplesToCalculatePower;
	
	// POWER CALCULATOR VARIABLES
	static float	fundamentalFreq 			= 50;
	static float[] 	sensorCallibrationConstants	= {460, 30};		
	static int[][]	channelCombinations			= new int[1][2];
	static int		pcQueueSize					= (int) (fundamentalFreq * 60); // 60 seconds
	static ControlledArrayBlockingQueue<IPowerSample>	powerSamples;
	
	static DAQSampleAdapter adapter;
	static PowerComputationFreq powerComputation;
	
	// POWER DATA - PUBLISH AND SUBSCRIBE VARIABLES
	static ControlledArrayBlockingQueue<IPowerSample> 	samplesToStore;
	static ControlledArrayBlockingQueue<IPowerSample> 	samplesToDetectEvents;
	
	static PublishSubscribe<IPowerSample>				powerSamplePubSub;
	
	// STORE POWER DATA VARIABLES
	static int samplesToAverage = 3000;
	static StorePower storePower;
	
	public static void main(String[] args) {
		// read from DAQ
		U6Streamer 			= new U6StreamerWaitMode(channels, scanRate, streamerQueueSize);
		U6DataAcquisition 	= new U6DataAcquisition(U6Streamer, daqQueueSize);
		
		// publish and subscribe raw data
		rawDataPubSub		= new PublishSubscribe<DAQSampleDTO<float[]>>(U6DataAcquisition.getDAQSamples_OUT());
		rawDataPubSub.subscribe(samplesToWriteRaw);
		rawDataPubSub.subscribe(samplesToCalculatePower);
		
		// write to file
		emddfWriter 		= new Writer(samplesToWriteRaw, scanRate, bitsPerSample, 
											channels.length, maxFileSizeInSamples, calibrationConstants);
		emddfWriter.path_prefix = path_prefix;
		//SURFWriter.setUnixTimestamp(Calendar.getInstance().getTimeInMillis()); do this when acquisition starts
		
		// calculate power
		adapter = new DAQSampleAdapter(samplesToCalculatePower, channels.length, daqQueueSize);
		
		channelCombinations[0][0] = 1;
		channelCombinations[0][1] = 2;
		
		// TODO: change all the timestamp related issues. must compute time with timestamp given by the DAQSampleDTO
		powerComputation = new PowerComputationFreq(adapter.getSamples(), (int) scanRate, 
													(int) fundamentalFreq, channelCombinations, pcQueueSize);
		
		// publish and subscribe
		powerSamplePubSub = new PublishSubscribe<IPowerSample>(powerComputation.getPowerSamples(0));
		powerSamplePubSub.subscribe(samplesToStore);
		//powerSamplePubSub.subscribe(samplesToDetectEvents);
		
		// store power
		storePower = new StorePower(samplesToAverage);
		storePower.samples_in = samplesToStore;
		
		// detect and store events
		
		
		// get all this up and running... right after fixing the 1001 bugs!!
		
		
	}
}
class StorePower implements Runnable {
	int samples = 3000; // 1 minute
	ControlledArrayBlockingQueue<IPowerSample> samples_in;
	PowerSampleDAO psDAO = new PowerSampleDAO();
	
	public StorePower(int a_samples) {
		this.samples = a_samples;
	}
	
	@Override
	public void run() {
		double avg_I = 0;
		double avg_V = 0;
		double avg_P = 0;
		double avg_Q = 0;
		long ts = 0;
		IPowerSample ps = null;
		int step = 0;
		
		while(true) {
			while(step < samples) {
				ps = samples_in.controlledPoll();
				if(ps != null) {
					avg_I += ps.getIRMS();
					avg_V += ps.getVRMS();
					avg_P += ps.getRealPower();
					avg_Q += ps.getReactivePower();
					step ++;
				}
			}
			ts = ps.getUnixTimestamp();
			avg_I = avg_I / samples;
			avg_V = avg_V / samples;
			avg_P = avg_P / samples;
			avg_Q = avg_Q / samples;
			
			// store this shit
			psDAO.create(ts, avg_P, avg_Q, avg_V, avg_I);
			// reset everything
			step = 0;
			avg_I = 0;
			avg_V = 0;
			avg_P = 0;
			avg_Q = 0;
		}
	}
}
