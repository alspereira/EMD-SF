package emdsf.Core.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;

public class DAQSampleAdapter implements Runnable {

	private ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> source;
	private Map<Integer,ControlledArrayBlockingQueue<Float>> destination;
	private int numChannels;
	
	public DAQSampleAdapter(ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> a_source, 
							int a_numChannels, int a_queueSize) {
		source = a_source;
		numChannels = a_numChannels;
		destination = new ConcurrentHashMap<Integer,ControlledArrayBlockingQueue<Float>>();
		for(int i = 1; i <= numChannels; i++)
			destination.put(i, new ControlledArrayBlockingQueue<Float>(a_queueSize));
	}
	
	public Map<Integer,ControlledArrayBlockingQueue<Float>> getSamples() {
		return this.destination;
	}

	@Override
	public void run() {
		DAQSampleDTO<float[]> 	daqSample;
		float[]					daqSampleData;
		while(true) {
			try {
				daqSample 		= source.take();
				daqSampleData 	= (float[]) daqSample.getSample();
				for(int i = 0; i < numChannels; i++) {
					destination.get(i + 1).put(daqSampleData[i]);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
