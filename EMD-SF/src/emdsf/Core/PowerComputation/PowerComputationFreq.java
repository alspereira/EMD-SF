package emdsf.Core.PowerComputation;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.jtransforms.fft.FloatFFT_1D;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerSampleDTO;

public class PowerComputationFreq extends APowerComputation implements Runnable {
	//private int samplingFreq;
	private int fundamentalFreq;
	private int channels;
	private int fftWindowSize;
	
	private Map<Integer, ControlledArrayBlockingQueue<Float>> source;
	private Map<Integer, ControlledArrayBlockingQueue<IPowerSample>> destination;
	private int	queueSize = 36000; // 10 seconds assuming a 60 Hz fundamental frequency
	private Map<Integer, float[]> fftWindows;
	private Map<Integer, float[]> fftBins;
	
	private int[][] channelCombinations;
	private int[] harmonicsRequested;
	
	//private boolean isRunning = false;
	private boolean mustQuit = false;
	
	private long sampleUnixTimestamp;
	
	public PowerComputationFreq(Map<Integer,ControlledArrayBlockingQueue<Float>> a_source, 
			int a_samplingFreq, int a_fundamentalFreq, int[][] a_channelCombinations, int a_queueSize) {
		this(a_source, Calendar.getInstance().getTimeInMillis(), a_samplingFreq, 
				a_fundamentalFreq, a_channelCombinations, a_queueSize);
	}

	public PowerComputationFreq(Map<Integer,ControlledArrayBlockingQueue<Float>> a_source, long a_initialUnixTimestamp, 
			int a_samplingFreq, int a_fundamentalFreq, int[][] a_channelCombinations, int a_queueSize) {
		source 				= a_source;
		sampleUnixTimestamp = a_initialUnixTimestamp;
		//samplingFreq	 	= a_samplingFreq;
		fundamentalFreq	 	= a_fundamentalFreq;
		channelCombinations = a_channelCombinations;
		queueSize			= a_queueSize;
		fftWindowSize 		= a_samplingFreq / a_fundamentalFreq;
		channels 			= a_source.size();
		
		fftWindows 			= new HashMap<Integer, float[]>(channels);
		fftBins 			= new HashMap<Integer, float[]>(channels);		
		
		for (int i = 0; i < channels; i++) {
			fftWindows.put(i, new float[fftWindowSize]);
			fftBins.put(i, new float[fftWindowSize / 2]);
		}
		
		destination = new HashMap<Integer, ControlledArrayBlockingQueue<IPowerSample>>(channels);
		for(int i = 0; i < channelCombinations.length; i ++)
			destination.put(i, new ControlledArrayBlockingQueue<IPowerSample>(queueSize));
	}
	
	public void setInitialTimestamp(long a_timestamp) {
		this.sampleUnixTimestamp = a_timestamp;
	}
	
	public ControlledArrayBlockingQueue<IPowerSample> getPowerSamples(int key) {
		return this.destination.get(key);
	}
	
	private void updateFFTWindow() throws InterruptedException {
		for(int i = 0; i < fftWindowSize; i++) {
			for(int j = 0; j < channels; j++) {
				fftWindows.get(j)[i] = source.get(j).take();
			}
		}
	}
	
	private double getAngle(double re, double imag) {
		return Math.atan2(imag, re);
	}
	
	// TODO: find a better way to add icc and vcc
	private void calculatePower() {
		double vRMS_total = getRMS(fftWindows.get(0));
		double iRMS_total = getRMS(fftWindows.get(1));
		
		FloatFFT_1D fft;
		fft = new FloatFFT_1D(fftWindowSize);
		
		for(int i = 0; i < channels; i ++) {
			fft.realForward(fftWindows.get(i));
		}
		
		for(int j = 0; j < channels; j ++) {
			fftBins.get(j)[0] = fftWindows.get(j)[0];
			for(int k = 1; k < fftWindowSize / 2; k ++) {
				fftBins.get(j)[k] = (float) (Math.sqrt(Math.pow(fftWindows.get(j)[2 * k], 2) + Math.pow(fftWindows.get(j)[2 * k + 1], 2)) / (fftWindowSize / 2));
			}
		}
		
		float vRMS, iRMS, vAngle, iAngle, phaseDiff;
		int v, i;
		PowerSampleDTO ps;
		
    	double icc = 1;
    	double vcc = 1;
    	
		// We assume Voltage is always in the first position
    	vRMS_total = vRMS_total * vcc;
		iRMS_total = iRMS_total * icc;
		
		for(int l = 0; l < channelCombinations.length; l ++) {
			v 			= channelCombinations[l][0];
			i 			= channelCombinations[l][1];
			
			vRMS 		= (float) ((float) (fftBins.get(v)[1] /  Math.sqrt(2)) * vcc);
			vAngle		= (float) getAngle(fftWindows.get(v)[2],fftWindows.get(v)[3]);
			iRMS 		= (float) ((float) (fftBins.get(i)[1] /  Math.sqrt(2)) * icc);
			iAngle 		= (float) getAngle(fftWindows.get(i)[2],fftWindows.get(i)[3]);
			phaseDiff 	= iAngle - vAngle;
				
			ps = new PowerSampleDTO(sampleUnixTimestamp, iRMS, vRMS, 
									(float) (iRMS*vRMS*Math.cos(phaseDiff)), 
									(float) (iRMS*vRMS*Math.sin(phaseDiff)), 
									(float) Math.cos(phaseDiff), null);
			
			ps.iRMS_total = (float) iRMS_total;
			ps.vRMS_total = (float) vRMS_total;
			
			try {
				destination.get(l).put(ps);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		sampleUnixTimestamp += (float)(1 / fundamentalFreq) * 1000;
	}

	@Override
	public void run() {
		//isRunning = true;
		while(mustQuit == false) {
			try {
				updateFFTWindow();
				calculatePower();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// not necessary because at this stage the thread will die. TODO: verify!!
		//isRunning = false;
		//mustQuit = false;
	}

}
