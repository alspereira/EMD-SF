package emdsf.Core.PowerComputation;

public abstract class APowerComputation {

	public APowerComputation() {
	}
	
	public static double getRMS(float[] raw) {
		double sum = 0;
    	if (raw.length==0) {
    		return sum;
        } 
    	else {  	
            for (int ii=0; ii<raw.length; ii++) {
            	sum += raw[ii];
            }
        }
        double average = sum/raw.length;

        double[] meanSquare = new double[raw.length];
        double sumMeanSquare = 0;
        for (int ii=0; ii<raw.length; ii++) {
        	sumMeanSquare += Math.pow(raw[ii]-average,2);
        	meanSquare[ii] = sumMeanSquare;
        }
        double averageMeanSquare = sumMeanSquare/raw.length;
        double rootMeanSquare = Math.pow(averageMeanSquare,0.5);
        return rootMeanSquare;
	}

}
