package emdsf.Core.PowerComputation.DTO;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;

public class PC_DrivingPower implements Runnable {
	
	private ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> daqSamples_IN;
	private ControlledArrayBlockingQueue<IPowerSample> powerSamples_OUT;
	
	float[] callibration;
	
	public long timestamp = 0l;
	public float sampleRate;
	

	public PC_DrivingPower(int queueSize, float[] callibration, float sampleRate, ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> daqSamples_IN) {
		powerSamples_OUT = new ControlledArrayBlockingQueue<>(queueSize);
		this.callibration = callibration;
		this.sampleRate = sampleRate;
		this.daqSamples_IN = daqSamples_IN;
	}
	/*public PC_DrivingPower(int queueSize, float[] callibration, float sampleRate, ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> daqSamples_IN) {
		this(queueSize, callibration, daqSamples_IN);
		this.sampleRate = sampleRate;
		//this.callibration = callibration;
		//this.daqSamples_IN = daqSamples_IN;
	}*/
	
	public ControlledArrayBlockingQueue<IPowerSample> getPowerSamplesQueue() {
		return this.powerSamples_OUT;
	}
	
	// if the input queue is empty (meaning that all elements were processed, it is possible to quit
	public boolean canQuit() {
		return (this.daqSamples_IN.size() == 0 && canQuit == true);
	}
	
	public volatile boolean canQuit = false;

	@Override
	public void run() {
		// TODO Auto-generated method stub
		DAQSampleDTO<float[]> ds;
		PowerSampleDTO ps;
		long count = 0l;
		float d = 1/(float)sampleRate;
		//System.out.println(sampleRate);
		//System.out.println(d);
		
		while(canQuit() == false) {
			//System.out.println(((long)(d*1000)));
			ds = daqSamples_IN.controlledPoll();
			if(ds!= null) {
				//ps = new PowerSampleDTO(timestamp = timestamp + (long) ((d)*1000), ds.getSample()[0]*callibration[0], 
				//		ds.getSample()[1]*callibration[1]);
				
				ps = new PowerSampleDTO(
						timestamp = timestamp + (long) ((d)*1000), 
						ds.getSample()[0]*callibration[0], 
						ds.getSample()[1]*callibration[1]
						/*,ds.getSample()[2]*callibration[2],
						ds.getSample()[3]*callibration[3]*/ 
					);
				//ps.vRMS_total = ds.getSample()[4]*callibration[4];
				//ps.iRMS_total = ds.getSample()[5]*callibration[5];
				
				ps.id = ++count;
				//System.out.println("timestamp: " + ps.getUnixTimestamp() + "   " + timestamp);
				//System.exit(-1);
				//System.out.println(timestamp);
				//System.out.println(sampleRate);
				
				//System.out.println(d);
				try {
					powerSamples_OUT.put(ps);
					//System.out.println(ps.getUnixTimestamp() + " " + ps.getRealPower());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}	
		}
		//this.daqSamples_IN.mustQuit = true;
		//this.powerSamples_OUT.mustQuit = true;
		this.powerSamples_OUT.quit();
		System.out.println("gracefully quiting " + this.getClass().getName());
	}

}
