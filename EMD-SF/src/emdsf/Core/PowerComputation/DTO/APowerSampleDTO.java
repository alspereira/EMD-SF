package emdsf.Core.PowerComputation.DTO;

import java.util.HashMap;
import java.util.Map;

@FunctionalInterface
interface Command {
    float getValue();
}

public abstract class APowerSampleDTO implements IPowerSample {
	
	private long id;
	private long unixTimestamp; // until milliseconds
	private Map<String, Float> harmonics;
	public Map<PowerMetricType, Command> powerMetrics = new HashMap<>();
	
	public APowerSampleDTO() {
		updateMap();
	}
	
	private void updateMap() {
		/*powerMetrics.put(PowerMetricType.I_RMS, 			() -> { return this.getIRMS(); });
		powerMetrics.put(PowerMetricType.V_RMS, 			() -> { return this.getVRMS(); });
		powerMetrics.put(PowerMetricType.REAL_POWER, 		() -> { return this.getRealPower(); });
		powerMetrics.put(PowerMetricType.REACTIVE_POWER, 	() -> { return this.getReactivePower(); });
		powerMetrics.put(PowerMetricType.APPARENT_POWER, 	() -> { return this.getApparentPower(); });*/
	}

	@Override
	public long getID() {
		return this.id;
	}

	@Override
	public long getUnixTimestamp() {
		return this.unixTimestamp;
	}

	public double getDateNum() {
		long millis = 86400000;
		long time_reference = 719529; // Matlab datenum	
		double today_time_reference = (double) this.unixTimestamp / (double) millis;
		double dateNum = time_reference + today_time_reference;
		return dateNum;
	}

	public String getDateTime() {
		// TODO Implement a method that returns a formated date time string
		return null;
	}
	
	public float getMetricValue(PowerMetricType e) {
		//return powerMetrics.get(e).getValue();
		return this.getRealPower();
	}

	@Override
	public abstract float getIRMS();

	@Override
	public abstract float getVRMS();

	@Override
	public abstract float getRealPower();

	@Override
	public abstract float getReactivePower();

	@Override
	public abstract float getApparentPower();

	@Override
	public abstract float getPowerFactor();

	@Override
	public Map<String, Float> getHamonics() {
		return this.harmonics;
	}

}
