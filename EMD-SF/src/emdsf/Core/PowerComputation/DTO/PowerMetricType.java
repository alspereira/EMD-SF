package emdsf.Core.PowerComputation.DTO;

public enum PowerMetricType {
	I_RMS,
	V_RMS, 
	REAL_POWER, 
	REACTIVE_POWER, 
	APPARENT_POWER, 
	POWER_FACTOR
}
