package emdsf.Core.PowerComputation.DTO;

import java.util.HashMap;
import java.util.Map;

public class PowerSampleDTO extends APowerSampleDTO {
	
	//private final static String TYPE_SHORT = "SIMPLE";
	//private final static String TYPE_LONG = "FULL";
	
	public long id	   			= 0;
	private long unixTimestamp 	= 0;
	
	private float iRMS = Float.MAX_VALUE;
	private float vRMS = Float.MAX_VALUE;
	private float apparentPower = Float.MAX_VALUE;
	private float realPower;
	private float reactivePower;
	private float powerFactor = Float.MAX_VALUE;
	
	public float iRMS_total;
	public float vRMS_total;
	
	private Map<String, Float> harmonics = new HashMap<String, Float>();
	
	//private String TYPE;
	
	public PowerSampleDTO(long unixTimestamp, float iRMS, float vRMS, float realPower, float reactivePower, float powerFactor, Map<String, Float> harmonics) {
		super();
		this.unixTimestamp = unixTimestamp;
		this.iRMS = iRMS;
		this.vRMS = vRMS;
		this.apparentPower = iRMS * vRMS;
		this.realPower = realPower;
		this.reactivePower = reactivePower;
		this.powerFactor = powerFactor;
		this.harmonics = harmonics;
	}
	
	public PowerSampleDTO(long unixTimestamp, float realPower, float reactivePower) {	
		super();
		this.unixTimestamp = unixTimestamp;
		this.realPower = realPower;
		this.reactivePower = reactivePower;
	}
	
	public PowerSampleDTO(long unixTimestamp, float realPower, float reactivePower, float vRMS, float iRMS) {	
		super();
		this.unixTimestamp 	= unixTimestamp;
		this.realPower 		= realPower;
		this.reactivePower 	= reactivePower;
		this.vRMS 			= vRMS;
		this.iRMS 			= iRMS;
	}
	
	@Override
	public long getID() {
		return this.id;
	}

	@Override
	public long getUnixTimestamp() {
		return this.unixTimestamp;
	}

	@Override
	public float getIRMS() {
		return this.iRMS;
	}

	@Override
	public float getVRMS() {
		return this.vRMS;
	}
	
	@Override
	public float getApparentPower() {
		return this.apparentPower;
	}

	@Override
	public float getRealPower() {
		return this.realPower;
	}

	@Override
	public float getReactivePower() {
		return this.reactivePower;
	}

	@Override
	public float getPowerFactor() {
		return this.powerFactor;
	}

	@Override
	public Map<String, Float> getHamonics() {
		return this.harmonics;
	}

	@Override
	public float getIRMSTotal() {
		return this.iRMS_total;
	}

	@Override
	public float getVRMSTotal() {
		return this.vRMS_total;
	}
}
