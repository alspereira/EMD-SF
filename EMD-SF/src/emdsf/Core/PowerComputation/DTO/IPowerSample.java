package emdsf.Core.PowerComputation.DTO;

import java.util.Map;

public interface IPowerSample {
	public long getID();
	
	public long getUnixTimestamp();
	
	//public double getDateNum(); // this is the Matlab datenum 
	//public String getDateTime();
	
	public float getIRMS();
	public float getVRMS();
	public float getRealPower();
	public float getReactivePower();
	public float getApparentPower();
	public float getPowerFactor();
	
	public float getIRMSTotal();
	public float getVRMSTotal();
	
	public Map<String, Float> getHamonics();

	public float getMetricValue(PowerMetricType metric);
}
