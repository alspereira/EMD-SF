package emdsf.Core.PowerComputation.calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathArrays;
import org.jtransforms.fft.DoubleFFT_1D;
import org.jtransforms.fft.FloatFFT_1D;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerSampleDTO;

public class PowerCalculator implements Runnable {

	private int samplingFrequency;
	private int fundamentalFrequency;
	private int channels;
	private int fftWindowSize;
	
	private Map<Integer, ArrayBlockingQueue<Float>> source;
	private Map<Integer, float[]> fftWindows;
	private Map<Integer, float[]> fftBins;
	private Map<Integer, ArrayBlockingQueue<IPowerSample>> destination;
	
	private int[][] channelCombinations;
	private int[] harmonicsRequested;
	
	private boolean isRunning = false;
	private boolean mustQuit = false;
	
	private long sampleUnixTimestamp;
	
	public PowerCalculator() {
		
	}
	
	public PowerCalculator(Map<Integer,ArrayBlockingQueue<Float>> source, long initialUnixTimestamp, int fs, int ff, int[][] channelCombinations, int[] harmonicsRequested) {
		this(source, initialUnixTimestamp, fs, ff, channelCombinations);
		this.harmonicsRequested = harmonicsRequested;
	}
	
	public PowerCalculator(Map<Integer,ArrayBlockingQueue<Float>> source, long initialUnixTimestamp, int fs, int ff, int[][] channelCombinations) {
		this(source, initialUnixTimestamp, fs, ff);
		this.channelCombinations = channelCombinations;
	}
	
	private PowerCalculator(Map<Integer,ArrayBlockingQueue<Float>> source, long initialUnixTimestamp, int fs, int ff) {
		this.source 				= source;
		this.sampleUnixTimestamp 	= initialUnixTimestamp;
		this.samplingFrequency 		= fs;
		this.fundamentalFrequency 	= ff;
		fftWindowSize 				= samplingFrequency / fundamentalFrequency;
		channels 					= source.size();
		
		fftWindows 					= new HashMap<Integer, float[]>(channels);
		fftBins 					= new HashMap<Integer, float[]>(channels);		
		for (int i = 0; i < channels; i++) {
			fftWindows.put(i, new float[fftWindowSize]);
			fftBins.put(i, new float[fftWindowSize / 2]);
		}
	}
	
	public double[][] calculatePower(double[] current, double[] voltage, int fs, int ff) {
		int samplesPerPeriod 	= fs / ff;
		int periods 			= current.length / samplesPerPeriod;
		
		double[][] out 			= new double[4][periods];
		
		double[] p_temp;
		int from, to;
		for(int i = 0; i < periods; i++) {
			from = i * samplesPerPeriod;
			to = from + samplesPerPeriod;
			p_temp = calculatePower2(
					MathArrays.copyOfRange(current, from, to),
					MathArrays.copyOfRange(voltage, from, to),
					null,0,0);
			
			out[0][i] = p_temp[0];
			out[1][i] = p_temp[1];
			out[2][i] = p_temp[2];
			out[3][i] = p_temp[3];
		}
		
		return out;
	}
	
	public double[] calculatePower2(double[] current, double[] voltage, int[] harmonicsIndex, int fs, int ff) {
		double icc = 10, vcc = 1;
		double[] out = new double[4];
				
		MathArrays.scaleInPlace(icc, current);
		MathArrays.scaleInPlace(1 / StatUtils.max(voltage), voltage);
		MathArrays.scaleInPlace(110 * Math.sqrt(2), voltage);
		
		DoubleFFT_1D fft = new DoubleFFT_1D(current.length);
		fft.realForward(current);
		
		DoubleFFT_1D fft2 = new DoubleFFT_1D(current.length);
		fft2.realForward(voltage);
		
		double[] absI = new double[current.length / 2];
		double[] absV = new double[current.length / 2];
		
		absI[0] = current[0] / (current.length / 2);
		absV[0] = voltage[0] / (current.length / 2);
		
		for(int i = 1; i < current.length / 2; i++) {
			absI[i] = (float) (Math.sqrt(Math.pow(current[2*i], 2) + Math.pow(current[2*i+1], 2)) / (current.length / 2));			
			absV[i] = (float) (Math.sqrt(Math.pow(voltage[2*i], 2) + Math.pow(voltage[2*i+1], 2)) / (current.length / 2)); 
		}
		
		double S_50Hz = (absI[1] * absV[1]) / 2;
		
		double iRMS_50Hz = (double) (((absI[1] / Math.sqrt(2))));
		double vRMS_50Hz = (double) (((absV[1] / Math.sqrt(2))));
		
		double iAngle_50Hz = (double) getAngle(current[2], current[3]);
		double vAngle_50Hz = (double) getAngle(voltage[2], voltage[3]);

		double phase_diff_50Hz = iAngle_50Hz - vAngle_50Hz;
		
		double p = (double)(iRMS_50Hz*vRMS_50Hz*Math.cos(phase_diff_50Hz));
		double q = (double)(iRMS_50Hz*vRMS_50Hz*Math.sin(phase_diff_50Hz));
		
		
		out[0] = p;
		out[1] = q;
		out[2] = iRMS_50Hz;
		out[3] = vRMS_50Hz;
		
		return out;
	}

	
	public ArrayBlockingQueue<IPowerSample> getDestination(int key) {
		// when calling this, make sure it was already created.
		return destination.get(key);
	}
	
	public void Start(Map<Integer, ArrayBlockingQueue<IPowerSample>> destination) {
		this.destination = destination;
		if(isRunning == false)
			new Thread(this).start();
		else
			System.out.println("This thread is already running.");
	}
	
	public void Start(int queueSize) {
		destination = new HashMap<Integer, ArrayBlockingQueue<IPowerSample>>(channels);
		for(int i = 0; i < channelCombinations.length; i ++)
			destination.put(i, new ArrayBlockingQueue<IPowerSample>(queueSize));
		if(isRunning == false)
			new Thread(this).start();
		else
			System.out.println("This thread is already running.");
	}
	
	// Private methods
	int count = 0;
	
	private void updateFFTWindow() throws InterruptedException {
		for(int i = 0; i < fftWindowSize; i++) {
			for(int j = 0; j < channels; j++) {
				fftWindows.get(j)[i] = source.get(j).take();
			}
		}
	}
	
	private double getAngle(double re, double imag) {
		return Math.atan2(imag, re);
	}

	private void calculatePower() {
		double vRMS_total = getRMS(fftWindows.get(0));
		double iRMS_total = getRMS(fftWindows.get(1));
		
		FloatFFT_1D fft;
		fft = new FloatFFT_1D(fftWindowSize);
		
		// Calculate the FFTs
		for(int i = 0; i < channels; i ++) {
			fft.realForward(fftWindows.get(i));
		}
		
		for(int j = 0; j < channels; j ++) {
			fftBins.get(j)[0] = fftWindows.get(j)[0];
			for(int k = 1; k < fftWindowSize / 2; k ++) {
				fftBins.get(j)[k] = (float) (Math.sqrt(Math.pow(fftWindows.get(j)[2 * k], 2) + Math.pow(fftWindows.get(j)[2 * k + 1], 2)) / (fftWindowSize / 2));
			}
		}
		
		float vRMS, iRMS, vAngle, iAngle, dif;
		int v, i;
		PowerSampleDTO ps;
		
    	double icc = 1;
    	double vcc = 1;
    	
		// We assume Voltage is always in the first position
    	vRMS_total = vRMS_total * vcc;
		iRMS_total = iRMS_total * icc;
		
		for(int l = 0; l < channelCombinations.length; l ++) {
			v = channelCombinations[l][0];
			i = channelCombinations[l][1];
			
			vRMS = (float) ((float) (fftBins.get(v)[1] /  Math.sqrt(2)) * vcc);
			vAngle = (float) getAngle(fftWindows.get(v)[2],fftWindows.get(v)[3]);
			iRMS = (float) ((float) (fftBins.get(i)[1] /  Math.sqrt(2)) * icc);
			iAngle = (float) getAngle(fftWindows.get(i)[2],fftWindows.get(i)[3]);
			dif = iAngle - vAngle;
				
			ps = new PowerSampleDTO(sampleUnixTimestamp, iRMS, vRMS, (float) (iRMS*vRMS*Math.cos(dif)), (float) (iRMS*vRMS*Math.sin(dif)), (float) Math.cos(dif), null);
			
			ps.iRMS_total = (float) iRMS_total;
			ps.vRMS_total = (float) vRMS_total;
			
			try {
				destination.get(l).put(ps);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.sampleUnixTimestamp += (float)(1 / fundamentalFrequency) * 1000;	
	}
	
	public double getRMS(float[] raw) {
		double sum = 0;
    	if (raw.length==0) {
    		return sum;
        } 
    	else {  	
            for (int ii=0; ii<raw.length; ii++) {
            	sum += raw[ii];
            }
        }
        double average = sum/raw.length;

        double[] meanSquare = new double[raw.length];
        double sumMeanSquare = 0;
        for (int ii=0; ii<raw.length; ii++) {
        	sumMeanSquare += Math.pow(raw[ii]-average,2);
        	meanSquare[ii] = sumMeanSquare;
        }
        double averageMeanSquare = sumMeanSquare/raw.length;
        double rootMeanSquare = Math.pow(averageMeanSquare,0.5);
        return rootMeanSquare;
	}
	public long sampleCount = 0;
	@Override
	public void run() {
		isRunning = true;
		while(mustQuit == false) {
			try {
				updateFFTWindow();
				calculatePower();
				sampleCount++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isRunning = false;
		mustQuit = false;
	}
}
