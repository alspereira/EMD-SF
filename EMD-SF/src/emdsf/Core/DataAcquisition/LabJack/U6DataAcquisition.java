package emdsf.Core.DataAcquisition.LabJack;

import emdsf.Core.DataAcquisition.ADataAcquisition;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;

public class U6DataAcquisition extends ADataAcquisition implements Runnable {
	
	private U6StreamerWaitMode 	LJStreamer;
	private int					channelCount;
	
	public volatile Boolean finished;
	
	public U6DataAcquisition(U6StreamerWaitMode LJStreamer, int queueSize) {
		super(queueSize);
		this.LJStreamer 	= LJStreamer;
		this.channelCount 	= this.LJStreamer.getChannelCount();
		this.finished		= false;
	}
	
	//public ArrayBlockingQueue<MultiChannelSample> getMultiChannelSamples() {
	//	return super.getMultiChannelSamples();
	//}
	
	@Override
	public void run() {
		new Thread((Runnable) this.LJStreamer).start();
		//DAQSampleDTO<double[]> sample;
		DAQSampleDTO<double[]> sample;
		double[] samples;
		
		while(true) {
			try {
				//samples = LJStreamer.getSamples().poll(1000, TimeUnit.MILLISECONDS);
				sample = LJStreamer.getSamples().take();
				//samples = sample.getSamples();
				super.addDAQSample(sample, channelCount);
				//super.toMultiChannelSample(sample, channelCount); // can block here!! DANGER		
			} catch (InterruptedException e) {
				e.printStackTrace();
				return;
			}
		}
	}
}