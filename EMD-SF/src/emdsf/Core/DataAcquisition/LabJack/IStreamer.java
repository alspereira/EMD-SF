package emdsf.Core.DataAcquisition.LabJack;

import base.ControlledArrayBlockingQueue;

public interface IStreamer {
	public ControlledArrayBlockingQueue<double[]> getSamples();
	public int getChannelCount();
}
