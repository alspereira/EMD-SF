package emdsf.Core.DataAcquisition.LabJack;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

import com.labjack.LJUD;
import com.labjack.LJUDException;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;

public class U6StreamerWaitMode implements Runnable {
	
	private double 	scanRate;
	private double 	numScans;
	
	private int		numChannels;
	private int[]	channels;
	private ControlledArrayBlockingQueue<DAQSampleDTO> samples;
	
	private Boolean mustQuit;
	
	public U6StreamerWaitMode(int[] channels, double scanRate, int queueSize) {
		this.channels 		= channels;
		this.scanRate 		= scanRate;
		this.numScans 		= scanRate;
		this.mustQuit 		= false;
		this.numChannels 	= channels.length;
		samples = new ControlledArrayBlockingQueue<DAQSampleDTO>(queueSize);
	}
	
	public ControlledArrayBlockingQueue<DAQSampleDTO> getSamples() {
		return this.samples;
	}
	
	public int getChannelCount() {
		return this.numChannels;
	}
	
	// Quit this thread. I.e., no more data will be read from the labjack
	public void quit() {
		this.mustQuit = true;
	}

	private void handleLJUDException(LJUDException e) {
		e.printStackTrace();
		if(e.getError() > LJUD.Errors.MIN_GROUP_ERROR.getValue()) {
			System.exit(-1);
		}
	}

	//Displays warning message if there is one. Error values < 0 are warnings
	//and do not cause a LJUDException in the LJUD class.
	private void checkForWarning(int error) {
		Pointer errorStringPtr = new Memory(256);
		if(error < 0) {
			LJUD.errorToString(error, errorStringPtr);
			System.out.println("Warning: " + errorStringPtr.getString(0).trim());
		}
	}

	@Override
	public void run() {
		
		int intErrorcode 				= 0;
		IntByReference refHandle 		= new IntByReference(0);
		int intHandle 					= 0;

		IntByReference refIOType 		= new IntByReference(0);
		IntByReference refChannel 		= new IntByReference(0);
		DoubleByReference refValue 		= new DoubleByReference(0.0);
		DoubleByReference refBacklog 	= new DoubleByReference(0.0);
		
		DoubleByReference refNumScansRequested = new DoubleByReference(0.0);
		
		double[] adblData 				= new double[numChannels * (int)numScans]; //Max buffer size (#channels*numScansRequested)
		
		IntByReference dummyInt 		= new IntByReference(0);
		DoubleByReference dummyDouble 	= new DoubleByReference(0.0);
		NumberFormat formatter 			= new DecimalFormat("0.000");
		
		Boolean isDone 					= false;
		
		
		//Read and display the UD and LJUDJava versions.
		System.out.println("UD Driver Version = " + formatter.format(LJUD.getDriverVersion()));

        //Open the first found LabJack U6.
		intErrorcode = LJUD.openLabJack(LJUD.Constants.dtU6, LJUD.Constants.ctUSB, "1", 1, refHandle);
		checkForWarning(intErrorcode);
		intHandle = refHandle.getValue();		
		
		//Configure the stream:
		//Configure all analog inputs for 12-bit resolution
		LJUD.addRequest(intHandle, LJUD.Constants.ioPUT_CONFIG, LJUD.Constants.chAIN_RESOLUTION, 12, 0, 0);
		
		//Configure the analog input range on channel 0 for bipolar +-10 volts.
		LJUD.addRequest(intHandle, LJUD.Constants.ioPUT_AIN_RANGE, 0, LJUD.Constants.rgBIP10V, 0, 0);
		
		//Set the scan rate.
		LJUD.addRequest(intHandle, LJUD.Constants.ioPUT_CONFIG, LJUD.Constants.chSTREAM_SCAN_FREQUENCY, scanRate, 0, 0);
		
		//Give the driver a 5 second buffer (scanRate * 2 channels * 5 seconds).
		LJUD.addRequest(intHandle, LJUD.Constants.ioPUT_CONFIG, LJUD.Constants.chSTREAM_BUFFER_SIZE, scanRate * numChannels * 5, 0, 0);		
		
		//Configure reads to retrieve data when the requested samples are available (wait mode LJ_swSLEEP).
		LJUD.addRequest(intHandle, LJUD.Constants.ioPUT_CONFIG, LJUD.Constants.chSTREAM_WAIT_MODE, LJUD.Constants.swSLEEP, 0, 0);
		
		//Clear the stream channels
		LJUD.addRequest(intHandle, LJUD.Constants.ioCLEAR_STREAM_CHANNELS, 0, 0, 0, 0);
		//Define the scan list
		for(int i = 0; i < numChannels; i++) {
			
			LJUD.addRequest(intHandle, LJUD.Constants.ioADD_STREAM_CHANNEL, channels[i], 0, 0, 0);
		}
		
		//Execute the list of requests.
		LJUD.goOne(intHandle);
		
		//Get all the results just to check for errors.
		LJUD.getFirstResult(intHandle, refIOType, refChannel, refValue, dummyInt, dummyDouble);
		isDone = false;
		while(!isDone) {
			try {
				LJUD.getNextResult(intHandle, refIOType, refChannel, refValue, dummyInt, dummyDouble);
			}
			catch(LJUDException le) {
				if(le.getError() == LJUD.Errors.NO_MORE_DATA_AVAILABLE.getValue()) {
					isDone = true;
				}
				else {
					//Stop the stream
			        LJUD.eGet(intHandle, LJUD.Constants.ioSTOP_STREAM, 0, dummyDouble, 0);
					throw le;
				}
			}
		}
		
		//Stop the stream
        //LJUD.eGet(intHandle, LJUD.Constants.ioSTOP_STREAM, 0, dummyDouble, 0);
		
		//Start the stream.
		LJUD.eGet(intHandle, LJUD.Constants.ioSTART_STREAM, 0, refValue, 0);
		
		//The actual scan rate is dependent on how the desired scan rate divides into
		//the LabJack clock.  The actual scan rate is returned in the value parameter
		//from the start stream command.
		System.out.println("Actual Scan Rate = " + formatter.format(refValue.getValue()));
		
		// Read data while does not have to quit
		
		refNumScansRequested.setValue(numScans);
		while(!mustQuit) {
			//Note that the array we pass must be sized to hold enough SAMPLES, and
			//the Value we pass specifies the number of SCANS to read.
			LJUD.eGet(intHandle, LJUD.Constants.ioGET_STREAM_DATA,
					LJUD.Constants.chALL_CHANNELS, refNumScansRequested, adblData);
			
			// Offer the samples to the queue. If no space is available samples will be dropped
			if(samples.offer(
					new DAQSampleDTO( adblData, Calendar.getInstance().getTimeInMillis() )
					) == false ) {
				System.out.println("Raw Samples are being droped!");
			}
		}
		
		// TODO If this is point is reached it means that mustQuit was set to true. As such we must add the "pill of death"
		// to inform the consumers that no more data will be made available by this queue.
		
		
		//Stop the stream
        LJUD.eGet(intHandle, LJUD.Constants.ioSTOP_STREAM, 0, dummyDouble, 0);
		
	}	
}