package emdsf.Core.DataAcquisition.DTO;

public class DAQSampleDTO<T> {
	
	private long	unixTimestamp;
	private T 		data;

	public DAQSampleDTO(T data, long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
		this.data = data;
	}
	
	public DAQSampleDTO(T data) {
		this(data, 0l);
	}

	public long getUnixTimestamp() {
		return unixTimestamp;
	}

	public void setUnixTimestamp(long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
	}

	public T getSample() {
		return data;
	}

	public void setSample(T data) {
		this.data = data;
	}
}
