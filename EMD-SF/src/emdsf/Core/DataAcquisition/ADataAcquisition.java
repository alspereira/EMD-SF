package emdsf.Core.DataAcquisition;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;

public abstract class ADataAcquisition implements Runnable {
	
	protected ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> daqSamples_OUT;
	protected int channels;

	public ADataAcquisition(int queueSize, int channels) {
		daqSamples_OUT = new ControlledArrayBlockingQueue<DAQSampleDTO<float[]>>(queueSize);
		this.channels = channels;
	}
	
	public ADataAcquisition(int queueSize) {
		daqSamples_OUT = new ControlledArrayBlockingQueue<DAQSampleDTO<float[]>>(queueSize);
	}
	
	public ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> getDAQSamples_OUT() {
		return daqSamples_OUT;
	}
	
	// Interleaved sample from LabJack our soundcard (double[])
	protected void addDAQSample(DAQSampleDTO<double[]> data, int channels) {
		double[] 	samples 	= data.getSample();
		long		timestamp	= data.getUnixTimestamp();
		
		DAQSampleDTO<float[]> daqSample;
		
		float[]	multiChannelSample;
		for(int i = 0; i < samples.length; i = i + channels) {
			multiChannelSample = new float[channels];
			for(int j = 0; j < channels; j ++)
				multiChannelSample[j] = (float) samples[i + j];
			daqSample = new DAQSampleDTO<float[]>(multiChannelSample, timestamp);
			try {
				daqSamples_OUT.put(daqSample);
			} catch (InterruptedException e) {
				System.out.println("loosing samples");
				e.printStackTrace();
			}
		}
	}
	
	// sample from emd-df
	protected void addDAQSample(float[][] data) {
		
		DAQSampleDTO<float[]> daqSample;
		
		float[] daqSampleData;
		int numSamples = data[0].length;

		for(int i = 0; i < numSamples; i ++) {
			daqSampleData = new float [channels];
			for(int j = 0; j < channels; j++) {
				daqSampleData[j] = data[j][i];
			}
			daqSample = new DAQSampleDTO<float[]>(daqSampleData);
			try {
				daqSamples_OUT.put(daqSample);
			} catch (InterruptedException e) {
				System.out.println("loosing samples");
				e.printStackTrace();
			}
		}
	}

	@Override
	public abstract void run();

}
