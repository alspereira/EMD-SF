package emdsf.Core.DataAcquisition.File;

import java.io.File;
import java.io.IOException;

import emddf.file.SURFFile;
import emddf.file.SURFFileDescr;
import emdsf.Core.DataAcquisition.ADataAcquisition;
import javaEventing.EventManager;
import javaEventing.EventObject;
import util.visualization.DetectorChart;

public class DA_EMDDF2 extends ADataAcquisition {
	
	private File emddfFile;
	private int bufferSize;
	
	public DA_EMDDF2(File emddfFile, int queueSize, int bufferSize, int channels) {
		super(queueSize, channels);
		this.emddfFile = emddfFile;
		this.bufferSize = bufferSize;
		this.channels = channels;
	}
	//DetectorChart dc = new DetectorChart(5000);

	@Override
	public void run() {
		//dc.start();
		int samplesLength = 0;
		int numBuffers = 0;
		int samplesRemaining = 0;
		
		int count = 0;
		SURFFile surfFile = null;

		try {
			 surfFile = SURFFile.openAsRead( emddfFile );
			
			samplesLength = (int) surfFile.getFrameNum();
			//System.out.println("samples: " + samplesLength);
						
			if((samplesLength % bufferSize) == 0) {
				numBuffers = samplesLength / bufferSize;
				//System.out.println("number of buffers " + numBuffers);
			}
			else {
				numBuffers = (int) Math.ceil(samplesLength / bufferSize);
				samplesRemaining = samplesLength - (numBuffers * bufferSize);
				//System.out.println("number of buffers " + numBuffers);
				//System.out.println("samples ramaining " + samplesRemaining);
			}
			
			float[][] frames = new float[channels][bufferSize];
			while(count < 9000*10) {
				//System.out.println("get another buffer");
				frames = new float[channels][bufferSize];
				surfFile.readFrames(frames, 0, bufferSize);
				numBuffers --;
				/*for(int k = 0; k < frames[0].length; k++) {
					System.out.println(k);
					try {
						dc.powerMetric.put((double) frames[0][k]*20266.113);
						dc.detectionStatistics.put((double) frames[1][k]*20266.113);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
				super.addDAQSample(frames);
				//System.out.println("regular buffers " + numBuffers);
				count = count + frames[0].length;
				//System.out.println(count);
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
			// all done reading this file
			System.out.println("Finished reading the file: " + emddfFile.getAbsolutePath());
			EventManager.triggerEvent(this, new EventObject());
			//EventManager.getEventManagerInstance().shutdown();
			this.daqSamples_OUT.quit();
			
		}
		//this.daqSamples_OUT.mustQuit = true;
		surfFile.cleanUp();
		System.out.println("gracefully quiting " + this.getClass().getName());
	}

}