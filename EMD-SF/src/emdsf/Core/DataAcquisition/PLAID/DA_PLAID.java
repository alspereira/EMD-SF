package emdsf.Core.DataAcquisition.PLAID;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class DA_PLAID {
	
	// this function is used to read the pre-processed VI data
	public static Map<Integer, double[]> readVI(File data_file) throws IOException {
		Reader in;
		List<CSVRecord> arr = new ArrayList<CSVRecord>();
		Map<Integer, double[]> out = new LinkedHashMap<Integer, double[]>();
		double[] data_temp = null;
		int index = 0;
		int key = 1;
		
		in = new FileReader(data_file.getAbsolutePath());
		arr = CSVFormat.DEFAULT.parse(in).getRecords();
		Iterable<CSVRecord> records = arr;		
		for (CSVRecord record : records) {
			data_temp = new double[record.size()];
			for(int i = 0; i < record.size(); i++) {
				//System.out.println(record.get(i));
				data_temp[i] = Double.parseDouble(record.get(i)); 
			}
			out.put(key++, data_temp);
		}			
		
		return out;
	}
	
	public static Map<Integer, double[][]> readPLAID(File[] data_files, int data_channels, int numSamples) throws IOException {
		Reader in;
		List<CSVRecord> arr = new ArrayList<CSVRecord>();
		
		Map<Integer, double[][]> data = new LinkedHashMap<Integer, double[][]>();
		double[][] data_temp = null;
		int index = 0;
		
		int begin = 0, end = 1074;
		int min = Integer.MAX_VALUE;
		int max = 0;
		
		for(int f = begin; f < end; f++) {
			System.out.println(data_files[f].getAbsolutePath());
			
			in = new FileReader(data_files[f].getAbsolutePath());
			arr = CSVFormat.DEFAULT.parse(in).getRecords();
			//if(arr.size() < min)
			//	min = arr.size();
			//if(arr.size() > max)
			//	max = arr.size();
			System.out.println(arr.size());
			//Iterable<CSVRecord> records = arr;
			data_temp = new double[data_channels][arr.size()];
			data_temp = new double[data_channels][numSamples];
			int start = arr.size() - numSamples;
			for(int i = start; i < arr.size(); i++) {	
				data_temp[0][index] = Double.parseDouble(arr.get(i).get(0));
				data_temp[1][index] = Double.parseDouble(arr.get(i).get(1));
				index++;
			}
			/*
			for (CSVRecord record : records) {
				data_temp[0][index] = Double.parseDouble(record.get(0));
				data_temp[1][index] = Double.parseDouble(record.get(1));
				index++;
			}*/
			index = 0;
			data.put(f + 1, data_temp);
			
		}
		System.out.println("MIN: " + min);
		System.out.println("MAX: " + max);
		return data;		
	}
}
