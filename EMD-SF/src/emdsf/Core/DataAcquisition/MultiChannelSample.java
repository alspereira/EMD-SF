package emdsf.Core.DataAcquisition;

public class MultiChannelSample {
	
	private double[] samples;
	
	private long	unixTimestamp;
	
	public long getUnixTimestamp() {
		return unixTimestamp;
	}

	public void setUnixTimestamp(long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
	}
	
	public MultiChannelSample(double[] samples, long unixTimestamp) {
		this.samples = samples;
		this.unixTimestamp = unixTimestamp;
	}

	public MultiChannelSample(double[] samples) {
		this(samples, 0l);
	}

	public double[] getSamples() {
		return this.samples;
	}
}