package emdsf.EventBased.Evaluation.DTO;

import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;

public class GTPowerEventDTO implements IPowerEvent {

	public final long position;
	private final String text;

	public GTPowerEventDTO(long position, String text) {
		this.position = position;
		this.text = text;
	}
	
	@Override
	public long getPosition() {
		return this.position;
	}
	
	public String getText() {
		return text;
	}

	@Override
	public long getId() {
		// TODO Auto-generated method stub
		return 0;
	}
}