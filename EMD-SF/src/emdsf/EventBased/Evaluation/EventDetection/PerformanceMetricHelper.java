package emdsf.EventBased.Evaluation.EventDetection;

public class PerformanceMetricHelper {
	
	
	public enum  Options {
		TP, FP, FN, TN
	}
	
	private double GT_Count = 0; // this should be the same as TP + FN
	private double TP = 0, FP = 0 , TN = 0, FN = 0;
	private double TP_Count = 0, FP_Count = 0, TN_Count = 0, FN_Count = 0;
	
	private double PC = 0;
	private double TPC_FN = 0; // total power change of missed events
	private double TPC_FP = 0;
	private double APC_FN = 0;
	private double APC_FP = 0;
	
	private long ED_Pos;
	private long GT_Pos;
	
	// pass this in the constructor
	private int evaluation_id;
	private int test_id;
	private int tolerance;
	
	public PerformanceMetricHelper(int evaluation_id, int test_id, int tolerance) {
		this.evaluation_id 	= evaluation_id;
		this.test_id 		= test_id;
		this.tolerance 		= tolerance;
	}
	
	// E = TP + FN
	public void setValues(Options o, double PC, long ED_Pos, long GT_Pos) {
		resetMetrics();
		this.ED_Pos = ED_Pos;
		this.GT_Pos = GT_Pos;
		switch(o) {
			case TP:
				TP = 1;
				TP_Count ++;
				GT_Count ++;
				this.PC = Math.abs(PC);
				this.updateTN();
				break;
			case FP: 
				FP = 1;
				FP_Count ++;
				this.PC = PC;
				this.TPC_FP += Math.abs(PC);
				this.APC_FP = getNewAverage(FP_Count, APC_FP, PC);
				this.updateTN();
				break;
			case FN:
				FN = 1;
				FN_Count ++;
				GT_Count ++;
				this.PC = PC;
				this.TPC_FN += Math.abs(PC);
				this.APC_FN = getNewAverage(FN_Count, APC_FN, PC);
				this.updateTN();
				break;
			case TN:
				TN = 1;
				this.updateTN();
				break;
			default:
				break;
		}
	}
	
	public static String getHeader() {
		String out = "EVALUATION_ID;TEST_ID;GT_POS;ED_POS;DIFF;TOLERANCE;";
		out =  out + "TP;FP;TN;FN;PC;GT_Count;TP_Count;FP_Count;TN_Count;FN_Count;";
		//out = out + "Precision;Recall;FScore;P_R;";
		
		out = out + "Accuracy;Error;MCC;StdMCC";
		
		out = out + "Precision;Recall;F1Score;F05Score;F2Score;P_R;";
		
		out = out + "Specificity;FDR;";
		out = out +	"ROC_AUC;G_Fit;B_Fit;ROC_AUCB;";
		//out = out +	"ROC_AUC_RFDR;G_Fit_RFDR;B_Fit_RFDR;ROC_AUCB_RFDR;";
		//out = out +	"ROC_AUC_PR;G_Fit_PR;B_Fit_PR;ROC_AUCB_PR;";
		
		out = out + "TPR;FPR;TPR_FPR;";
		out = out + "TPP;FPP;TPP_FPP;";
		out = out + "TPC_FN;TPC_FP;TPC_FN__TPC_FP;";
		out = out + "APC_FN;APC_FP;APC_FN__APC_FP";
		return out + "\n";
	}
	
	public String toString() {
		String out = "";
		out = evaluation_id + ";" + test_id + ";" + GT_Pos + ";" + ED_Pos + ";" + (GT_Pos-ED_Pos) + ";" + tolerance + ";";
		out = out + TP + ";" + FP + ";" + TN + ";" + FN + ";" + PC + ";" + GT_Count + ";" + TP_Count + ";" + FP_Count + ";" + TN_Count + ";" + FN_Count + ";";
		//out = out + getP() + ";" + getR() + ";" + getFScore() + ";" + getP_R() + ";";
		
		out = out + getA() + ";" + getE() + ";" + getMCC() + ";" + getStdMCC() + ";";
		
		out = out + getP() + ";" + getR() + ";" + getFScore() + ";" + getFScore(0.5f) + ";" + getFScore(2f)  + ";" + getP_R() + ";";
		
		
		out = out + getSPC() + ";" + getFDR() + ";" ;
		out = out + getAUC(getTPR(), getSPC()) + ";" + getGFitness(getTPR(), getSPC()) + ";" + getBFitness(getTPR(), getSPC(), false) + ";" + getAUCB(getTPR(), getSPC()) + ";";
		//out = out + getAUC(getR(), getFDR()) + ";" + getGFitness(getR(), getFDR()) + ";" + getBFitness(getR(), getFDR(), false) + ";" + getAUCB(getR(), getFDR()) + ";";
		//out = out + getAUC(getP(), getR()) + ";" + getGFitness(getP(), getR()) + ";" + getBFitness(getP(), getR(), false) + ";" + getAUCB(getP(), getR()) + ";";
		
		out = out + getTPR() + ";" + getFPR() + ";" + getTPR_FPR() + ";";
		out = out + getTPP() + ";" + getFPP() + ";" + getTPP_FPP() + ";";
		out = out + getTPC_FN() + ";" + getTPC_FP() + ";" + getTPC_FN__TPC_FP() + ";";
		out = out + getAPC_FN() + ";" + getAPC_FP() + ";" + getAPC_FN__APC_FP();
		return out + "\n";
	}
	
	public static String getTotalHeader() {
		String out = "EVALUATION_ID;TEST_ID;TOLERANCE;";
		out =  out + "GT_Count;TP_Count;FP_Count;TN_Count;FN_Count;";
		
		out = out + "Accuracy;Error;MCC;StdMCC;";
		
		out = out + "Precision;Recall;F1Score;F05Score;F2Score;P_R;";
		
		out = out + "Specificity;FDR;";
		out = out +	"ROC_AUC;G_Fit;B_Fit;ROC_AUCB;";
		//out = out +	"ROC_AUC_RFDR;G_Fit_RFDR;B_Fit_RFDR;ROC_AUCB_RFDR;";
		//out = out +	"ROC_AUC_PR;G_Fit_PR;B_Fit_PR;ROC_AUCB_PR;";
		
		out = out + "TPR;FPR;TPR_FPR;";
		out = out + "TPP;FPP;TPP_FPP;";
		out = out + "TPC_FN;TPC_FP;TPC_FN__TPC_FP;";
		out = out + "APC_FN;APC_FP;APC_FN__APC_FP";
		return out + "\n";
	}
	
	public String toStringTotal() {
		String out = "";
		out = evaluation_id + ";" + test_id + ";" + tolerance + ";";
		out = out + GT_Count + ";" + TP_Count + ";" + FP_Count + ";" + TN_Count + ";" + FN_Count + ";";
		
		out = out + getA() + ";" + getE() + ";" + getMCC() + ";" + getStdMCC() + ";";
		
		out = out + getP() + ";" + getR() + ";" + getFScore() + ";" + getFScore(0.5f) + ";" + getFScore(2f)  + ";" + getP_R() + ";";
		
		//out = out + getSPC() + ";" + getFDR() + ";" + getROC_AUC() + ";" + getPR_AUC() + ";";
		out = out + getSPC() + ";" + getFDR() + ";" ;
		out = out + getAUC(getTPR(), getSPC()) + ";" + getGFitness(getTPR(), getSPC()) + ";" + getBFitness(getTPR(), getSPC(), false) + ";" + getAUCB(getTPR(), getSPC()) + ";";
		//out = out + getAUC(getR(), getFDR()) + ";" + getGFitness(getR(), getFDR()) + ";" + getBFitness(getR(), getFDR(), false) + ";" + getAUCB(getR(), getFDR()) + ";";
		//out = out + getAUC(getP(), getR()) + ";" + getGFitness(getP(), getR()) + ";" + getBFitness(getP(), getR(), false) + ";" + getAUCB(getP(), getR()) + ";";
		
		out = out + getTPR() + ";" + getFPR() + ";" + getTPR_FPR() + ";";
		out = out + getTPP() + ";" + getFPP() + ";" + getTPP_FPP() + ";";
		out = out + getTPC_FN() + ";" + getTPC_FP() + ";" + getTPC_FN__TPC_FP() + ";";
		out = out + getAPC_FN() + ";" + getAPC_FP() + ";" + getAPC_FN__APC_FP();
		return out + "\n";
	}
	
	private void resetMetrics() {
		TP = 0; FP = 0; FN = 0; TN = 0;
		PC = 0;
	}
	
	// here new value can be replaced by this.PC
	private double getNewAverage(double currentCount, double previousAverage, double newValue) {
		//System.out.println(newValue);
		//System.out.println("P: " + previousAverage);
		double newAverage = (double) (currentCount - 1) / currentCount;
		//System.out.println(newAverage);
		newValue = Math.abs(newValue);
		newAverage = newAverage * previousAverage;
		newAverage = newAverage + ( (double)(1/currentCount) * newValue);
		//System.out.println("N: " + newAverage);
		//previousAverage = newAverage;
		return newAverage;
	}
	
	private void updateTN() {
		TN_Count = (int) (this.ED_Pos - (TP_Count + FP_Count + FN_Count));
	}
	
	public double getA() {
		double o = 0;
		if(TP_Count + TN_Count + FP_Count + FN_Count > 0)
			o = (double) (TP_Count + TN_Count) / (TP_Count + TN_Count + FP_Count + FN_Count);
		return o;
	}
	
	public double getE() {
		double o = 0;
		if(TP_Count + TN_Count + FP_Count + FN_Count > 0)
			o = 1 - getA();
		return o;
	}
	
	public double getMCC() {
		double o = 0;
		double n = (double) (TP_Count * TN_Count) - (double)(FP_Count * FN_Count);
		double d = 
				(double) (TP_Count + FP_Count) * 
				(double) (TP_Count + FN_Count) * 
				(double) (TN_Count + FP_Count) * 
				(double) (TN_Count + FN_Count);
		d = Math.sqrt(d);
		//System.out.println(TP_Count + " | " + TN_Count + " | " + FP_Count + " | " + FN_Count);
		//System.out.println(n);
		//System.out.println(d);
		if( d > 0 ) {
			o = (double) (n / d);
		}
		//System.out.println(o);
		return o;
	}
	
	public double getStdMCC() {
		return (1 + getMCC()) * 0.5;
	}
	
	// Precision
	public double getP() {
		double o;
		if(TP_Count + FP_Count == 0)
			o = 0;
		else
			o = (double) TP_Count / (TP_Count + FP_Count);
		return o;
	}
	
	public double getR() {
		double o;
		if(TP_Count + FN_Count == 0)
			o = 0;
		else 
			o = (double) TP_Count / (TP_Count + FN_Count) ;
		return o;
	}
	
	// Two other commonly used F measures are the F_{2} measure, which weights recall higher than precision, and the F_{0.5} measure, which puts more emphasis on precision than recall.
	public double getFScore() {
		double o;
		if(TP_Count + FP_Count + FN_Count == 0)
			o = 0;
		else
			o = (double) (2 * TP_Count) / ( (2 * TP_Count) + FP_Count + FN_Count );
		
		return o;
	}
	
	public double getFScore(float b) {
		double o;
		if(TP_Count + FP_Count + FN_Count == 0)
			o = 0;
		else
			o =  (double) ( (1 + Math.pow(b, 2)) * TP_Count ) / ( ( (1 + Math.pow(b, 2)) * TP_Count) + ( Math.pow(b, 2) * FN_Count ) + FP_Count ); 
		return o;
	}
	
	// Same as Sensitivity / Recall (TPR = Sensitivity = Recall)
	public double getTPR() {
		return this.getR();
	}
	
	public double getFPR() {
		double o;
		if(FP_Count + TN_Count == 0)
			o = 0;
		else 
			o = (double) FP_Count / (FP_Count + TN_Count);
		
		return o;
	}
	
	public double getTPP() {
		double o;
		if(GT_Count == 0)
			o = 0;
		else			
			o = (double) TP_Count / GT_Count;
		return o;
	}
	
	public double getFPP() {
		double o;
		if(GT_Count == 0)
			o = 0;
		else
			o = (double) FP_Count / GT_Count;
		return o;
	}
	
	// specificity
	public double getSPC() {
		double o;
		if(TN_Count + FP_Count == 0)
			o = 0;
		else
			o = (double) TN_Count / (TN_Count + FP_Count);
		return o;
	}
	
	// false discovery rate
	public double getFDR() {
		double o;
		if(TP_Count + FP_Count == 0)
			o = 0;
		else
			o = (double) FP_Count / (TP_Count + FP_Count);
		return o;
	}
	
	// AUC discrete aka W-fitnes
	public double getROC_AUC() {
		return (this.getTPR() + this.getSPC()) * 0.5;
	}
	
	// AUC discrete aka W-fitnes (remove!!!)
	public double getPR_AUC() {
		return (this.getP() + this.getTPR()) * 0.5;
	}
	
	// AUC discrete aka W-fitnes
	public double getAUC(double a, double b) {
		return (a + b) * 0.5;
	}
	
	public double getGFitness(double a, double b) {
		return Math.sqrt(a * b);
	}
	
	// a = sensitivity b = specificity
	public double getBFitness(double a, double b, boolean targetIsMajority) {
		double bias;
		if(targetIsMajority)
			bias = (double) a / 2;
		else
			bias = (double) b / 2;
		return 0.5 * a * b + bias;
	}
	
	public double getAUCB(double a, double b) {
		return getAUC(a, b) * (1 - Math.abs(a - b));
	}
	
	public double getTPC_FN() {
		return TPC_FN;
	}
	
	public double getTPC_FP() {
		return TPC_FP;
	}
	
	public double getAPC_FN() {
		return APC_FN;
	}
	
	public double getAPC_FP() {
		return APC_FP;
	}
	
	public double getP_R() {
		return Math.pow(getP(), 2) + Math.pow(getR(), 2) - (2 * getP()) - (2 * getR()) + 2;
	}
	
	// Combination of TRP and FPR
	public double getTPR_FPR() {
		//return Math.pow(getTPR(), 2) - (2 * getTPR()) + 1;
		return Math.pow(getTPR(), 2) + Math.pow(getFPR(), 2) - (2 * getTPR()) + 1;
	}
	
	public double getTPP_FPP() {
		return Math.pow(getFPP(), 2) + Math.pow(getTPP(), 2) - (2 * getTPP()) + 1;
	}
	
	public double getTPC_FN__TPC_FP() {
		return Math.pow(getTPC_FN(), 2) + Math.pow(getTPC_FP(), 2);
	}
	
	public double getAPC_FN__APC_FP() {
		return Math.pow(getAPC_FN(), 2) + Math.pow(getAPC_FP(), 2);
	}
}
