package emdsf.EventBased.Evaluation.EventDetection;

import java.io.IOException;

import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;
import util.database.dto.ExtendedDetectionPowerEventDTO;
import util.file.MyFileWriter;

public class MyPerformanceEvaluatorKyle2 {

	private IPowerEvent[] groundTruthEvents;
	private ExtendedDetectionPowerEventDTO[] detectedEvents;
	private double[] deltas;
	
	
	private MyFileWriter mFW;
	private MyFileWriter mFW_global;
	private PerformanceMetricHelper pmH;
	
	private int evaluation_id;
	private int test_id;
	
	private long file_size = 36755788l;
	 
	public MyPerformanceEvaluatorKyle2(
			IPowerEvent[] groundTruthEvents, 
			ExtendedDetectionPowerEventDTO[] detectedEvents,
			double[] deltas,
			MyFileWriter mFW, 
			MyFileWriter mFW_global, 
			int evaluation_id, 
			int test_id) throws IOException {
		
		this.groundTruthEvents 	= groundTruthEvents;
		this.detectedEvents 	= detectedEvents;
		this.mFW = mFW;
		this.mFW_global = mFW_global;
		
		this.evaluation_id 	= evaluation_id;
		this.test_id 		= test_id;
		
		this.deltas 		= deltas;
	}
	
	public synchronized void eval(int tolerance) throws IOException {
		this.pmH = new PerformanceMetricHelper(evaluation_id, test_id, tolerance);
		//this.mFW.write(pmH.getHeader());
		
		int eIndex = 0;
		int gtIndex = 0;
		IPowerEvent currentGT; 
		ExtendedDetectionPowerEventDTO currentEvent;
		Long dist;
		Long prevDist = null;
		ExtendedDetectionPowerEventDTO prevEvent = null;
		if(this.detectedEvents.length > 0) {
			currentGT = this.groundTruthEvents[0];
			currentEvent = this.detectedEvents[0];
			
			
			while(gtIndex < groundTruthEvents.length && eIndex < detectedEvents.length) {	
				
				currentGT = this.groundTruthEvents[gtIndex];
				currentEvent = detectedEvents[eIndex];
				
				// this is the perfect case when the event and the gt position match
				if( currentEvent.getPosition() == currentGT.getPosition() ) {
					// we must check what was before
					if(prevEvent != null) {
						// there was an event inside the tolerance for this GT, that now should be added as a FP
						pmH.setValues(PerformanceMetricHelper.Options.FP, prevEvent.delta_111,  prevEvent.getPosition(), currentGT.getPosition());
						mFW.write(pmH.toString());
						// reset all the might come from before
						dist = null;
						prevDist = null;
						prevEvent = null;
					}					
					// adds a TP and moves to the next event and next GT
					pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
					mFW.write(pmH.toString());
					eIndex ++;
					gtIndex ++;
				}
				// event is inside the tolerance (it will never be the exact position since
				// this case is caught in the previous if clause
				else if( currentEvent.getPosition() >= currentGT.getPosition() - tolerance
						&&
						currentEvent.getPosition() <= currentGT.getPosition() + tolerance
						) {
					dist = currentGT.getPosition() - currentEvent.getPosition();
					if(dist < 0 ) {
						// current event in front of GT
						if(prevDist != null) { // something was detected before as a possible match for this GT
							
							if(Math.abs(dist) < Math.abs(prevDist)) { // this one is closer to the GT and should be selected
								pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
								mFW.write(pmH.toString());
								eIndex ++;
								gtIndex ++;
								// reset all
								prevDist = null;
								dist = null;
								prevEvent = null;
							}
							else { // the previous event was closer and should be selected
								pmH.setValues(PerformanceMetricHelper.Options.TP, prevEvent.delta_111, prevEvent.getPosition(), currentGT.getPosition());
								mFW.write(pmH.toString());
								eIndex ++;
								gtIndex ++;
								// reset all
								prevDist = null;
								dist = null;
								prevEvent = null;
							}
						}
						else { // there was no candidate to be an event
							// this one is closer to the GT so it is a TP
							pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
							mFW.write(pmH.toString());
							eIndex ++;
							gtIndex ++;
						}
					}
					else {
						// current event is before GT. thus we must check if there is an event closer to the gt
						prevDist = dist;
						prevEvent = currentEvent;
						// increment the current event count such that we can compare another event
						eIndex++;
					}
				}
				// another else if goes here
				else {
					if( currentEvent.getPosition() < currentGT.getPosition() - tolerance) {
						// event is before the tolerance -> it is a false positive
						pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
						mFW.write(pmH.toString());
						// increment the current event index in order to go for the next comparison
						eIndex ++;
					}
					else { // currentEvent.getPosition() > currentGT.getPosition() + tolerance
						// event is after the tolerance -> there is a false negative
						pmH.setValues(PerformanceMetricHelper.Options.FN, deltas[gtIndex],  currentEvent.getPosition(), currentGT.getPosition());
						mFW.write(pmH.toString());
						// but this event can be a TP for the next GT, thus we only increment the GT and compare this event again
						gtIndex ++;
					}
				}
			}
			
			if(gtIndex < groundTruthEvents.length) {
				//System.out.println("more FN " + this.test_id + " " + tolerance);
				for(int i = gtIndex; i < groundTruthEvents.length; i++) {
					currentGT = this.groundTruthEvents[gtIndex];
					// need to check this delta
					pmH.setValues(PerformanceMetricHelper.Options.FN, deltas[gtIndex],  currentEvent.getPosition(), currentGT.getPosition());
					mFW.write(pmH.toString());
					gtIndex ++;
				}
			}
			
			if( eIndex <  detectedEvents.length ) {
				//System.out.println("more FP " + this.test_id + " " + tolerance);
				for(int i = eIndex; i < detectedEvents.length; i++) {
					currentEvent = detectedEvents[eIndex];
					pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
					mFW.write(pmH.toString());
					eIndex ++;
				}
			}
			// update the TN
			pmH.setValues(PerformanceMetricHelper.Options.TN, 0, this.file_size, 0);
			// print the last row here
			mFW.write(pmH.toString());
		}
		else {
			// nao encontrou nada. é tudo falso negativo
			for(int i = 0; i < groundTruthEvents.length; i++) {
				currentGT = this.groundTruthEvents[gtIndex];
				//currentEvent = Long.min
				pmH.setValues(PerformanceMetricHelper.Options.FN, this.deltas[i],  0, currentGT.getPosition());
				mFW.write(pmH.toString());
				gtIndex ++;
			}
		}
		
		
		//mFW_global.write(pmH.toString());
		mFW_global.write(pmH.toStringTotal());
	}
	
	public class EvaluationSample {
		public IPowerEvent gtpe;
		public IPowerEvent edpe;
		
		public EvaluationSample(IPowerEvent gtpe, IPowerEvent edpe) {
			this.gtpe = gtpe;
			this.edpe = edpe;
		}
	}
}
