package emdsf.EventBased.Evaluation.EventDetection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Event.IPowerEventListener;
import emdsf.EventBased.Disaggregation.EventDetection.Event.PowerEvent;
import emdsf.EventBased.Disaggregation.FeatureExtraction.Delta;
import emdsf.EventBased.Evaluation.DTO.GTPowerEventDTO;

public class EDE_Pereira20xx extends AEventDetectionEvaluation implements IPowerEventListener{

	public GTPowerEventDTO[] groundTruthEvents;
	private int currentGroundTruthIndex = 0;
	private long gt_pos;
	
	private int tolerance = 0; // this is what defines how far a detected power event can be for the real value
	private float threshold = 30;  // this is the minimum change in the metric for an event to be considered
	private int w1, w2, w3;
	
	Delta dm = new Delta();
	PowerMetricType metric;
	
	public EDE_Pereira20xx(PowerMetricType metric, int tolerance, float threshold,
			int w1, int w2, int w3) {
		this.metric = metric;
		this.tolerance = tolerance;
		this.threshold = threshold;
		this.w1 = w1;
		this.w2 = w2;
		this.w3 = w3;
	}
	
	private boolean isRealFP(IPowerSample[] powerSamples, int e) {
		boolean isRealFP = false;
		float delta = dm.getDelta(powerSamples, metric, e, w1, w2, w3);
		if(Math.abs(delta) <= threshold)
			isRealFP = true;
		return isRealFP;
	}
	
	private void updateGrountTruthIndex() {
		if(this.currentGroundTruthIndex <= this.groundTruthEvents.length - 1)
			this.currentGroundTruthIndex ++;
		
	}
	
	int tp = 0, fp = 0, fn = 0, ufp = 0, rfp = 0;

	@Override
	public void onEventDetected(PowerEvent event) {
		long detected_pos = event.getPowerSamples()[event.getIndex()].getID();
		
		if(this.currentGroundTruthIndex <= this.groundTruthEvents.length)
			gt_pos = groundTruthEvents[currentGroundTruthIndex].position;
		else
			gt_pos = Long.MAX_VALUE;
		
		//System.out.println("size: " + event.getPowerSamples().length + " event index: " + event.getIndex());
		System.out.println("detected: " + detected_pos + " | ground-truth: " + gt_pos + " | " + (gt_pos - tolerance) + " | " + gt_pos + tolerance );
		System.out.println("delta: " + dm.getDelta(event.getPowerSamples(), metric, event.getIndex(), w1, w2, w3));
		
		if(detected_pos + tolerance < gt_pos) { // FP
			System.out.println("FP at " + detected_pos + " from GT: " + gt_pos + " | total " + ++fp);
			
			if(isRealFP(event.getPowerSamples(), event.getIndex())) { //  a real false positive is one whose delta is lower than the threshold
				super.addEvaluationSample(detected_pos, gt_pos, 
						event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.RFP);
						//System.out.println("TFP at " + detected_pos + " from GT: " + gt_pos + " total " + ++rfp);
			}
			else {
				super.addEvaluationSample(detected_pos, gt_pos, 
					event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.FP);
					//System.out.println("FFP at " + detected_pos + " from GT: " + gt_pos + " | total " + ++ufp);
			}
		}
		else {
			while(gt_pos < detected_pos + tolerance) {
				if( detected_pos >= gt_pos - tolerance && detected_pos <= gt_pos + tolerance) { // is TP
					//System.out.println("TP at " + detected_pos + " from GT: " + gt_pos + " | total " + ++tp);
					super.addEvaluationSample(detected_pos, gt_pos, 
							event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.TP);
					
					this.updateGrountTruthIndex();
					break;
				}
				else /*( detected_pos > gt_pos + tolerance ) */{ // is FN
					super.addEvaluationSample(detected_pos, gt_pos, 
							event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.FN);
					//System.out.println("FN at " + detected_pos + " from GT: " + gt_pos + " | total " + ++fn);
					
					this.updateGrountTruthIndex();
					gt_pos = groundTruthEvents[currentGroundTruthIndex].position;
				}
				
			}		
		}
		// resolver a chatice de acabar o ground-truth e o gajo continuar a detetar eventos
	}
}
