package emdsf.EventBased.Evaluation.EventDetection;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Calendar;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Event.IPowerEventListener;
import emdsf.EventBased.Disaggregation.EventDetection.Event.PowerEvent;
import emdsf.EventBased.Disaggregation.FeatureExtraction.Delta;
import emdsf.EventBased.Evaluation.DTO.GTPowerEventDTO;

public class EDE_Pereira20xx2 extends AEventDetectionEvaluation implements IPowerEventListener{

	private GTPowerEventDTO[] groundTruthEvents;
	private int currentGroundTruthIndex = 0;
	private long gt_pos;
	private long last_gt_pos;
	private int tolerance = 0; // this is what defines how far a detected power event can be for the real value
	private float threshold = 30;  // this is the minimum change in the metric for an event to be considered
	private int w1, w2, w3;
	
	Delta dm = new Delta();
	PowerMetricType metric;
	FileWriter fw;
	
	public EDE_Pereira20xx2(GTPowerEventDTO[] groundTruthEvents, PowerMetricType metric, int tolerance, float threshold,
			int w1, int w2, int w3, String filePath) {
		this.metric = metric;
		this.tolerance = tolerance;
		this.threshold = threshold;
		this.w1 = w1;
		this.w2 = w2;
		this.w3 = w3;
		fw = new FileWriter(filePath);
		fw.WriteString("POS;GT_POS;MetricValue;TP;FN;FP;TFP;NFP;Delta;Ds\n");
		//fw.WriteString("POS;GT_POS;MetricValue;TP;FN;FP;TFP;NFP;Delta;Acc_TP;Acc_FN;Acc_FP;AccTFP;AccNFP;Pre;"
		//		+ "Sen;F1;TPR;FPR;FDR;\n");
		this.groundTruthEvents = groundTruthEvents;
		this.last_gt_pos = groundTruthEvents[groundTruthEvents.length - 1].position;
		System.out.println(this.last_gt_pos);
	}
	
	private boolean isRealFP(IPowerSample[] powerSamples, int e) {
		boolean isRealFP = false;
		float delta = dm.getDelta(powerSamples, metric, e, w1, w2, w3);
		if(Math.abs(delta) <= threshold)
			isRealFP = true;
		return isRealFP;
	}
	
	private void updateGrountTruthIndex() {
		if(this.currentGroundTruthIndex < this.groundTruthEvents.length - 1)
			this.currentGroundTruthIndex ++;
		
	}
	
	//int tp = 0, fp = 0, fn = 0, nfp = 0, tfp = 0;
	int accTp = 0, accFp = 0, accFn = 0, accFfp = 0, accTfp = 0, accTn;
	float pre = 0, sen = 0, f1 = 0;
	float accPre = 0, accSen = 0, accF1 = 0;
	float tpr = 0, fpr = 0;
	float accTpr = 0, accFpr = 0;
	float fdr = 0; // false discovery rate
	float accFdr = 0;
	

	@Override
	public synchronized void onEventDetected(PowerEvent event) {
		long detected_pos = event.getPowerSamples()[event.getIndex()].getID();
		
		if(this.currentGroundTruthIndex <= this.groundTruthEvents.length - 1)
			gt_pos = groundTruthEvents[currentGroundTruthIndex].position;
		else
			gt_pos = Long.MAX_VALUE;
		
		float delta = dm.getDelta(event.getPowerSamples(), metric, event.getIndex(), w1, w2, w3);
		
		//System.out.println("handle event: " + event.getPowerSamples()[event.getIndex()].getID() 
		//		+ " | gt pos " + gt_pos + " + tolerance " + (gt_pos + tolerance));
		
		if(detected_pos + tolerance < gt_pos || detected_pos > last_gt_pos + tolerance ) { // FP
			//System.out.println("FP at " + detected_pos + " from GT: " + gt_pos + " | total " + ++fp);
			accFp++;
			if(isRealFP(event.getPowerSamples(), event.getIndex())) { //  a real false positive is one whose delta is lower than the threshold
				super.addEvaluationSample(detected_pos, gt_pos, 
						event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.RFP);
				accTfp++;
				
				fw.WriteString(
						Long.toString(detected_pos)+";"
						+Long.toString(gt_pos)+";"
						+Float.toString(event.getPowerSamples()[event.getIndex()].getMetricValue(metric))+";"
						+"0" + ";" + "0" + ";" + "1" + ";" + "1" + ";" + "0" + ";" + delta + ";" + event.getDs() + "\n"); 
						//+accTp + ";" + accFn + ";" + accFp + ";" + accTfp + ";" + accFfp + ";" + pre + ";"
						//+ "\n");
				
				//fw.WriteString("POS;GT_POS;MetricValue;TP;FN;FP;TFP;NFP;Delta;Acc_TP;Acc_FN;Acc_FP;AccTFP;AccNFP;Pre;"
				//		+ "Sen;F1;TPR;FPR;FDR;\n");
			}
			else {
				super.addEvaluationSample(detected_pos, gt_pos, 
					event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.FP);
					//System.out.println("FFP at " + detected_pos + " from GT: " + gt_pos + " | total " + ++ufp);
				accFfp++;
				fw.WriteString(
						Long.toString(detected_pos)+";"
						+Long.toString(gt_pos)+";"
						+Float.toString(event.getPowerSamples()[event.getIndex()].getMetricValue(metric))+";"
						+"0" + ";" + "0" + ";" + "1" + ";" + "0" + ";" + "1" + ";" + delta  + ";" + event.getDs() +  "\n");
			}
		}
		else {
			//System.out.println("handling something else gt_pos " + gt_pos);
			while(gt_pos <= detected_pos + tolerance) {
				if( detected_pos >= gt_pos - tolerance && detected_pos <= gt_pos + tolerance) { // is TP
					//System.out.println("TP at " + detected_pos + " from GT: " + gt_pos + " | total " + ++tp);
					super.addEvaluationSample(detected_pos, gt_pos, 
							event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.TP);
					fw.WriteString(
							Long.toString(detected_pos)+";"
							+Long.toString(gt_pos)+";"
							+Float.toString(event.getPowerSamples()[event.getIndex()].getMetricValue(metric))+";"
							+"1" + ";" + "0" + ";" + "0" + ";" + "0" + ";" + "0" + ";" + delta   + ";" + event.getDs() +  "\n");
					this.updateGrountTruthIndex();
					break;
				}
				else if( detected_pos > gt_pos + tolerance ) { // is FN
					super.addEvaluationSample(detected_pos, gt_pos, 
							event.getPowerSamples()[event.getIndex()].getMetricValue(metric), SampleType.FN);
					//System.out.println("FN at " + detected_pos + " from GT: " + gt_pos + " | total " + ++fn);
					fw.WriteString(
							Long.toString(detected_pos)+";"
							+Long.toString(gt_pos)+";"
							+Float.toString(event.getPowerSamples()[event.getIndex()].getMetricValue(metric))+";"
							+"0" + ";" + "1" + ";" + "0" + ";" + "0" + ";" + "0" + ";" + delta   + ";" + event.getDs() +  "\n");
					this.updateGrountTruthIndex();
					
					gt_pos = groundTruthEvents[currentGroundTruthIndex].position;
					
				}
				else {
					System.out.println("missed something here " + detected_pos + "| " + gt_pos );
				}
				
				System.out.println("still in here");
			}		
		}
		// resolver a chatice de acabar o ground-truth e o gajo continuar a detetar eventos
		//System.out.println("done handling event: " + event.getPowerSamples()[event.getIndex()].getID());
	}
	
	private class FileWriter {
		Path path;
		public FileWriter(String _path) {
			File output = new File(_path);
			try {
				output.createNewFile();
				
				path = Paths.get(_path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void WriteString(String myString) {
			try {
				Files.write(path, myString.getBytes(), StandardOpenOption.APPEND);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	

}
