package emdsf.EventBased.Evaluation.EventDetection;

import java.util.ArrayList;
import java.util.List;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;
import emdsf.EventBased.Evaluation.DTO.GTPowerEventDTO;

public class Evaluator {

	private IPowerEvent[] groundTruthEvents;
	
	private IPowerEvent[] detectedEvents;
	
	private List<EvaluationSample> 	truePositives;
	private List<EvaluationSample> 	falseNegative;
	private List<IPowerEvent>		falsePositives;
	private int						trueNegatives;
	private int 					gtIndex;

	public Evaluator(IPowerEvent[] groundTruthEvents, IPowerEvent[] detectedEvents) {
		this.groundTruthEvents 	= groundTruthEvents;
		this.detectedEvents 	= detectedEvents;
		
		this.truePositives 		= new ArrayList<EvaluationSample>();
		this.falseNegative 		= new ArrayList<EvaluationSample>();
		this.falsePositives 	= new ArrayList<IPowerEvent>();
		this.gtIndex 			= 0;
	}
	
	private void updateGTIndex() {
		if(gtIndex < groundTruthEvents.length - 1) {
			gtIndex ++;
		}
	}
	
	public void eval(int tolerance) {
		IPowerEvent currentGT = this.groundTruthEvents[0];
		IPowerEvent currentEvent;
		int tp = 0, fp = 0, fn = 0;
		
		for(int i = 0; i < detectedEvents.length; i++) {
			currentEvent = detectedEvents[i];
			currentGT = this.groundTruthEvents[gtIndex];
			
			System.out.println ("GT INDEX: " + gtIndex + " GT POS: "  + currentGT.getPosition() + " I: " + i);
			System.out.println( (currentEvent.getPosition() - tolerance) + " | " + currentEvent.getPosition() + " | " + (currentEvent.getPosition() - tolerance) );
			
			if( currentGT.getPosition() >= (currentEvent.getPosition() - tolerance) 
					&& 
				currentGT.getPosition() <= (currentEvent.getPosition() + tolerance)  ) {
				
				this.truePositives.add(new EvaluationSample(currentGT, currentEvent));
				this.updateGTIndex();
				System.out.println("TP " + ++tp);
			}
			else {
				if( currentEvent.getPosition() < currentGT.getPosition() - tolerance ) {
					this.falsePositives.add(currentEvent);
					System.out.println("FP " + ++fp);
				}
				else {
					if( currentEvent.getPosition() > currentGT.getPosition() + tolerance ) {
						this.falseNegative.add( new EvaluationSample(currentGT, currentEvent) );
						this.updateGTIndex();
						System.out.println("FN " + ++fn);
					}
					else {
						System.out.println("outra merda qq");
					}
				} 		
			}			
		}
		System.exit(-1);
	}
	
	public class EvaluationSample {
		public IPowerEvent gtpe;
		public IPowerEvent edpe;
		
		public EvaluationSample(IPowerEvent gtpe, IPowerEvent edpe) {
			this.gtpe = gtpe;
			this.edpe = edpe;
		}
	}

	public List<EvaluationSample> getTruePositives() {
		return truePositives;
	}

	public List<EvaluationSample> getFalseNegative() {
		return falseNegative;
	}

	public List<IPowerEvent> getFalsePositives() {
		return falsePositives;
	}

	public int getTrueNegatives() {
		return trueNegatives;
	}
	
	

}
