package emdsf.EventBased.Evaluation.EventDetection;

import java.io.IOException;

import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;
import util.database.dto.ExtendedDetectionPowerEventDTO;
import util.file.MyFileWriter;

public class MyPerformanceEvaluatorComplex {

	private IPowerEvent[] groundTruthEvents;
	private ExtendedDetectionPowerEventDTO[] detectedEvents;
	private double[] deltas;
	
	
	private MyFileWriter mFW;
	private MyFileWriter mFW_global;
	private PerformanceMetricHelper pmH;
	
	private int evaluation_id;
	private int test_id;
	
	private long file_size = 36755788l;
	 
	public MyPerformanceEvaluatorComplex(
			IPowerEvent[] groundTruthEvents, 
			ExtendedDetectionPowerEventDTO[] detectedEvents,
			double[] deltas,
			MyFileWriter mFW, 
			MyFileWriter mFW_global, 
			int evaluation_id, 
			int test_id) throws IOException {
		
		this.groundTruthEvents 	= groundTruthEvents;
		this.detectedEvents 	= detectedEvents;
		this.mFW = mFW;
		this.mFW_global = mFW_global;
		
		this.evaluation_id 	= evaluation_id;
		this.test_id 		= test_id;
		
		this.deltas 		= deltas;
	}
	
	public synchronized void eval(int tolerance) throws IOException {
		this.pmH = new PerformanceMetricHelper(evaluation_id, test_id, tolerance);
		//this.mFW.write(pmH.getHeader());
		
		int eIndex = 0;
		int gtIndex = 0;
		IPowerEvent currentGT; 
		ExtendedDetectionPowerEventDTO currentEvent;
		Long dist_1;
		Long dist_2;
		Long prevDist = null;
		ExtendedDetectionPowerEventDTO prevEvent = null;
		ExtendedDetectionPowerEventDTO nextEvent = null;
		if(this.detectedEvents.length > 0) {
			currentGT = this.groundTruthEvents[0];
			currentEvent = this.detectedEvents[0];
					
			while(gtIndex < groundTruthEvents.length && eIndex < detectedEvents.length) {	
				
				currentGT = this.groundTruthEvents[gtIndex];
				currentEvent = detectedEvents[eIndex];
				
				// this is the perfect case when the event and the gt position match
				if( currentEvent.getPosition() == currentGT.getPosition() ) {
					//System.out.println("TP na mouche");
					pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
					//mFW.write(pmH.toString());
					eIndex++;
					gtIndex++;
				}
				// CE is inside tolerance, but before CGT
				else if(currentEvent.getPosition() >= currentGT.getPosition() - tolerance &&
						currentEvent.getPosition() < currentGT.getPosition()) {
					//System.out.println("está na tolerancia");
					nextEvent = this.getNextEvent(eIndex);
					// se nao tiver este evento é um TP
					if(nextEvent == null) {
						// é um tp
						//System.out.println("TP porque é o ultimo evento");
						pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
						//mFW.write(pmH.toString());
						eIndex++; // maybe this is not needed here.
						gtIndex++; // importante para depois preencher os FN
					}
					else {
						dist_1 = currentGT.getPosition() - currentEvent.getPosition();
						dist_2 = currentGT.getPosition() - nextEvent.getPosition();
						if(Math.abs(dist_1) <= Math.abs(dist_2)) {
							// se nao tiver este evento é um TP
							//System.out.println("TP porque existe nextEvent mas está mais longe do atual");
							pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
							//mFW.write(pmH.toString());
							eIndex ++;
							gtIndex ++;
						}
						else {
							if(nextEvent.getPosition() <= currentGT.getPosition()) {
								//FP
								//System.out.println("FP pois existe nextEvent e está mais perto da GT");
								pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
								//mFW.write(pmH.toString());
								eIndex ++;
							}
							else if(nextEvent.getPosition() > currentGT.getPosition() + tolerance) {
								// é um true positive
								//System.out.println("é um TP porque o next está mais longe");
								pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
								//mFW.write(pmH.toString());
								eIndex ++;
								gtIndex ++;
							}
							else if(nextEvent.getPosition() <= currentGT.getPosition() + tolerance) {
								// TP ou FP
								prevEvent = currentEvent;
								//System.out.println("pode ser TP ou FP");
								eIndex++;
								prevEvent = currentEvent;
							}
						}
					}
				}
				// CE is inside tolerance, but after CGT
				else if(currentEvent.getPosition() <= currentGT.getPosition() + tolerance &&
						currentEvent.getPosition() > currentGT.getPosition()) {
					if(prevEvent != null) {
						//System.out.println("prev is TP and this has to be evaluated next");
						// prevEvent is TP e este tem que ser avaliado contra o prox gt
						pmH.setValues(PerformanceMetricHelper.Options.TP, prevEvent.delta_111, prevEvent.getPosition(), currentGT.getPosition());
						//mFW.write(pmH.toString());
						gtIndex++;
						eIndex++;
						prevEvent = null;
					}
					else {
						//pmH.setValues(PerformanceMetricHelper.Options.FP, prevEvent.delta_111,  prevEvent.getPosition(), currentGT.getPosition());
						//mFW.write(pmH.toString());
						//gtIndex++;
						//eIndex++;
						prevEvent = currentEvent;
						//System.out.println("candidate to TP");
						// candidato a tp e o prev event is a FP
					}
				}
				// CE is before tolerance
				else if(currentEvent.getPosition() < currentGT.getPosition() - tolerance) {
					// SE EXISTE ALGUM ANTES É UM TP
					if(prevEvent != null) {
						// preEvent é TP
						//System.out.println("prev is TP");
						// prevEvent is TP e este tem que ser avaliado contra o prox gt
						pmH.setValues(PerformanceMetricHelper.Options.TP, prevEvent.delta_111, prevEvent.getPosition(), currentGT.getPosition());
						//mFW.write(pmH.toString());
						//gtIndex++;
						prevEvent = null;
					}
					// ESTE É UM FP
					pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
					//mFW.write(pmH.toString());
					eIndex++;
				}
				// CE is after tolerance
				else if(currentEvent.getPosition() > currentGT.getPosition() + tolerance) {
					// SE TEM ALGUM ANTES PASSA A SER TP
					
					if(prevEvent != null) {
						// preEvent é TP
						//System.out.println("prev is TP");
						// prevEvent is TP e este tem que ser avaliado contra o prox gt
						pmH.setValues(PerformanceMetricHelper.Options.TP, prevEvent.delta_111, prevEvent.getPosition(), currentGT.getPosition());
						//mFW.write(pmH.toString());
						eIndex++;
						gtIndex++;
						prevEvent = null;
					} else {
						// SE NAO ENTAO HOUVE UM FN
						//System.out.println("FN");
						pmH.setValues(PerformanceMetricHelper.Options.FN, deltas[gtIndex],  currentEvent.getPosition(), currentGT.getPosition());
						//mFW.write(pmH.toString());
						gtIndex++;
					}
				}
				else {
					System.out.println("this should never be reached");
				}
			}
			
			if(gtIndex < groundTruthEvents.length) {
				//System.out.println("more FN " + this.test_id + " " + tolerance);
				for(int i = gtIndex; i < groundTruthEvents.length; i++) {
					currentGT = this.groundTruthEvents[gtIndex];
					// need to check this delta
					pmH.setValues(PerformanceMetricHelper.Options.FN, deltas[gtIndex],  currentEvent.getPosition(), currentGT.getPosition());
					//mFW.write(pmH.toString());
					gtIndex ++;
				}
			}
			
			if( eIndex <  detectedEvents.length ) {
				//System.out.println("more FP " + this.test_id + " " + tolerance);
				for(int i = eIndex; i < detectedEvents.length; i++) {
					currentEvent = detectedEvents[eIndex];
					pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
					//mFW.write(pmH.toString());
					eIndex ++;
				}
			}
			// update the TN
			pmH.setValues(PerformanceMetricHelper.Options.TN, 0, this.file_size, 0);
			// print the last row here
			//mFW.write(pmH.toString());
		}
		else {
			// nao encontrou nada. é tudo falso negativo
			for(int i = 0; i < groundTruthEvents.length; i++) {
				currentGT = this.groundTruthEvents[gtIndex];
				//currentEvent = Long.min
				pmH.setValues(PerformanceMetricHelper.Options.FN, this.deltas[i],  0, currentGT.getPosition());
				//mFW.write(pmH.toString());
				gtIndex ++;
			}
		}
		
		
		//mFW_global.write(pmH.toString());
		mFW_global.write(pmH.toStringTotal());
	}
	
	private ExtendedDetectionPowerEventDTO getNextEvent(int eIndex) {
		if(eIndex >= detectedEvents.length)
			return null;
		return detectedEvents[eIndex];
	}
	/*
	public class EvaluationSample {
		public IPowerEvent gtpe;
		public IPowerEvent edpe;
		
		public EvaluationSample(IPowerEvent gtpe, IPowerEvent edpe) {
			this.gtpe = gtpe;
			this.edpe = edpe;
		}
	}*/
}
