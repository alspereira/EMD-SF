package emdsf.EventBased.Evaluation.EventDetection;

import java.io.IOException;

import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;
import util.database.dto.ExtendedDetectionPowerEventDTO;
import util.file.MyFileWriter;

public class MyPerformanceEvaluator {

	private IPowerEvent[] groundTruthEvents;
	private ExtendedDetectionPowerEventDTO[] detectedEvents;
	private double[] deltas;
	
	
	private MyFileWriter mFW;
	private MyFileWriter mFW_global;
	private PerformanceMetricHelper pmH;
	
	private int evaluation_id;
	private int test_id;
	
	private long file_size = 36755788l;
	 
	public MyPerformanceEvaluator(
			IPowerEvent[] groundTruthEvents, 
			ExtendedDetectionPowerEventDTO[] detectedEvents,
			double[] deltas,
			MyFileWriter mFW, 
			MyFileWriter mFW_global, 
			int evaluation_id, 
			int test_id) throws IOException {
		
		this.groundTruthEvents 	= groundTruthEvents;
		this.detectedEvents 	= detectedEvents;
		this.mFW = mFW;
		this.mFW_global = mFW_global;
		
		this.evaluation_id 	= evaluation_id;
		this.test_id 		= test_id;
		
		this.deltas 		= deltas;
	}
	
	public synchronized void eval(int tolerance) throws IOException {
		this.pmH = new PerformanceMetricHelper(evaluation_id, test_id, tolerance);
		//this.mFW.write(pmH.getHeader());
		
		int eIndex = 0;
		int gtIndex = 0;
		IPowerEvent currentGT; 
		ExtendedDetectionPowerEventDTO currentEvent;
		if(this.detectedEvents.length > 0) {
			currentGT = this.groundTruthEvents[0];
			currentEvent = this.detectedEvents[0];
			
			
			while(gtIndex < groundTruthEvents.length && eIndex < detectedEvents.length) {	
				
				currentGT = this.groundTruthEvents[gtIndex];
				currentEvent = detectedEvents[eIndex];
				
				if( currentEvent.getPosition() >= currentGT.getPosition() - tolerance
						&&
					currentEvent.getPosition() <= currentGT.getPosition() + tolerance
						) {
					
					pmH.setValues(PerformanceMetricHelper.Options.TP, currentEvent.delta_111, currentEvent.getPosition(), currentGT.getPosition());
					mFW.write(pmH.toString());
					eIndex ++;
					gtIndex ++;
				}
				else {
					if( currentEvent.getPosition() < currentGT.getPosition() - tolerance) {
						pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
						mFW.write(pmH.toString());
						eIndex ++;
					}
					else { // currentEvent.getPosition() > currentGT.getPosition() + tolerance
						pmH.setValues(PerformanceMetricHelper.Options.FN, deltas[gtIndex],  currentEvent.getPosition(), currentGT.getPosition());
						mFW.write(pmH.toString());
						gtIndex ++;
					}
				}
			}
			
			if(gtIndex < groundTruthEvents.length) {
				//System.out.println("more FN " + this.test_id + " " + tolerance);
				for(int i = gtIndex; i < groundTruthEvents.length; i++) {
					currentGT = this.groundTruthEvents[gtIndex];
					// need to check this delta
					pmH.setValues(PerformanceMetricHelper.Options.FN, deltas[gtIndex],  currentEvent.getPosition(), currentGT.getPosition());
					mFW.write(pmH.toString());
					gtIndex ++;
				}
			}
			
			if( eIndex <  detectedEvents.length ) {
				//System.out.println("more FP " + this.test_id + " " + tolerance);
				for(int i = eIndex; i < detectedEvents.length; i++) {
					currentEvent = detectedEvents[eIndex];
					pmH.setValues(PerformanceMetricHelper.Options.FP, currentEvent.delta_111,  currentEvent.getPosition(), currentGT.getPosition());
					mFW.write(pmH.toString());
					eIndex ++;
				}
			}
			// update the TN
			pmH.setValues(PerformanceMetricHelper.Options.TN, 0, this.file_size, 0);
			// print the last row here
			mFW.write(pmH.toString());
		}
		else {
			// nao encontrou nada. é tudo falso negativo
			for(int i = 0; i < groundTruthEvents.length; i++) {
				currentGT = this.groundTruthEvents[gtIndex];
				//currentEvent = Long.min
				pmH.setValues(PerformanceMetricHelper.Options.FN, this.deltas[i],  0, currentGT.getPosition());
				mFW.write(pmH.toString());
				gtIndex ++;
			}
		}
		
		
		//mFW_global.write(pmH.toString());
		mFW_global.write(pmH.toStringTotal());
	}
	
	public class EvaluationSample {
		public IPowerEvent gtpe;
		public IPowerEvent edpe;
		
		public EvaluationSample(IPowerEvent gtpe, IPowerEvent edpe) {
			this.gtpe = gtpe;
			this.edpe = edpe;
		}
	}
}
