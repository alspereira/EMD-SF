package emdsf.EventBased.Evaluation.EventDetection;

public class _PerformanceMetricHelper {
	
	public int TP = 0, FP = 0 , TN = 0, FN = 0;
	public int GT = 0; // this should be the same as TP + FN
	public float powerChange = 0;
	
	private double TPC_FN = 0; // total power change of missed events
	private double TPC_FP = 0;
	public double APC_FN = 0;
	public double APC_FP = 0;
	
	// E = TP + FN

	public _PerformanceMetricHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public void updateTPC_FN(double value) {
		this.TPC_FN += Math.abs(value);
	}
	
	public void updateTPC_FP(double value) {
		this.TPC_FP += Math.abs(value);
	}
	
	public void updateAPC_FN(double value) {
		this.APC_FN += Math.abs(value);
	}
	
	public void updateAPC_FP(double value) {
		this.APC_FP += Math.abs(value);
	}
	
	public double getP() {
		return TP / (TP + FP);
	}
	
	public double getR() {
		return TP / (TP + FN);
	}
	
	public double getFScore() {
		return (2 * TP) / ( (2 * TP) + FP + FN );
	}
	
	// Same as Sensitivity / Recall (TPR = Sensitivity = Recall)
	public double getTPR() {
		return TP / (TP + FN);
	}
	
	public double getFPR() {
		return FP / (FP + TN);
	}
	
	public double getTPP() {
		return TP / GT;
	}
	
	public double getFPP() {
		return FP / GT;
	}
	
	public double getTPC_FN() {
		return TPC_FN;
	}
	
	public double getTPC_FP() {
		return TPC_FP;
	}
	
	public double getAPC_FN() {
		return APC_FN;
	}
	
	public double getAPC_FP() {
		return APC_FP;
	}
	
	public double getP_R() {
		return Math.pow(getP(), 2) + Math.pow(getR(), 2) - (2 * getP()) - (2 * getR()) + 2;
	}
	
	public double getSpecificity() {
		return 0;
	}
	
	// Combination of TRP and FPR
	public double getTPR_FPR() {
		//return Math.pow(getFalsePositiveRate(), 2) + Math.pow(getTruePositiveRate(), 2) - (2 * getTruePositiveRate()) + 1;
		
		return Math.pow(getTPR(), 2) - (2 * getTPR()) + 1;
	}
	
	public double getTPP_FPP() {
		return Math.pow(getFPP(), 2) + Math.pow(getTPP(), 2) - (2 * getTPP()) + 1;
	}
	
	public double getTPC_FN__TPC_FP() {
		return Math.pow(getTPC_FN(), 2) + Math.pow(getTPC_FP(), 2);
	}
	
	public double getAPC_FN__APC_FP() {
		return Math.pow(getAPC_FN(), 2) + Math.pow(getAPC_FP(), 2);
	}
}
