package emdsf.EventBased.Evaluation.EventDetection;

import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;
//import jdbc.dao.PowerEventEvalDAO;
//import jdbc.dao.PowerEventEvalTotalDAO;

public class Evaluator3 {

	private IPowerEvent[] groundTruthEvents;
	private IPowerEvent[] detectedEvents;
	
	public int tp, fp, fn, tfp, tfn;
	 
	public Evaluator3(IPowerEvent[] groundTruthEvents, IPowerEvent[] detectedEvents) {
		this.groundTruthEvents 	= groundTruthEvents;
		this.detectedEvents 	= detectedEvents;		
	}
	
	public void eval(int tolerance) {
		IPowerEvent currentGT = this.groundTruthEvents[0];
		IPowerEvent currentEvent;
		
		int eIndex = 0;
		int gtIndex = 0;
		
		tp = 0; fn = 0; fp = 0; tfp = 0; tfn = 0; 
		
		while(gtIndex < groundTruthEvents.length && eIndex < detectedEvents.length) {	
			
			currentGT = this.groundTruthEvents[gtIndex];
			currentEvent = detectedEvents[eIndex];
			
			if( currentEvent.getPosition() >= currentGT.getPosition() - tolerance
					&&
				currentEvent.getPosition() <= currentGT.getPosition() + tolerance
					) {
				tp++;
				eIndex ++;
				gtIndex ++;
			}
			else {
				if( currentEvent.getPosition() < currentGT.getPosition() - tolerance) {
					fp++;
					eIndex ++;
				}
				else { // currentEvent.getPosition() > currentGT.getPosition() + tolerance
					fn++;
					gtIndex ++;
				}
			}
		}
		
		if(gtIndex < groundTruthEvents.length) {
			for(int i = 0; i < groundTruthEvents.length; i++) {
				fn++;
			}
		}
		
		if( eIndex <  detectedEvents.length ) {
			for(int i = 0; i < detectedEvents.length; i++) {
				fp++;
			}
		}
	}
	
	public class EvaluationSample {
		public IPowerEvent gtpe;
		public IPowerEvent edpe;
		
		public EvaluationSample(IPowerEvent gtpe, IPowerEvent edpe) {
			this.gtpe = gtpe;
			this.edpe = edpe;
		}
	}
}
