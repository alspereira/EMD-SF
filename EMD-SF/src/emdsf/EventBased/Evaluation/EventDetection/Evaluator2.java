package emdsf.EventBased.Evaluation.EventDetection;

import java.util.ArrayList;
import java.util.List;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;
import emdsf.EventBased.Evaluation.DTO.GTPowerEventDTO;

public class Evaluator2 {

	private IPowerEvent[] groundTruthEvents;
	
	private IPowerEvent[] detectedEvents;
	
	private List<EvaluationSample> 	truePositives;
	private List<EvaluationSample> 	falseNegative;
	private List<IPowerEvent>		falsePositives;
	private int						trueNegatives;
	private int 					gtIndex;

	public Evaluator2(IPowerEvent[] groundTruthEvents, IPowerEvent[] detectedEvents) {
		this.groundTruthEvents 	= groundTruthEvents;
		this.detectedEvents 	= detectedEvents;
		
		this.truePositives 		= new ArrayList<EvaluationSample>();
		this.falseNegative 		= new ArrayList<EvaluationSample>();
		this.falsePositives 	= new ArrayList<IPowerEvent>();
		this.gtIndex 			= 0;
	}
	
	public int tp = 0, fp = 0, fn = 0;
	
	public void eval(int tolerance) {
		IPowerEvent currentGT = this.groundTruthEvents[0];
		IPowerEvent currentEvent;
		
		int eIndex = 0;
		int gtIndex = 0;
		
		while(gtIndex < groundTruthEvents.length && eIndex < detectedEvents.length) {
			
			currentGT = this.groundTruthEvents[gtIndex];
			currentEvent = detectedEvents[eIndex];
			
			//System.out.println ("GT INDEX: " + gtIndex + " GT POS: "  + currentGT.getPosition() + " " + (currentGT.getPosition() - tolerance) + " " + (currentGT.getPosition() + tolerance));
			//System.out.println ("E INDEX: " + eIndex + " E POS: "  + currentEvent.getPosition());
			
			
			if( currentEvent.getPosition() >= currentGT.getPosition() - tolerance
					&&
				currentEvent.getPosition() < currentGT.getPosition() + tolerance
					) {
				//System.out.println("TP: " + ++tp);
				tp++;
				eIndex ++;
				gtIndex ++;
			}
			else {
				if( currentEvent.getPosition() < currentGT.getPosition() - tolerance) {
					// some FP here
					//System.out.println("FP: " + ++fp);
					fp++;
					eIndex ++;
				}
				else { // currentEvent.getPosition() > currentGT.getPosition() + tolerance
					// FN here, but this event can be good in the next iteration
					//System.out.println("FN: " + ++fn);
					fn++;
					gtIndex ++;
				}
			}
		}
		System.out.println("----");
		System.out.println("E: " + detectedEvents.length + " GT: " + groundTruthEvents.length + " TP: " + tp + " FP: " + fp + " FN: " + fn);
		System.out.println("EIndex: " + eIndex + " GTIndex: " + gtIndex);
		//System.exit(-1);
	}
	
	public class EvaluationSample {
		public IPowerEvent gtpe;
		public IPowerEvent edpe;
		
		public EvaluationSample(IPowerEvent gtpe, IPowerEvent edpe) {
			this.gtpe = gtpe;
			this.edpe = edpe;
		}
	}

	public List<EvaluationSample> getTruePositives() {
		return truePositives;
	}

	public List<EvaluationSample> getFalseNegative() {
		return falseNegative;
	}

	public List<IPowerEvent> getFalsePositives() {
		return falsePositives;
	}

	public int getTrueNegatives() {
		return trueNegatives;
	}
	
	

}
