package emdsf.EventBased.Evaluation.EventDetection;

import java.util.ArrayList;
import java.util.List;

public abstract class AEventDetectionEvaluation {
	
	protected enum SampleType {
		TP, FP, TN, FN, RFP
	}
	
	protected List<EvaluationSample> evaluationSamples;

	protected AEventDetectionEvaluation() {
		evaluationSamples = new ArrayList<EvaluationSample>();
	}
	
	protected void addEvaluationSample(long eventPosition, long groundTruthPosition, float metricValue, SampleType type) {
		switch(type) {
			case TP:
				this.evaluationSamples.add(new EvaluationSample(eventPosition, groundTruthPosition, metricValue, 
						true, false, false, false, false));
				break;
			case FN:;
				this.evaluationSamples.add(new EvaluationSample(eventPosition, groundTruthPosition, metricValue,
						false, false, false, true, false));
				break;
			case FP:
				this.evaluationSamples.add(new EvaluationSample(eventPosition, groundTruthPosition, metricValue,
						false, true, false, false, false));
				break;
			case TN:
				this.evaluationSamples.add(new EvaluationSample(eventPosition, groundTruthPosition, metricValue,
						false, false, true, false, false));
				break;
			case RFP:
				this.evaluationSamples.add(new EvaluationSample(eventPosition, groundTruthPosition, metricValue,
						false, true, false, false, true));
				break;
			default:
				break;
		}
	}

	protected class  EvaluationSample {
		long eventPosition;
		long groundTruthPosition;
		float metricValue;
		boolean isTP, isFP, isTN, isFN, isRFP;
		
		public EvaluationSample(long eventPosition, long groundTruthPosition, float metricValue, 
				boolean isTP, boolean isFP, boolean isTN, boolean isFN, boolean isRFP) {
			this.eventPosition = eventPosition;
			this.groundTruthPosition = groundTruthPosition;
			this.metricValue = metricValue;
			this.isTP = isTP;
			this.isFP = isFP;
			this.isTN = isTN;
			this.isFN = isFN;
			this.isRFP = isRFP;
		}
		public String getHeader() {
			String out = "";
			///out = "ID;DS;P;TP;FP;FN;ds_TP;ds_FP;ds_FN;PosGT;isTP;isFP;isFN\n"
			out = "POS;GT_POS;MetricValue;TP;FN;FP;TFP,NFP\n";
			return out;
		}
		
		public String toString() {
			String out = "";
			out = Long.toString(eventPosition)+";"
					+Long.toString(groundTruthPosition)+";"
					+Float.toString(metricValue)+";"
					+isTP + ";" + isFN + ";" + isFP + ";" + isRFP + ";" + !isRFP; 
			return out;
		}
	}
}