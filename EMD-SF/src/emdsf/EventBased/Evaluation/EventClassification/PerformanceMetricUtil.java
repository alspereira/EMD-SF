package emdsf.EventBased.Evaluation.EventClassification;

public class PerformanceMetricUtil {
	
	int runningAccuracy;
	int runningPrecision;

	public static double Accuracy(double tp, double tn, double fp, double fn) {
		double o = 0;
		if(tp + fp + tn + fn == 0)
			o = 0;
		else
			o = (tp + tn) / (tp + fp + tn + fn);
		
		return o;
	} 
	
	public static double ErrorRate(double tp, double tn, double fp, double fn) {
		double o = 0;
		if(tp + fp + tn + fn == 0)
			o = 0;
		else
			o =(fp + fn) / (tp + fp + tn + fn);
		return o;
	}
	
	// Positive Predicted Value
	public static double Precision(double tp, double fp) {
		double o = 0;
		if (tp + fp == 0)
			o = 0;
		else
			o = tp / (tp + fp);
		return o;
	}
	
	// TPR, Sensitivity
	public static double Recall(double tp, double fn) {
		double o = 0;
		if (tp + fn == 0)
			o = 0;
		else
			o = tp / (tp + fn);
		return o;
	}
	
	// specificity -> True Negative Rate
	public static double getTNR(double tn, double fp) {
		double o = 0;
		if(tn + fp > 0)
			o = tn / (tn + fp);
		return o;
	}
	
	// False Discovery Rate -> 1 - Positive Predicted Value (Precision)
	public static double getFDR(double tp, double fp) {
		double o = 0;
		if(tp + fp > 0)
			o = fp / (fp + tp);
		return o;
	}
	
	public static double getFPR(double fp, double tn) {
		double o = 0;
		if(fp + tn > 0)
			o = fp / (fp + tn);
		return o;
	}
	
	public static double getFNR(double fn, double tp) {
		double o = 0;
		if(fn + tp > 0)
			o = fn / (fn + tp);
		return o;
	}
	
	public static double FbScore(double b, double p, double r) {
		double o;
		if(p + r == 0)
			o = 0;
		else
			o = ((Math.pow(b, 2) + 1) * p * r) / (Math.pow(b, 2) * p + r);
		return o;
	}
	
	// AUC discrete aka AUC_ROC
	public static double WFitness(double tpr, double tnr) {
		return (tpr + tnr) * 0.5;
	}
	
	public static double GFitness(double tpr, double tnr) {
		return Math.sqrt(tpr * tnr);
	}
	
	// a = sensitivity b = specificity
	public static double BFitness(double tpr, double tnr, boolean targetIsMajority) {
		double bias;
		if(targetIsMajority)
			bias = (double) tpr / 2;
		else
			bias = (double) tnr / 2;
		return 0.5 * tpr * tnr + bias;
	}
	
	public static double WFitnessB(double tpr, double tnr) {
		return WFitness(tpr, tnr) * (1 - Math.abs(tpr - tnr));
	}
	
	private static double DTP11(double a, double b) {
		return Math.pow(a, 2) + Math.pow(b, 2) - (2 * a) - (2 * b) + 2;
	}
	
	private static double DTP10(double a, double b) {
		return Math.pow(a, 2) + Math.pow(b, 2) - (2 * a) + 1;
	}
	
	private static double DTP00(double a, double b) {
		return Math.pow(a, 2) + Math.pow(b, 2);
	}
	
	public static double DTPpr(double p, double r) {
		return DTP11(p,r);
	}
	
	public static double DTPrate(double tpr, double fpr) {
		return DTP10(tpr,fpr);
	}
	
	public static double DTPperc(double pctC, double pctI) {
		return DTP10(pctC, pctI);
	}
	
	public static double DTPpc(double pc_fp, double pc_fn) {
		return DTP00(pc_fp, pc_fn);
	}
	
	public static double TPP(double tp, double fn) {
		double o = 0;
		if(tp + fn > 0)
			o = tp / (tp + fn);
		return o;
	}
	
	public static double FPP(double fp, double tp, double fn) {
		double o = 0;
		if(tp + fn > 0)
			o = fp / (tp + fn);
		return o;
	}
	
	public static double MCC(double tp, double fp, double tn, double fn) {
		double o = 0;
		double n = (tp * tn) - (fp * fn);
		double d = (tp + fp) * (tp + fn) * (tn + fp) * (tn + fn);
		if(d > 0) {
			o =  n / Math.sqrt(d);
		}
		return o;
	}
}
