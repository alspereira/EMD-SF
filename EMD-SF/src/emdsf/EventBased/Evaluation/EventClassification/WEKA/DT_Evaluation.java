package emdsf.EventBased.Evaluation.EventClassification.WEKA;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.Util;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier.EC_DT;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier.TreeType;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureType;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;

public class DT_Evaluation extends AEvaluation {
	
	private List<int[]> folds;
	private EvaluationFeatures[] featureSets;
	private Instances	data;
	private int[] params;
	
	Map<EvaluationFeatures, Map<Integer, Evaluation>> out;
	
	public DT_Evaluation(Instances data, int[] params, List<int[]> folds, EvaluationFeatures[] featureSets, String file_path, double[] averagePowerChange) throws IOException {
		super(file_path, averagePowerChange);
		this.data 			= data;
		this.params			= params;
		this.folds 			= folds;
		this.featureSets	= featureSets;
	}
	
	// in -> train and test instances
	public void eval(TreeType type) throws Exception {
		Instances train = null;
		Instances test = null;
		EC_DT la;
		int fold_index;
		FeatureType[] ft;
		int[] indexes;
		int[] feature_indexes = new int[0];
		Map<String, String> header;
		for(int r = 0; r < runs; r++) {
			System.out.println("ROUND: " + (r+1));
			for(int p = 0; p < params.length; p++ ) {
				fold_index = 0;
				for (int[] fold : folds) {
					fold_index ++;
					indexes = Util.getInstanceIndexes(data, data.numAttributes() - 3, fold);
					test = Util.getIndexes(data, indexes);
					train = Util.removeIndexes(data, indexes);	
					// remove unnecessary attributes
					super.removeLocationAndApplianceID(train);
					super.removeLocationAndApplianceID(test);
					for (EvaluationFeatures ef : featureSets) {
						ft = ef.getFeatureType();
						feature_indexes = new int[0];
						for(FeatureType f : ft)
							feature_indexes = ArrayUtils.addAll(feature_indexes, f.getFeatureIndexes());
						
						la = new EC_DT(
								Util.removeAttributes(train, feature_indexes), 
								Util.removeAttributes(test, feature_indexes));
						Evaluation eval;
						header = new LinkedHashMap<String, String>();
						if(type == TreeType.REP) { // REP
							eval = la.evalREP(params[p]);
							header.put("RUN", Integer.toString(r+1));
							header.put("MAX_DEPTH", Integer.toString(params[p]));
							header.put("FOLD", Integer.toString(fold_index));
							header.put("FEATURES", ef.toString());
							header.put("TRAIN_ET", Double.toString(la.elapsedTimeTrain));
							header.put("TEST_ET", Double.toString(la.elapsedTimeTest));
							super.writeToFile(header, eval);
						}	
						else if(type == TreeType.J48) {
							eval = la.evalJ48(params[p]);
							header.put("RUN", Integer.toString(r+1));
							header.put("MIN_OBJECTS", Integer.toString(params[p]));
							header.put("FOLD", Integer.toString(fold_index));
							header.put("FEATURES", ef.toString());
							header.put("TRAIN_ET", Double.toString(la.elapsedTimeTrain));
							header.put("TEST_ET", Double.toString(la.elapsedTimeTest));
							super.writeToFile(header, eval);
						}
					}
				}
			}
		}
		super.close();
	}
}