package emdsf.EventBased.Evaluation.EventClassification.WEKA;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.Util;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier.EC_KStar;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureType;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;

public class KStar_Evaluation extends AEvaluation {
	
	private int[] blends;
	private List<int[]> folds;
	private EvaluationFeatures[] featureSets;
	private Instances	data;
	
	Map<EvaluationFeatures, Map<Integer, Evaluation>> out;
	
	public KStar_Evaluation(Instances data, int[] blends, List<int[]> folds, EvaluationFeatures[] featureSets, String file_path, double[] averagePowerChange) throws IOException {
		super(file_path, averagePowerChange);
		this.data 			= data;
		this.blends			= blends;
		this.folds 			= folds;
		this.featureSets	= featureSets;
	}
	
	// in -> train and test instances
	public void eval() throws Exception {
		Instances train = null;
		Instances test = null;
		EC_KStar la;
		int fold_index;
		FeatureType[] ft;
		int[] indexes;
		int[] feature_indexes = new int[0];
		Map<String, String> header;
		for(int r = 8; r < runs-1; r++) {
			System.out.println("Round: " + r);
			for(int b = 0; b < blends.length; b++ ) {
				fold_index = 0;
				for (int[] fold : folds) {
					fold_index ++;
					indexes = Util.getInstanceIndexes(data, data.numAttributes() - 3, fold);
					test = Util.getIndexes(data, indexes);
					train = Util.removeIndexes(data, indexes);	
					// remove unnecessary attributes
					super.removeLocationAndApplianceID(train);
					super.removeLocationAndApplianceID(test);
					for (EvaluationFeatures ef : featureSets) {
						ft = ef.getFeatureType();
						feature_indexes = new int[0];
						for(FeatureType f : ft)
							feature_indexes = ArrayUtils.addAll(feature_indexes, f.getFeatureIndexes());
						
						la = new EC_KStar(
								Util.removeAttributes(train, feature_indexes), 
								Util.removeAttributes(test, feature_indexes));
						Evaluation eval = la.eval(blends[b]);
						header = new LinkedHashMap<String, String>();
						header.put("RUN", Integer.toString(r+1));
						header.put("B", Integer.toString(blends[b]));
						header.put("FOLD", Integer.toString(fold_index));
						header.put("FEATURES", ef.toString());
						header.put("TRAIN_ET", Double.toString(la.elapsedTimeTrain));
						header.put("TEST_ET", Double.toString(la.elapsedTimeTest));
						super.writeToFile(header, eval);
					}
				}
			}
		}
		super.close();
	}
}
