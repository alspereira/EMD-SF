package emdsf.EventBased.Evaluation.EventClassification.WEKA;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathArrays;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.Util;
import emdsf.EventBased.Evaluation.EventClassification.PerformanceMetricUtil;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;

public abstract class AEvaluation {
	
	private FileWriter fileWriter = null;
	private CSVPrinter csvFilePrinter = null;
	private CSVFormat csvFileFormat = CSVFormat.EXCEL.withRecordSeparator("\n").withDelimiter(';');
	
	protected int runs = 10;
	
	private double[] averagePowerChange;
	
	public AEvaluation(String file_name, double[] averagePowerChange) throws IOException {
		fileWriter = new FileWriter(file_name);
        csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
        this.averagePowerChange = averagePowerChange;
	}
	
	private double getWeighted(List<Map<String, Double>> ia_results, String metric, double[] weights) {
		double o = 0;
		double sumWeights = StatUtils.sum(weights);
		//Iterator<Map<String, Double>> it = ia_results.iterator();
		for(int i = 0; i < weights.length; i++) {
			o += (ia_results.get(i).get(metric)) * weights[i];
		}
		return o / sumWeights;
	}
	
	private Map<String, Double> getWeightMetrics(List<Map<String, Double>> ia_results, Evaluation eval) {
		Map<String, Double> temp = new LinkedHashMap<String, Double>();
		temp.put("P", eval.weightedPrecision());
		temp.put("R", eval.weightedRecall());
		temp.put("A", this.getWeighted(ia_results, "A", Util.getClassCounts(eval)));
		temp.put("E", this.getWeighted(ia_results, "E", Util.getClassCounts(eval)));
		temp.put("TPR", eval.weightedTruePositiveRate());
		temp.put("FPR", eval.weightedFalsePositiveRate());
		temp.put("TNR", eval.weightedTrueNegativeRate());
		temp.put("FNR", eval.weightedFalseNegativeRate());
		temp.put("FDR", this.getWeighted(ia_results, "FDR", Util.getClassCounts(eval)));
		temp.put("TPP", this.getWeighted(ia_results, "TPP", Util.getClassCounts(eval)));
		temp.put("FPP", this.getWeighted(ia_results, "FPP", Util.getClassCounts(eval)));
		temp.put("F1",eval.weightedFMeasure());
		temp.put("F05", PerformanceMetricUtil.FbScore(0.5, eval.weightedPrecision(), eval.weightedRecall()));
		temp.put("F2", PerformanceMetricUtil.FbScore(2, eval.weightedPrecision(), eval.weightedRecall()));
		temp.put("MCC", this.getWeighted(ia_results, "MCC", Util.getClassCounts(eval)));
		temp.put("MMC_2", eval.weightedMatthewsCorrelation());
		temp.put("StdMCC", this.getStdMCC(eval.weightedMatthewsCorrelation()));
		
		temp.put("AUC_PR", eval.weightedAreaUnderPRC());
		temp.put("AUC_ROC", eval.weightedAreaUnderROC());
		
		temp.put("WFIT", this.getWeighted(ia_results, "WFIT", Util.getClassCounts(eval)));
		temp.put("GFIT", this.getWeighted(ia_results, "GFIT", Util.getClassCounts(eval)));
		temp.put("WFITB", this.getWeighted(ia_results, "WFITB", Util.getClassCounts(eval)));
		temp.put("DTPpr", this.getWeighted(ia_results, "DTPpr", Util.getClassCounts(eval)));
		temp.put("DTPperc", this.getWeighted(ia_results, "DTPperc", Util.getClassCounts(eval)));
		temp.put("DTPrate", this.getWeighted(ia_results, "DTPrate", Util.getClassCounts(eval)));
		temp.put("TPC_FP", this.getWeighted(ia_results, "TPC_FP", Util.getClassCounts(eval)));
		temp.put("TPC_FN", this.getWeighted(ia_results, "TPC_FN", Util.getClassCounts(eval)));
		temp.put("APC_FP", this.getWeighted(ia_results, "APC_FP", Util.getClassCounts(eval)));
		temp.put("APC_FN", this.getWeighted(ia_results, "APC_FN", Util.getClassCounts(eval)));
		temp.put("DPTtpc", this.getWeighted(ia_results, "DTPtpc", Util.getClassCounts(eval)));
		temp.put("DTPapc", this.getWeighted(ia_results, "DTPapc", Util.getClassCounts(eval)));
		return temp;
	}
	
	private Map<String, Double> getMacroMetrics(List<Map<String, Double>> aa_results) {
		Iterator<Map<String, Double>> it = aa_results.iterator();
		Map<String, Double> temp;
		double[] macro_metric = new double[25];
		double class_count = 0;
		while(it.hasNext()) {
			temp = it.next();
			// check if class is represented in the test in order to compute P and R
			if(temp.get("TP") + temp.get("FN") > 0) {
				macro_metric[0] += temp.get("P");
				macro_metric[1] += temp.get("R");
				macro_metric[2] += temp.get("A");
				macro_metric[3] += temp.get("E");
				macro_metric[4] += temp.get("TPR");
				macro_metric[5] += temp.get("FPR");
				macro_metric[6] += temp.get("TNR");
				macro_metric[7] += temp.get("FNR");
				macro_metric[8] += temp.get("FDR");
				macro_metric[9] += temp.get("AUC_PR");
				macro_metric[10] += temp.get("AUC_ROC");
				macro_metric[11] += temp.get("MCC");
				macro_metric[12] += temp.get("WFIT");
				macro_metric[13] += temp.get("GFIT");
				macro_metric[14] += temp.get("WFITB");
				macro_metric[15] += temp.get("DTPpr");
				macro_metric[16] += temp.get("DTPperc");
				macro_metric[17] += temp.get("DTPrate");
				macro_metric[18] += temp.get("F1");
				macro_metric[19] += temp.get("TPC_FP");
				macro_metric[20] += temp.get("TPC_FN");
				macro_metric[21] += temp.get("APC_FP");
				macro_metric[22] += temp.get("APC_FN");
				macro_metric[23] += temp.get("TPP");
				macro_metric[24] += temp.get("FPP");
				class_count ++;
			}
		}
		MathArrays.scaleInPlace(1 / class_count, macro_metric);
		
		temp = new LinkedHashMap<String, Double>();
		temp.put("P", macro_metric[0]);
		temp.put("R", macro_metric[1]);
		temp.put("A", macro_metric[2]);
		temp.put("E", macro_metric[3]);
		temp.put("TPR", macro_metric[4]);
		temp.put("FPR", macro_metric[5]);
		temp.put("TNR", macro_metric[6]);	
		temp.put("FNR", macro_metric[7]);
		temp.put("FDR", macro_metric[8]);
		temp.put("TPP", macro_metric[23]);
		temp.put("FPP", macro_metric[24]);
		temp.put("AUC_PR", macro_metric[9]);
		temp.put("AUC_ROC", macro_metric[10]);
		temp.put("MCC", macro_metric[11]);
		temp.put("StdMCC", this.getStdMCC(temp.get("MCC")));
		temp.put("F1", PerformanceMetricUtil.FbScore(1, macro_metric[0],  macro_metric[1])); // maybe i don't have to compute this
		temp.put("F1_avg", macro_metric[18]);
		temp.put("F05", PerformanceMetricUtil.FbScore(0.5, macro_metric[0],  macro_metric[1]));
		temp.put("F2", PerformanceMetricUtil.FbScore(2, macro_metric[0],  macro_metric[1]));
		temp.put("WFIT", macro_metric[12]);
		temp.put("GFIT", macro_metric[13]);
		temp.put("WFITB", macro_metric[14]);
		temp.put("DTPpr", macro_metric[15]);
		temp.put("DTPperc", macro_metric[16]);
		temp.put("DTPrate", macro_metric[17]);
		temp.put("TPC_FP", macro_metric[18]);
		temp.put("TPC_FN", macro_metric[19]);
		temp.put("APC_FP", macro_metric[20]);
		temp.put("APC_FN", macro_metric[21]);
		temp.put("DTPtpc", PerformanceMetricUtil.DTPpc(macro_metric[18], macro_metric[19]));
		temp.put("DTPapc", PerformanceMetricUtil.DTPpc(macro_metric[20], macro_metric[21]));
		return temp;
	}
	
	private Map<String, Double> getMicroMetrics(List<Map<String, Double>> ia_metrics) {
		Iterator<Map<String, Double>> it = ia_metrics.iterator();
		Map<String, Double> temp;
		double[] cm = new double[8];
		while(it.hasNext()) {
			temp = it.next(); 
			cm[0] += temp.get("TP");
			cm[1] += temp.get("FP");
			cm[2] += temp.get("TN");
			cm[3] += temp.get("FN");
			cm[4] += temp.get("TPC_FP");
			cm[5] += temp.get("TPC_FN");
			cm[6] += temp.get("APC_FP");
			cm[7] += temp.get("APC_FN");
		}
		temp = new LinkedHashMap<String, Double>();
		temp.put("TP", cm[0]);
		temp.put("FP", cm[1]);
		temp.put("TN", cm[2]);
		temp.put("FN", cm[3]);
		temp.put("TPP", PerformanceMetricUtil.TPP(cm[0], cm[3]));
		temp.put("FPP", PerformanceMetricUtil.FPP(cm[1], cm[0], cm[3]));
		temp.put("TPR", PerformanceMetricUtil.Recall(cm[0], cm[3]));
		temp.put("FPR", PerformanceMetricUtil.getFPR(cm[1], cm[2]));
		temp.put("TNR", PerformanceMetricUtil.getTNR(cm[2], cm[1]));
		temp.put("FNR", PerformanceMetricUtil.getFNR(cm[3], cm[0]));
		temp.put("FDR", PerformanceMetricUtil.getFDR(cm[0], cm[1]));
		temp.put("A",PerformanceMetricUtil.Accuracy(cm[0], cm[2], cm[1], cm[3]));
		temp.put("E", PerformanceMetricUtil.ErrorRate(cm[0], cm[2], cm[1], cm[3]));
		
		temp.put("MCC", PerformanceMetricUtil.MCC(cm[0], cm[1], cm[2], cm[3]));
		temp.put("StdMCC",this.getStdMCC(temp.get("MCC")));
		
		temp.put("F1", PerformanceMetricUtil.FbScore(1, PerformanceMetricUtil.Precision(cm[0], cm[1]), PerformanceMetricUtil.Recall(cm[0], cm[3])));
		
		temp.put("WFIT", PerformanceMetricUtil.WFitness(temp.get("TPR"), temp.get("TNR")));
		temp.put("GFIT", PerformanceMetricUtil.GFitness(temp.get("TPR"), temp.get("TNR")));
		
		// BFITNESS -> how to calculate majority class?!
		temp.put("WFITB", PerformanceMetricUtil.WFitnessB(temp.get("TPR"), temp.get("TNR")));
		
		temp.put("DTPpr", PerformanceMetricUtil.DTPpr(temp.get("F1"), temp.get("F1")));
		temp.put("DTPperc", PerformanceMetricUtil.DTPperc(temp.get("TPP"), temp.get("FPP")));
		temp.put("DTPrate", PerformanceMetricUtil.DTPrate(temp.get("TPR"), temp.get("FPR")));
		temp.put("TPC_FP", cm[4] / 11);
		temp.put("TPC_FN", cm[5] / 11);
		temp.put("APC_FP", cm[6] / 11);
		temp.put("APC_FN", cm[7] / 11);
		temp.put("DTPtpc", PerformanceMetricUtil.DTPpc(cm[4] / 11, cm[5] / 11));
		temp.put("DTPapc", PerformanceMetricUtil.DTPpc(cm[6] / 11, cm[7] / 11));
		
		return temp;
	}
	
	private List<Map<String, Double>> getIndividualClassMetrics(Evaluation eval) {
		Map<String, Double> temp;
		List<Map<String, Double>> ia_metrics = new ArrayList<Map<String, Double>>();
		double tp, fp, tn, fn;
		for (int i = 0; i < 11; i++) {
			tp = eval.numTruePositives(i);
			fp = eval.numFalsePositives(i);
			tn = eval.numTrueNegatives(i);
			fn = eval.numFalseNegatives(i);
			
			temp = new LinkedHashMap<String, Double>();
			temp.put("TP", tp);
			temp.put("FP", fp);
			temp.put("TN", tn);
			temp.put("FN", fn);
			temp.put("TPR", eval.truePositiveRate(i));
			temp.put("FPR", eval.falsePositiveRate(i));
			temp.put("TNR", eval.trueNegativeRate(i));
			temp.put("FNR", eval.falseNegativeRate(i));
			temp.put("FDR", PerformanceMetricUtil.getFDR(tp, fp));
			temp.put("TPP", PerformanceMetricUtil.TPP(tp, fn));
			temp.put("FPP", PerformanceMetricUtil.FPP(fp, tp, fn));
			temp.put("P", eval.precision(i));
			temp.put("R", eval.recall(i));
			temp.put("A", PerformanceMetricUtil.Accuracy(tp, tn, fp, fn));	
			temp.put("E", PerformanceMetricUtil.ErrorRate(tp, tn, fp, fn));
			temp.put("F1", PerformanceMetricUtil.FbScore(1, eval.precision(i), eval.recall(i)));
			temp.put("F05", PerformanceMetricUtil.FbScore(0.5, eval.precision(i), eval.recall(i)));
			temp.put("F2", PerformanceMetricUtil.FbScore(2, eval.precision(i), eval.recall(i)));
			
			temp.put("MCC", eval.matthewsCorrelationCoefficient(i));
			temp.put("StdMCC",this.getStdMCC(temp.get("MCC")));
			
			temp.put("AUC_PR", eval.areaUnderPRC(i));
			temp.put("AUC_ROC", eval.areaUnderROC(i));
			temp.put("WFIT", PerformanceMetricUtil.WFitness(eval.truePositiveRate(i), eval.trueNegativeRate(i)));
			temp.put("GFIT", PerformanceMetricUtil.GFitness(eval.truePositiveRate(i), eval.trueNegativeRate(i)));
			// BFITNESS
			temp.put("WFITB", PerformanceMetricUtil.WFitnessB(eval.truePositiveRate(i), eval.trueNegativeRate(i)));
			// DTP metrics
			
			temp.put("DTPpr", PerformanceMetricUtil.DTPpr(eval.precision(i), eval.recall(i)));
			temp.put("DTPperc", PerformanceMetricUtil.DTPperc(PerformanceMetricUtil.TPP(tp, fn), PerformanceMetricUtil.FPP(fp, tp, fn)));
			temp.put("DTPrate", PerformanceMetricUtil.DTPrate(eval.truePositiveRate(i), eval.falsePositiveRate(i)));
			
			temp.put("TPC_FP", fp * this.averagePowerChange[i]);
			temp.put("TPC_FN", fn * this.averagePowerChange[i]);
			temp.put("APC_FP", (fp != 0) ? (fp * this.averagePowerChange[i]) / fp : 0);
			temp.put("APC_FN", (fn != 0) ? (fn * this.averagePowerChange[i]) / fn :0);
			
			temp.put("DTPtpc", PerformanceMetricUtil.DTPpc(temp.get("TPC_FP"), temp.get("TPC_FN")));
			temp.put("DTPapc", PerformanceMetricUtil.DTPpc(temp.get("APC_FP"), temp.get("APC_FN")));
			
			ia_metrics.add(temp);
		}
		return ia_metrics;
	}
	
	protected double getStdMCC(double mcc) {
		return (1 + mcc) * 0.5;
	}
	
	protected void writeToFile(Map<String, String> params, Evaluation eval ) {
		// get params header and data
		Object[] header = new Object[params.size()]; 
		Object[] data = new Object[params.size()];
		int index = 0;
		for(Map.Entry<String, String> entry : params.entrySet()) {
			header[index] 	= entry.getKey();
			data[index] 	= entry.getValue();
			index++;
		}
		List<Map<String, Double>> ia_metrics = getIndividualClassMetrics(eval);
		Map<String, Double> micro_metrics = getMicroMetrics(ia_metrics);
		Map<String, Double> macro_metrics = getMacroMetrics(ia_metrics);
		Map<String, Double> weight_metrics = getWeightMetrics(ia_metrics, eval);
		Map<String, Double> other_metrics = new LinkedHashMap<String, Double>();
		other_metrics.put("C", eval.correct());
		other_metrics.put("I", eval.incorrect());
		other_metrics.put("PCT_C", eval.pctCorrect());
		other_metrics.put("PCT_I", eval.pctIncorrect());
		other_metrics.put("PCT_U", eval.pctUnclassified());
		other_metrics.put("K", eval.kappa());
		
		try {
			other_metrics.put("RMSE", eval.rootMeanSquaredError());
			other_metrics.put("RRSE", eval.rootRelativeSquaredError());
			other_metrics.put("MAE", eval.meanAbsoluteError());
			other_metrics.put("RAE", eval.relativeAbsoluteError());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(this.done_header == false)
			header = ArrayUtils.addAll(header, getHeader(ia_metrics, micro_metrics, macro_metrics, weight_metrics, other_metrics));
		
		data = ArrayUtils.addAll(data, getData(ia_metrics, micro_metrics, macro_metrics, weight_metrics, other_metrics));
		writer(header, data);
	}
	/*
	protected void writeToFile(int k, int fold, EvaluationFeatures features, long msTrain, 
			long msTest, Evaluation eval) {
		
		List<Map<String, Double>> ia_metrics = getIndividualClassMetrics(eval);
		Map<String, Double> micro_metrics = getMicroMetrics(ia_metrics);
		Map<String, Double> macro_metrics = getMacroMetrics(ia_metrics);
		Map<String, Double> weight_metrics = getWeightMetrics(ia_metrics, eval);
		Map<String, Double> percent_metrics = new LinkedHashMap<String, Double>();
		percent_metrics.put("PCT_C", eval.pctCorrect());
		percent_metrics.put("PCT_I", eval.pctIncorrect());
		percent_metrics.put("PCT_U", eval.pctUnclassified());
		percent_metrics.put("K", eval.kappa());
		percent_metrics.put("RMSE", eval.rootMeanSquaredError());
		percent_metrics.put("RRSE", eval.rootRelativeSquaredError());
		percent_metrics.put("MAE", eval.meanAbsoluteError());
		try {
			percent_metrics.put("RAE", eval.relativeAbsoluteError());
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		Object[] header = {"K","FOLD", "FEATURES", "MS TRAIN", "MS TEST"};
		System.out.println("* " + msTrain + " | " + msTest);
		Object[] data = {k, fold, features.toString(), msTrain, msTest};
		
		if(this.done_header == false)
			header = ArrayUtils.addAll(header, 
					getHeader(ia_metrics, micro_metrics, macro_metrics, weight_metrics, percent_metrics));
		
		data = ArrayUtils.addAll(data, 
				getData(ia_metrics, micro_metrics, macro_metrics, weight_metrics, percent_metrics));
		writer(header, data);
	}*/
	
	boolean done_header = false;
	private void writeHeader(Object[] header) {
		try {
			this.csvFilePrinter.printRecord(header);
			done_header = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void close() {
		try {
			fileWriter.flush();
			fileWriter.close();
			csvFilePrinter.close();
			System.out.println("CSV file was created successfully !!!");
		} catch (IOException e) {
			System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
            e.printStackTrace();
		}
	}
	
	private void writer(Object[] header, Object[] data) {
		List<Object> csvData = new ArrayList<Object>();
		try {
			// write the header only if it is first record
			if(this.done_header == false)
				this.writeHeader(header);
			
			for(Object d : data) {
				csvData.add(d);
			}
			csvFilePrinter.printRecord(csvData);
		} catch (IOException e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
	}
	
	private Object[] getData(List<Map<String, Double>> ia_data, 
			Map<String, Double> mi_data,Map<String, Double> ma_data,
			Map<String, Double> w_data,Map<String, Double> p_data) {
		List<Double> data = new ArrayList<Double>();
		Map<String, Double> temp;
		double value;
		Iterator<Map<String, Double>> it = ia_data.iterator();
		while(it.hasNext()) {
			temp = it.next();
			for(Map.Entry<String, Double> entry : temp.entrySet()) {
				value = entry.getValue();
				data.add(value);
			}
		}
		for(Map.Entry<String, Double> entry : mi_data.entrySet()) {
			value = entry.getValue();
			data.add(value);
		}
		for(Map.Entry<String, Double> entry : ma_data.entrySet()) {
			value = entry.getValue();
			data.add(value);
		}
		for(Map.Entry<String, Double> entry : w_data.entrySet()) {
			value = entry.getValue();
			data.add(value);
		}
		for(Map.Entry<String, Double> entry : p_data.entrySet()) {
			value = entry.getValue();
			data.add(value);
		}
		return data.toArray();
	}
	
	private Object[] getHeader(List<Map<String, Double>> ia_data, 
			Map<String, Double> mi_data,Map<String, Double> ma_data,
			Map<String, Double> w_data,Map<String, Double> p_data) {
		List<Object> header = new ArrayList<Object>();
		Map<String, Double> temp;
		String key;
		int i = 0;
		Iterator<Map<String, Double>> it = ia_data.iterator();
		while(it.hasNext()) {
			temp = it.next();
			for(Map.Entry<String, Double> entry : temp.entrySet()) {
				key = i + "_" + entry.getKey();
				header.add(key);
			}
			i++;
		}
		for(Map.Entry<String, Double> entry : mi_data.entrySet()) {
			key = "mi_" + entry.getKey();
			header.add(key);
		}
		for(Map.Entry<String, Double> entry : ma_data.entrySet()) {
			key = "ma_" + entry.getKey();
			header.add(key);
		}
		for(Map.Entry<String, Double> entry : w_data.entrySet()) {
			key = "w_" + entry.getKey();
			header.add(key);
		}
		for(Map.Entry<String, Double> entry : p_data.entrySet()) {
			key = entry.getKey();
			header.add(key);
		}
		
		return header.toArray();
	}
	
	protected void removeLocationAndApplianceID(Instances instances) {
		// remove app_id
		instances.deleteAttributeAt(instances.numAttributes() - 2);
		// remove app_location
		instances.deleteAttributeAt(instances.numAttributes() - 2);
	}
}
