package emdsf.EventBased.Evaluation.EventClassification.WEKA;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.Util;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier.EC_ANN;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier.EC_KNN;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureType;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;

public class ANN_Evaluation extends AEvaluation {
	
	private double[] lr; //learning rate values
	private List<int[]> folds;
	private EvaluationFeatures[] featureSets;
	private Instances	data;
	
	Map<EvaluationFeatures, Map<Integer, Evaluation>> out;
	
	public ANN_Evaluation(Instances data, double[] lr, List<int[]> folds, EvaluationFeatures[] featureSets, String file_path, double[] averagePowerChange) throws IOException {
		super(file_path, averagePowerChange);
		this.data 			= data;
		this.lr 			= lr;
		this.folds 			= folds;
		this.featureSets	= featureSets;
	}
	
	private  double getHiddenLayers(String h, double numAttributes, double numClasses) {
		double out;
		switch(h) {
			case "i":
				out = numAttributes;
				break;
			case "o":
				out = numClasses;
				break;
			case "t":
				out = numClasses + numAttributes;
			default:
				out = (numClasses + numAttributes) / 2;
				break;
		}
		return out;
	}
	
	// in -> train and test instances
	public void eval() throws Exception {
		Instances train = null;
		Instances test = null;
		EC_ANN la;
		int fold_index;
		FeatureType[] ft;
		int[] indexes;
		int[] feature_indexes = new int[0];
		Map<String, String> header;
		for(int r = 0; r < runs; r++) {
			System.out.println("ROUND: " + (r+1));
			for(int l = 0; l < lr.length; l++ ) {
				fold_index = 0;
				for (int[] fold : folds) {
					fold_index ++;
					indexes = Util.getInstanceIndexes(data, data.numAttributes() - 3, fold);
					test = Util.getIndexes(data, indexes);
					train = Util.removeIndexes(data, indexes);	
					// remove unnecessary attributes
					super.removeLocationAndApplianceID(train);
					super.removeLocationAndApplianceID(test);
					for (EvaluationFeatures ef : featureSets) {
						ft = ef.getFeatureType();
						feature_indexes = new int[0];
						for(FeatureType f : ft)
							feature_indexes = ArrayUtils.addAll(feature_indexes, f.getFeatureIndexes());
	
						train.setClassIndex(train.numAttributes() - 1);
						test.setClassIndex(test.numAttributes() -1);
						la = new EC_ANN(
								Util.removeAttributes(train, feature_indexes), 
								Util.removeAttributes(test, feature_indexes));
						
						Evaluation eval = la.eval(lr[l]);
						
						header = new LinkedHashMap<String, String>();
						header.put("RUN", Integer.toString(r+1));
						header.put("LR", Double.toString(lr[l]));
						header.put("FOLD", Integer.toString(fold_index));
						header.put("HL", Double.toString( getHiddenLayers(la.getClassifier().getHiddenLayers(),la.getNumAttributes(), la.getNumTrainClasses()) ));
						header.put("FEATURES", ef.toString());
						header.put("TRAIN_ET", Double.toString(la.elapsedTimeTrain));
						header.put("TEST_ET", Double.toString(la.elapsedTimeTest));
						super.writeToFile(header, eval);
					}
				}
			}
		}
		super.close();
	}
}