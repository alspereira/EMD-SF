package emdsf.EventBased.Evaluation.EventClassification.WEKA;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureType;

public enum EvaluationFeatures {
	
	//PQ(FeatureType.P, FeatureType.Q), // OK
	//PQI(FeatureType.P, FeatureType.Q, FeatureType.I), // OK
	
	// individual features
	/*
	WF_I(FeatureType.WF_I),
	Q_WF_I(FeatureType.Q_WF_I),
	Q_WF_IV(FeatureType.Q_WF_IV),
	H_I(FeatureType.H_I),
	H_IV(FeatureType.H_IV),
	BINARY_VI(FeatureType.BINARY_VI),
	PCA_WF_I(FeatureType.PCA_WF_I),
	PCA_Q_WF_I(FeatureType.PCA_Q_WF_I),
	PCA_Q_WF_IV(FeatureType.PCA_Q_WF_IV),
	PCA_BINARY_IV(FeatureType.PCA_BINARY_IV);
	*/
	
	// combined features
	
	PQ__H_I(FeatureType.P, FeatureType.Q, FeatureType.H_I),
	PQ__H_IV(FeatureType.P, FeatureType.Q, FeatureType.H_IV),
	
	PQ__Q_WF_I(FeatureType.P, FeatureType.Q, FeatureType.Q_WF_I),
	PQ__Q_WF_IV(FeatureType.P, FeatureType.Q, FeatureType.Q_WF_IV),
	
	PQ__PCA_BINARY_IV(FeatureType.P, FeatureType.Q, FeatureType.PCA_BINARY_IV),
	
	PQ__H_I__Q_WF_I(FeatureType.P, FeatureType.Q, FeatureType.H_I, FeatureType.Q_WF_I),
	PQ__H_I__Q_WF_IV(FeatureType.P, FeatureType.Q, FeatureType.H_I, FeatureType.Q_WF_IV),
	
	PQ__H_IV__Q_WF_I(FeatureType.P, FeatureType.Q, FeatureType.H_IV, FeatureType.Q_WF_I),
	PQ__H_IV__Q_WF_IV(FeatureType.P, FeatureType.Q, FeatureType.H_IV, FeatureType.Q_WF_IV),
	
	PQ__H_I__PCA_BINARY_VI(FeatureType.P, FeatureType.Q, FeatureType.H_I, FeatureType.PCA_BINARY_IV),
	PQ__H_IV__PCA_BINARY_VI(FeatureType.P, FeatureType.Q, FeatureType.H_IV, FeatureType.PCA_BINARY_IV),
	
	H_I__PCA_BINARY_IV(FeatureType.H_I, FeatureType.PCA_BINARY_IV),
	H_IV__PCA_BINARY_IV(FeatureType.H_IV, FeatureType.PCA_BINARY_IV),
	
	Q_WF_I__PCA_BINARY_VI(FeatureType.Q_WF_I,FeatureType.PCA_BINARY_IV),
	Q_WF_IV__PCA_BINARY_VI(FeatureType.Q_WF_IV, FeatureType.PCA_BINARY_IV),
	
	PCA_Q_WF_I__PCA_BINARY_IV(FeatureType.PCA_Q_WF_I,FeatureType.PCA_BINARY_IV), 
	PCA_Q_WF_IV__PCA_BINARY_IV(FeatureType.PCA_Q_WF_IV,FeatureType.PCA_BINARY_IV),
	PCA_WF_I__PCA_BINARY_IV(FeatureType.PCA_WF_I, FeatureType.PCA_BINARY_IV);
	
	
	
	private FeatureType[] featureType;
	
	EvaluationFeatures(FeatureType... features) {
		featureType = features;
	}
	
	public FeatureType[] getFeatureType() {
		return featureType;
	}
	
}
