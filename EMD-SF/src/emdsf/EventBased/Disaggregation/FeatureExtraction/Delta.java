package emdsf.EventBased.Disaggregation.FeatureExtraction;

import java.util.LinkedHashMap;
import java.util.Map;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;

public class Delta {

	public Delta() {
		// TODO Auto-generated constructor stub
	}
	
	public float getDelta(IPowerSample[] powerSamples, PowerMetricType metric, int e, int w1, int w2, int w3) {
		float avgBefore = 0.0f;
		float avgAfter = 0.0f;
		int i;
		
		/*
		 * 0 - 59
		 * 60 -> edge
		 * 61 - 120
		 * */
		
		// e = 60;
		// w1 = 60
		// w2 = 0;
		// w3 = 60;
		
		// i = e - w1 = 0;
		// i < e - 1 = i < 60 - 1 => i < 59;
		// 0 -> 58 = 59 pontos -> devia ser 60 pontos | substituir por <= (done)
		int step = 0;
		for(i = e - w1; i <= e - 1; i++) {
			//System.out.println(++step);
			avgBefore += powerSamples[i].getMetricValue(metric);
		
		}
		avgBefore = avgBefore / w1;
		
		// i = 60 + 0 + 1 = 61;
		// i < 60 + 0 + 60 -> i < 120
		// 61 -> 119 = 59 pontos -> devia ser 60 pontos
		for(i = e + w2 + 0; i <= e + w2 + w3; i++) {
			avgAfter += powerSamples[i].getMetricValue(metric);
		}		
			
		avgAfter = avgAfter / w3;
		return avgAfter - avgBefore;
	}
	
	public float getDelta(float[] samples, int e, int w1, int w2, int w3) {
		float avgBefore = 0.0f;
		float avgAfter = 0.0f;
		int i;
		
		for(i = e - w1; i <= e - 1; i++)
			avgBefore += samples[i];
		avgBefore = avgBefore / w1;
		
		for(i = e + w2 + 0; i <= e + w2 + w3; i++)
			avgAfter += samples[i];
		avgAfter = avgAfter / w3;
		
		return avgAfter - avgBefore;
	}

	public Map<Integer, Float> getHarmonicsDelta(IPowerSample[] powerSamples, int[] harmonicIndex, int e, int w1, int w2, int w3) {
		float[][] harmonicSamples = new float[harmonicIndex.length][powerSamples.length];
		Map<String, Float> harmonics;
		Map<Integer, Float> harmonicsDelta = new LinkedHashMap<Integer, Float>();
		
		// first we separate the samples of each harmonic
		for(int s = 0; s < powerSamples.length; s++) {
			harmonics = powerSamples[s].getHamonics();
			for(int h = 0; h < harmonicIndex.length; h++) {
				harmonicSamples[h][s] = harmonics.get(harmonicIndex[h]);
			}
		}
 		// then we calculate the deltas of each harmonic using an existing equation
		for(int h = 0; h < harmonicIndex.length; h++) {
			harmonicsDelta.put(harmonicIndex[h], getDelta(harmonicSamples[h],e,w1,w2,w3));
		}
		return harmonicsDelta;
	}
}