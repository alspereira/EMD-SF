package emdsf.EventBased.Disaggregation.FeatureExtraction;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathArrays;
import org.jtransforms.fft.DoubleFFT_1D;
import org.jtransforms.fft.FloatFFT_1D;

public class Harmonic {

	float[] data;
	float[] harmonics;
	
	public Harmonic(float[] data) {
		this.data = data;
	}
	
	public float[] getHarmonics(float[] data, int order, int offset) {
		float[] h = this.calculateHarmonics(data);
		float[] out = new float[order];
		
		for(int i = offset; i < order + offset; i++) {
			out[i - offset] = h[i];
		}
		return out;
	}
	
	public float[] getHarmonics(float[] data, int[] indexes) {
		float[] h = this.calculateHarmonics(data);
		float[] out = new float[indexes.length];
		
		for(int i = 0; i < indexes.length; i++)
			out[i] = h[indexes[i]];
		return out;
	}
	
	public static double[][] calculateHarmonics(double[] data, int fs, int ff, int cc) {
		int samplesPerPeriod 	= fs / ff;
		int num_periods 		= data.length / samplesPerPeriod;
		int num_harmonics 		= (data.length / num_periods) / 2;
		
		double[][] out 			= new double[num_harmonics][num_periods];
		System.out.println("OUT length: " + out.length);
		double[] h_temp;
		int from, to;
		for(int i = 0; i < num_periods; i++) {
			from = i * samplesPerPeriod;
			to = from + samplesPerPeriod;
			h_temp = calculateHarmonics(
					MathArrays.copyOfRange(data, from, to), cc);
			for(int p = 0; p < out.length; p++)
				out[p][i] = h_temp[p];
			
		}	
		return out;
	}
	
	public static double[] calculateHarmonics(double[] data, double cc) {
		MathArrays.scaleInPlace(cc, data);
		DoubleFFT_1D fft = new DoubleFFT_1D(data.length);
		double[] abs = new double[data.length / 2];
		fft.realForward(data);
		
		abs[0] = data[0] / (data.length / 2);	
		for(int i = 1; i < data.length / 2; i++) {
			abs[i] = (float) (Math.sqrt(Math.pow(data[2*i], 2) + Math.pow(data[2*i+1], 2)) / (data.length / 2));			
		}
		return abs;
	}
	
	
	private float[] calculateHarmonics(float[] data) {
		FloatFFT_1D fft = new FloatFFT_1D(data.length);
		float[] abs = new float[data.length / 2];
		
		fft.realForward(data);
		abs[0] = data[0] / (data.length / 2);	
		for(int i = 1; i < data.length / 2; i++) {
			abs[i] = (float) (Math.sqrt(Math.pow(data[2*i], 2) + Math.pow(data[2*i+1], 2)) / (data.length / 2));			
		}
		return abs;
	}
}