package emdsf.EventBased.Disaggregation.FeatureExtraction;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.MathUtils;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;

public class TransientProfile {

	public static double[] getTransient(IPowerSample[] samples, PowerMetricType pmType, int offset, int length) {
		double[] t = new double[length];
		for(int i = offset; i < offset + length; i++) {
			samples[i].getMetricValue(pmType);
		}
		return t;
	}
	
	public static double[] quantizeTransient(double[] samples, int bins) {
		double [] qt = new double[bins];
		int index = 0;
		double v = 0;
		
		System.out.println(Math.floor(2.7));
		System.out.println(Math.ceil(2.7));
		System.out.println((double)samples.length / bins);
		
		double binSize = Math.floor((double)samples.length / bins);
		
		System.out.println("bin size: " + binSize);
		for(int i = 0; i < bins - 1; i++) {
			v = StatUtils.percentile(samples, index, (int) binSize, 50);
			index += binSize;	
			qt[i] = v;
		}
		int l = samples.length - index;
		System.out.println("index: " + index);
		System.out.println("dif: " + l);
		v = StatUtils.percentile(samples,index, l, 50);
	
		//binData = MathArrays.copyOfRange(samples, index, samples.length - index);
		qt[bins - 1] = v;
		// now we get the remaining samples
		
		
		return qt;
	}
}