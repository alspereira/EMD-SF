package emdsf.EventBased.Disaggregation.FeatureExtraction;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathArrays;

public class VITrajectory {

	private double[] v;
	private double[] i;

	private double[] i_norm;
	private double[] v_norm;
	
	public VITrajectory(double[] v, double[] i) {
		this.v = v;
		this.i = i;
	}
	
	// normalize by RMS (according to original thesis on this topic)
	public void normalizeVI() {
		v_norm = MathArrays.scale(1 / Math.sqrt(StatUtils.sumSq(v) / v.length), v);
		i_norm = MathArrays.scale(1 / Math.sqrt(StatUtils.sumSq(i) / i.length), i);
	}
	
	// shape features

}
