package emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature;

public enum FeatureFileType {
		PQIV,
		H_I,
		H_IV,
		WF_I,
		Q_WF_I,
		Q_WF_IV,
		BIN_VI,
		PCA_WF_I,
		PCA_Q_WF_I,
		PCA_Q_WF_IV,
		PCA_BIN_VI
}
