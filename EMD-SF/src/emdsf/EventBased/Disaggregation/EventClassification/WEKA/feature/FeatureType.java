package emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature;

public enum FeatureType {
	P(0,1),
	Q(1,1),
	I(2,1),
	V(3,1),
	H_I(4, 22), // 4 - 250
	H_IV(254,22), // 254 - 250
	WF_I(504, 500),
	Q_WF_I(1004, 20),
	Q_WF_IV(1024,40),
	BINARY_VI(1064,256),
	PCA_WF_I(1320,6),
	PCA_Q_WF_I(1326,5),
	PCA_Q_WF_IV(1331,6),
	PCA_BINARY_IV(1337,124);
	
	private FeatureFileType fileType;
	private int index;
	private int length;
	
	FeatureType(int index) {
		this.index = index;
		this.length = 1;
	}
	
	FeatureType(int from, int length) {
		this.index = from;
		this.length = length;
	}
	
	public int[] getFeatureIndexes() {
		int[] out = new int[length];
		for(int i = 0; i < length; i++)
			out[i] = this.index + i;
		return out;
	}
	
	public int getIndex() {
		return this.index;
	}
}
