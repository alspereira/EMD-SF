package emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature;

import java.util.LinkedHashMap;
import java.util.Map;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Loader {
	
	DataSource ds;
	public Instances data;
	public Map<FeatureFileType, Instances> featureMap;
	
	public Loader(FeatureLocation[] locations) throws Exception {
		featureMap = new LinkedHashMap<FeatureFileType, Instances>();
		for(FeatureLocation l : locations) {
			System.out.println(l.path);
			featureMap.put(l.key, new DataSource(l.path).getDataSet());
		}		
	}
	
	public Loader(String file_path) throws Exception {
		ds = new DataSource(file_path);
		data = ds.getDataSet();
	}	
}

