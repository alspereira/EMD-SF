package emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature;

public class FeatureLocation {
	
	public final FeatureFileType key;
	public final String path;
	

	public FeatureLocation(FeatureFileType k, String p) {
		key = k;
		path = p;
	}
}
