package emdsf.EventBased.Disaggregation.EventClassification.WEKA;

import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.MathArrays.OrderDirection;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureFileType;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureLocation;
import emdsf.EventBased.Disaggregation.EventClassification.WEKA.feature.FeatureType;

public class Util {

	// given a set of indexes, return the instances on those indexes
	public static Instances removeIndexes(Instances instances, int[] indexes) {
		Instances out = new Instances(instances);
		Arrays.sort(indexes); // sort the indexes such that we can remove the items correctly
		
		for(int i = indexes.length - 1; i >= 0; i--)
			out.delete(indexes[i]);
		
		//for(int i : indexes) {
		//	//System.out.println(i - 1);
		//	out.delete(i - 1);
		//}
		return out;
	}
	
	public static double[] getClassCounts(Evaluation eval) {
		int numClasses = eval.getHeader().numClasses();
		double[] classCounts = new double[numClasses];
		double[][] confusionMatrix = eval.confusionMatrix();
		double classCountSum = 0; 
		for (int i = 0; i < numClasses; i++) {
		       for (int j = 0; j < numClasses; j++) {
		         classCounts[i] += confusionMatrix[i][j];
		       }
		       classCountSum += classCounts[i];
	    }
		return classCounts;
	}
	
	public static double[] getAveragePower(Instances instances, int[] appliances) {
		double[] o = new double[11];
		double[] t;
		int[] instance_indexes;
		Instances temp;
		for(int i = 0; i < appliances.length; i++) {
			instance_indexes = getInstanceIndexes(instances, instances.numAttributes() - 2, appliances[i]);
			temp = getIndexes(instances, instance_indexes);
			t = temp.attributeToDoubleArray(FeatureType.P.getIndex());
			o[i] = StatUtils.percentile(t, 50);
		}
		return o;
	}
	
	public static Instances getIndexes(Instances instances, int[] indexes) {
		Instances out = new Instances(instances, indexes.length);
		for(int i : indexes)
			out.add(instances.get(i));
		return out;
	}
	
	public static Instances getFeatures(Instances instances, int[] indexes) {
		Instances out = new Instances(instances, 0, 0); //, indexes.length);
		Attribute att;
		for(int i = 0; i < indexes.length; i++) {
			att = instances.attribute(indexes[i]);
			out.insertAttributeAt(att, i);
		}
		return out;
	}
	
	public static Instances removeAttributes(Instances instances, int[] indexes) {
		Instances out = new Instances(instances);
		int index = instances.numAttributes() - 2;
		while(index >= 0) {
			if(ArrayUtils.indexOf(indexes, index) == -1) { // is not on the attribute list
				out.deleteAttributeAt(index);
			}
			index --;
		}
		/*
		for(int i = 0; i < instances.numAttributes() - 1; i++) {
			if(ArrayUtils.indexOf(indexes, i) == -1) { // is not on the list
				out.deleteAttributeAt(i);
			}
		}*/
		return out;
	}
	
	public static int[] getInstanceIndexes(Instances instances, int attribute_index, int[] valuesToFind) {
		int[] out = new int[0];
		for(int v : valuesToFind) {
			out = ArrayUtils.addAll(out, getInstanceIndexes(instances, attribute_index, v));
		}
		return out;
	}
	
	public static int[] getInstanceIndexes(Instances instances, int index, int valueToFind) {
		double[] attValues = instances.attributeToDoubleArray(index);
		int[] indexes = new int[0];
		int startIndex = ArrayUtils.indexOf(attValues, valueToFind, 0);
		while(startIndex != -1) {
			indexes = ArrayUtils.add(indexes, startIndex);
			startIndex = ArrayUtils.indexOf(attValues, valueToFind, startIndex + 1);
		}		
		return indexes;
	}
	
	// combine instances
	public static Instances combineInstances(Instances...instances) {
		Instances out = new Instances(instances[0]);
		for(int i = 1; i < instances.length; i++)
			Instances.mergeInstances(out, instances[i]);
		return out;
	}
	
	public static FeatureLocation[] getFeatureLocation(String path) {
		FeatureLocation[] out = {
				new FeatureLocation(FeatureFileType.PQIV, path + "PQIV.csv"),
				new FeatureLocation(FeatureFileType.H_I, path +  "H_I.csv"),
				new FeatureLocation(FeatureFileType.H_IV, path + "H_IV.csv"),
				new FeatureLocation(FeatureFileType.WF_I, path + "WF_I.csv"),
				new FeatureLocation(FeatureFileType.Q_WF_I, path + "Q_WF_I.csv"),
				new FeatureLocation(FeatureFileType.Q_WF_IV, path + "Q_WF_IV.csv"),
				new FeatureLocation(FeatureFileType.BIN_VI, path + "BIN_VI.csv"),
				new FeatureLocation(FeatureFileType.PCA_WF_I, path + "PCA_WF_I.csv"),
				new FeatureLocation(FeatureFileType.PCA_Q_WF_I, path + "PCA_Q_WF_I.csv"),
				new FeatureLocation(FeatureFileType.PCA_Q_WF_IV, path + "PCA_Q_WF_IV.csv"),
				new FeatureLocation(FeatureFileType.PCA_BIN_VI, path + "PCA_BIN_VI.csv")
		};
		return out;
	}
	public  static List<int[]> returnIndexes(int length, int binSize) {
		List<int[]> out = new ArrayList<int[]>();
		
		int numBins = (int) Math.floor(length / binSize);
		System.out.println("NUM BINS: " + numBins);
		int[] indexes = new int[binSize];
		int startValue = 1;
		for(int b = 0; b < numBins-1; b++) {
			indexes = getSequence(startValue, binSize);
			out.add(indexes);
			startValue += binSize;
		}
		if(startValue <= length) {
			indexes = getSequence(startValue, (length - startValue) + 1);
			out.add(indexes);
		}
		return out;
	}
	
	private static int[] getSequence(int start, int size) {
		int[] out = new int[size];
		for(int i = 0; i < size; i++)
			out[i] = start + i;
		return out;
	}
	
	public static int[] _returnIndexes(int from, int to) {
		// 1 to 5 : range = 4
		int range = to - from;
		System.out.println(range);
		int out[] = new int[range];
		for(int i = 0; i < range; i++) {
			out[i] = from + i;
		}
		return out;
	}
	
}
