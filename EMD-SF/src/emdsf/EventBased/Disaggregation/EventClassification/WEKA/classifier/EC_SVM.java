package emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instances;
import weka.core.WekaPackageManager;

public class EC_SVM {

	private Instances train;
	private Instances test;
	
	public EC_SVM(Instances _train, Instances _test) {
		this.train = _train;
		this.test = _test;
		this.train.setClassIndex(this.train.numAttributes() -1);
		this.test.setClassIndex(this.test.numAttributes() -1);
	}
	
	public long elapsedTimeTrain = 0l;
	public long elapsedTimeTest = 0l;
	
	private LibSVM classifier;
	
	public LibSVM getClassifier() {
		return this.classifier;
	}
	
	public double getNumAttributes() {
		return this.train.numAttributes() - 1;
	}
	
	public double getNumTrainClasses() {
		return this.train.numClasses();
	}
	
	public double getNumTestClasses() {
		return this.test.numClasses();
	}
	
	public Evaluation eval(double cost) throws Exception {
		WekaPackageManager.loadPackages( false, true, false );
		LibSVM c = new LibSVM();
		c.setDebug(false);
		this.classifier = c;
		// the defaults are OK for us: KernelType: RBF; SVMType: C_SVC
		// c.setGamma(gamma);
		c.setCost(cost);
		c.setNormalize(false);
		long startTime = System.currentTimeMillis();
		c.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(c, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
	
}
