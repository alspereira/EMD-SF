package emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier;

import emdsf.EventBased.Disaggregation.EventClassification.WEKA.Util;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.core.Instances;

public class EC_KNN {

	private Instances train;
	private Instances test;
	
	public EC_KNN(Instances _train, Instances _test) {
		this.train = _train;
		this.test = _test;
		this.train.setClassIndex(this.train.numAttributes() -1);
		this.test.setClassIndex(this.test.numAttributes() -1);
	}
	
	public long elapsedTimeTrain = 0l;
	public long elapsedTimeTest = 0l;
	
	public Evaluation eval(int k) throws Exception {
		IBk kNN = new IBk(k);
		long startTime = System.currentTimeMillis();
		kNN.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(kNN, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
}
