package emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.REPTree;
import weka.core.Instances;

public class EC_DT {
	
	

	private Instances train;
	private Instances test;
	
	public EC_DT(Instances _train, Instances _test) {
		this.train = _train;
		this.test = _test;
		this.train.setClassIndex(this.train.numAttributes() -1);
		this.test.setClassIndex(this.test.numAttributes() -1);
	}
	
	public long elapsedTimeTrain = 0l;
	public long elapsedTimeTest = 0l;
	
	// 0, 1, 2, 5, 10
	public Evaluation evalJ48(int minNodes) throws Exception {
		J48 c = new J48();
		c.setMinNumObj(minNodes);
		long startTime = System.currentTimeMillis();
		c.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(c, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
	
	// -1 5 10 15 20
	public Evaluation evalREP(int maxDepth) throws Exception {
		REPTree c = new REPTree();
		c.setMaxDepth(maxDepth);
		long startTime = System.currentTimeMillis();
		c.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(c, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
	
	public Evaluation eval(TreeType dTree) throws Exception {
		
		Classifier c;;
		
		switch(dTree) {
			case J48:
				c = new J48();
				break;
			case REP:
				c = new REPTree();
				break;
			default:
				c = new J48();
				break;
		}
		long startTime = System.currentTimeMillis();
		c.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(c, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
}
