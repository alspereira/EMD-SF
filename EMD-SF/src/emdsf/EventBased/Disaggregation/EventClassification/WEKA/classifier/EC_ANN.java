package emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier;

import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instances;

public class EC_ANN {

	private Instances train;
	private Instances test;
	
	public EC_ANN(Instances _train, Instances _test) {
		this.train = _train;
		this.test = _test;
		this.train.setClassIndex(this.train.numAttributes() -1);
		this.test.setClassIndex(this.test.numAttributes() -1);
	}
	
	public long elapsedTimeTrain = 0l;
	public long elapsedTimeTest = 0l;
	
	private MultilayerPerceptron classifier;
	
	public MultilayerPerceptron getClassifier() {
		return this.classifier;
	}
	
	public double getNumAttributes() {
		return this.train.numAttributes() - 1;
	}
	
	public double getNumTrainClasses() {
		return this.train.numClasses();
	}
	
	public double getNumTestClasses() {
		return this.test.numClasses();
	}
	
	public Evaluation eval(double learningRate) throws Exception {
		MultilayerPerceptron ann = new MultilayerPerceptron();
		ann.setLearningRate(learningRate);
		ann.setHiddenLayers("o");
		this.classifier = ann;
		long startTime = System.currentTimeMillis();
		ann.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(ann, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
	
//	public Evaluation eval(String hiddenLayers) throws Exception {
//		MultilayerPerceptron ann = new MultilayerPerceptron();
//		ann.setHiddenLayers(hiddenLayers);
//		this.classifier = ann;
//		long startTime = System.currentTimeMillis();
//		ann.buildClassifier(train);
//		long endTime = System.currentTimeMillis();
//		this.elapsedTimeTrain = endTime - startTime;
//		Evaluation eval = new Evaluation(test);
//		startTime = System.currentTimeMillis();
//		eval.evaluateModel(ann, test);
//		endTime = System.currentTimeMillis();
//		this.elapsedTimeTest = endTime - startTime;
//		return eval;
//	}
}
