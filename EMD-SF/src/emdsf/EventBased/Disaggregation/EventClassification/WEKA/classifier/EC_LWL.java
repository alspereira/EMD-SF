package emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.net.search.fixed.NaiveBayes;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.lazy.LWL;
import weka.core.Instances;

public class EC_LWL {

	private Instances train;
	private Instances test;
	
	public EC_LWL(Instances _train, Instances _test) {
		this.train = _train;
		this.test = _test;
		this.train.setClassIndex(this.train.numAttributes() -1);
		this.test.setClassIndex(this.test.numAttributes() -1);
	}
	
	public long elapsedTimeTrain = 0l;
	public long elapsedTimeTest = 0l;
	
	public Evaluation eval(int k) throws Exception {
		LWL c = new LWL();
		Classifier nb = (Classifier) new NaiveBayes();
		c.setClassifier(nb);
		c.setKNN(k);
		long startTime = System.currentTimeMillis();
		c.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(c, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		return eval;
	}
}
