package emdsf.EventBased.Disaggregation.EventClassification.WEKA.classifier;

import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.lazy.KStar;
import weka.core.Instances;
import weka.core.Utils;

public class EC_KStar {

	private Instances train;
	private Instances test;
	
	public EC_KStar(Instances train, Instances test) {
		this.train = train;
		this.test = test;
		this.train.setClassIndex(this.train.numAttributes() -1);
		this.test.setClassIndex(this.test.numAttributes() -1);
		
	}
	public long elapsedTimeTrain;
	public long elapsedTimeTest;
	
	public Evaluation eval(int blend) throws Exception {
		KStar kStar = new KStar();
		
		kStar.setGlobalBlend(blend);
		long startTime = System.currentTimeMillis();
		kStar.buildClassifier(train);
		long endTime = System.currentTimeMillis();
		this.elapsedTimeTrain = endTime - startTime;
		Evaluation eval = new Evaluation(test);
		startTime = System.currentTimeMillis();
		eval.evaluateModel(kStar, test);
		endTime = System.currentTimeMillis();
		this.elapsedTimeTest = endTime - startTime;
		//System.out.println(this.elapsedTimeTrain + " | " + this.elapsedTimeTest);
		return eval;
	}
}