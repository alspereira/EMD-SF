package emdsf.EventBased.Disaggregation.EventDetection;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.ADetectionStatistics;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DS_LikelihoodAnderson2014;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DS_LikelihoodPereira2014;

public class DS_Factory {
	
	public static ADetectionStatistics getDetectionStatistics(DSType dsType,
			ControlledArrayBlockingQueue<IPowerSample> in, 
			int preEventWindowSize, int postEventWindowSize, float threshold, PowerMetricType metric) {
		
		ADetectionStatistics ds = null;
		 
		switch(dsType) {
			case PEREIRA_2014:
				ds = new DS_LikelihoodPereira2014(in, preEventWindowSize, postEventWindowSize, threshold, metric);
				break;
			case ANDERSON_2014:
				ds = new DS_LikelihoodAnderson2014(in, preEventWindowSize, postEventWindowSize, threshold, metric);
				break;
			default:
				break;
		}
		return ds;
	}

	
}
