package emdsf.EventBased.Disaggregation.EventDetection;

import base.ControlledArrayBlockingQueue;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation.ADetectionActivation;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation.DA_Maxima;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation.EDA_Voting;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DetectionStatistic;

public class DA_Factory {
	
	public static class Factory {
		private static int samplesBeforeEvent;
		private static int samplesAfterEvent;
		private static ControlledArrayBlockingQueue<DetectionStatistic> in;
		private static ADetectionActivation da;
		
		public Factory (ControlledArrayBlockingQueue<DetectionStatistic> in, 
				int samplesBeforeEvent, int samplesAfterEvent) {
			Factory.in = in;
			Factory.samplesBeforeEvent = samplesBeforeEvent;
			Factory.samplesAfterEvent = samplesAfterEvent;
		}
		
		public ADetectionActivation buildMaxima(int precision, float threshold) {
			da = new DA_Maxima(in, samplesBeforeEvent, samplesAfterEvent, precision, threshold);
			return da;
		}
		
		public ADetectionActivation buildVoting(int votingWindowSize, int minVotes) {
			da = new EDA_Voting(in, samplesBeforeEvent, samplesAfterEvent, votingWindowSize, minVotes);
			return da;
		}
	}
}
