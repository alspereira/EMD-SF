package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.EventLookup;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.APowerSampleDTO;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.APowerEventDetector;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.IPowerEventDispatcher;

public class MaximaEventLookup extends AEventLookup {
	
	private int samplesBeforeEvent;
	private int samplesAfterEvent;
	private int scale;
	private float threshold;
	private int afterEventWindowSize;
	private int beforeEventWindowSize;
	
	public MaximaEventLookup(IPowerEventDispatcher eventDispatcher, int samplesBeforeEvent, int samplesAfterEvent, 
			int scale, float threshold, int beforeEventWindowSize, int afterEventWindowSize) {
		this.samplesBeforeEvent 	= samplesBeforeEvent;
		this.samplesAfterEvent 		= samplesAfterEvent;
		this.scale 					= scale;
		this.threshold 				= threshold;
		this.beforeEventWindowSize 	= beforeEventWindowSize;
		this.afterEventWindowSize 	= afterEventWindowSize;
		
		int queueSize = this.samplesBeforeEvent + this.samplesAfterEvent + this.beforeEventWindowSize + this.afterEventWindowSize;
		this.eventDispatcher = eventDispatcher;
		likelihoodSamples_IN = new ControlledArrayBlockingQueue<LikelihoodSample>(1000*queueSize + 1);
		buffer = new LikelihoodSample[queueSize + 1];		
	}
	
	private String datetime(long unixTime) {
		SimpleDateFormat dateFormat 	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date d = new Date(unixTime);
		return  dateFormat.format(d);
	}
		
	private boolean isMaxima(LikelihoodSample[] samples, float threshold, int scale) {
		int center = this.samplesBeforeEvent + this.beforeEventWindowSize;
		int left = center - scale;
		int right = center + scale;
		
		boolean up = true;
		boolean down = true;
		
		float value = samples[center].sample.getRealPower();
		
		LikelihoodSample ls = samples[this.samplesBeforeEvent + this.beforeEventWindowSize];
		
		//System.out.println("ds at: " + ls.sample.getID() + ": " + ls.data[3] + " sample: " + ls.sample.getRealPower() + " avgBefore: " +
		//		ls.data[0] + " avgAfter: " + ls.data[1] + " delta: " + (ls.data[1] - ls.data[0]) + " stDev: " + ls.data[2] + " test: " + ls.data[4] + 
		//		" stdBefore: " + ls.data[5] + " stdafter: " + ls.data[6] + " " + ls.data[7]);
		
		
		//float[] out = {avgDWBefore, avgDWAfter, stDevDW, Math.abs(eLikelihood), 0, stdBefore, stdAfter};
		
		//if(ls.sample.getID() == 10441 + 300)
		//	System.exit(-1);
		
		
		//System.out.println("center: " + center);
		
		if(samples[center].eventLikelihood == 0)
			return false;
		
		for(int i = center; i > left; i --) {		
			if(samples[center].eventLikelihood <= samples[i - 1].eventLikelihood) {
				up = false;
				//System.out.println("not up");
				break;
			}
		}
		
		for(int i = center; i < right - 1 && up == true; i ++) {
			if(samples[center].eventLikelihood <= samples[i + 1].eventLikelihood) {
				down = false;
				//System.out.println("not down");
				break;
			}
		}
		return (up && down);
	}
	// TODO check if we should use scale or other variable here
	private boolean isMaxima2(LikelihoodSample[] samples, float threshold, int scale) {
		
		boolean isMaxima = true;
		float value = Math.abs(samples[this.samplesBeforeEvent + this.beforeEventWindowSize].eventLikelihood);
		LikelihoodSample ls = samples[this.samplesBeforeEvent + this.beforeEventWindowSize];
		//System.out.println("ds at: " + ls.sample.getID() + ": " + value + " sample: " + ls.sample.getRealPower() + " avgBefore: " +
		//ls.avgBefore + " avgAfter: " + ls.avgAfter + " delta: " + (ls.avgAfter - ls.avgBefore) + " stDev: " + ls.stDev + " test: " + (ls.test));
		
		//if(ls.sample.getID() == 10441 + 300)
		//	System.exit(-1);
		float sLeft, sLeft2;
		
		float sRight, sRight2;
		
		if(value > threshold) {
			for(int i = 1; i < scale - 1; i++) {
				sLeft = Math.abs(samples[this.samplesBeforeEvent + this.beforeEventWindowSize - i].eventLikelihood);
				//sLeft2 = Math.abs(samples[this.samplesBeforeEvent + this.beforeEventWindowSize - (i+1)].eventLikelihood);
				sRight = Math.abs(samples[this.samplesBeforeEvent + this.beforeEventWindowSize + i].eventLikelihood);
				//sRight2 = Math.abs(samples[this.samplesBeforeEvent + this.beforeEventWindowSize + (i+1)].eventLikelihood);
				if(  (value < sLeft) || (value < sRight) )  {
					isMaxima = false;
					break;
				}
			}
		} 
		else {
			isMaxima = false;
		}
 		return isMaxima;
	}
	
	@Override
	protected boolean canQuit() {
		boolean canQuit = true;
		int minSize 	= buffer.length; 
		//System.out.println("min size: " + minSize + " samples in buffer: " + super.samplesInBuffer);
		
		// If there are still samples in the main queue or there are still enough samples in the buffer to proceed the tread cannot quit
		if(this.likelihoodSamples_IN.size() > 0 || super.samplesInBuffer >= minSize /*|| this.buffer.size() >= minSize*/ ) {
			System.out.println("size of the likelihood samples queue: " + this.likelihoodSamples_IN.size());
			canQuit = false;
		}
		return canQuit;
	}
	
	
	

	@Override
	public void run() {
		loadBuffer();
		LikelihoodSample ls;
		while( mustQuit == false || canQuit() == false) {

			if(isMaxima(buffer, threshold, scale)) {
				//System.out.println("event at " + buffer[this.samplesBeforeEvent + this.beforeEventWindowSize].sample.getID() + " | " + 
				//		time + "| " + buffer[this.samplesBeforeEvent + this.beforeEventWindowSize].eventLikelihood + " | " + ++counter);
				ls = buffer[this.samplesBeforeEvent + this.beforeEventWindowSize];
				
				super.notifyDispatcher(this.buffer, this.samplesBeforeEvent + this.beforeEventWindowSize,
						ls.eventLikelihood, null );
				
				
			}
			else {
				//System.out.println("not event at " + buffer[scale].sample.getUnixTimestamp() + " | " + 
				//		time + "| " + buffer[scale].eventLikelihood + " | " + ++counter);
			}
			super.updateBuffer();
		}
		System.out.println("gracefully quiting " + this.getClass().getName());
	}
}