package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;

public class DS_LikelihoodAnderson2014 extends ADetectionStatistics {
	
	ControlledArrayBlockingQueue<IPowerSample> in;
	
	private float threshold;
	private int edge;
	private PowerMetricType metric;
	
	public DS_LikelihoodAnderson2014(ControlledArrayBlockingQueue<IPowerSample> in, int preEventWindowSize,
			int postEventWindowSize, float threshold, PowerMetricType metric) {	
		super(in, new IPowerSample[preEventWindowSize + postEventWindowSize +1]);
		this.edge 		= preEventWindowSize;
		this.threshold 	= threshold;
		this.metric		= metric;
	}

	@Override
	protected DetectionStatistic getDetectionStatistics(IPowerSample[] samples) {
		IPowerSample[] sw;	
		sw = Arrays.copyOfRange(samples, 0, edge);
		float avgBefore = super.calculateAverage(sw, metric);
		
		//float stdDevBefore = super.calculateStandardDeviation(sw, metric);
		float stdDevBefore = super.calculateStandardDeviation(samples, metric, avgBefore);
		
		sw = Arrays.copyOfRange(samples, edge + 1, samples.length);
		float avgAfter = super.calculateAverage(sw, metric);
		//float stdDevAfter = super.calculateStandardDeviation(sw, metric);
		float stdDevAfter = super.calculateStandardDeviation(samples, metric, avgAfter);
		
		float avgDiff = avgAfter - avgBefore;
		float stDev = super.calculateStandardDeviation_2(samples, metric);
		double ds = 0.0;
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		data.put("stDevBefore", stdDevBefore);
		data.put("stDevAfter", stdDevAfter);
		data.put("stDev", stDev);
		data.put("avgBefore", avgBefore);
		data.put("avgAfter", avgAfter);
		data.put("avgDif", avgDiff);
		
		if( Math.abs(avgDiff) > this.threshold ) {
			ds = (double) Math.log(stdDevBefore/stdDevAfter);
			
			ds = (double) (ds + 
					( (Math.pow( samples[edge].getMetricValue(metric) - avgBefore, 2)) / ( 2*Math.pow(stdDevBefore, 2) ) )
					);
			
			ds = (double) (ds - 
					( (Math.pow( samples[edge].getMetricValue(metric) - avgAfter, 2)) / ( 2*Math.pow(avgAfter, 2)  ))
					);
		}
		
		return new DetectionStatistic(samples[edge], ds, data);
	}

}
