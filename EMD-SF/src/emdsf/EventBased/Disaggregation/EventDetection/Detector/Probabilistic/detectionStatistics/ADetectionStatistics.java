package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics;

import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;

public abstract class ADetectionStatistics implements Runnable {
	
	private ControlledArrayBlockingQueue<IPowerSample> in;
	public ControlledArrayBlockingQueue<DetectionStatistic> out;
	
	private IPowerSample[] buffer;
	private int samplesInBuffer = 0;

	public volatile boolean canQuit = false;
	//public volatile boolean mustQuit = false;
	
	public int preEventWindowSize;
	public int postEventWindowSize;
	public float threshold;
	
	public ADetectionStatistics() {
		
	}
		
	public ADetectionStatistics(ControlledArrayBlockingQueue<IPowerSample> in, IPowerSample[] buffer) {
		this.in = in;
		this.buffer = buffer;
		this.out = new ControlledArrayBlockingQueue<>(buffer.length * 1000);
		//System.out.println("ADS " + this.buffer.length);
	}

	protected boolean canQuit() {
		boolean q = false;
		//if(mustQuit == true)
		//	q = true;
		//else {
			if( canQuit == true && samplesInBuffer < buffer.length ) // samplesInBuffer == required samples for detection window
				q = true;
		//}
		return q;
	}

	protected void loadBuffer() {
		IPowerSample powerSample;
		int c = 0;
		while(c < buffer.length && canQuit() == false) {
			powerSample = in.controlledPoll();
			if( powerSample!= null ) {
				buffer[c++] = powerSample;
				this.samplesInBuffer ++;
			}
		}
	}
	
	protected void updateBuffer() {
		System.arraycopy(buffer, 1, buffer, 0, buffer.length - 1);
		this.samplesInBuffer --;

		IPowerSample powerSample = in.controlledPoll();
		if(powerSample != null) {
			buffer[buffer.length -1] = powerSample;
			this.samplesInBuffer ++;
		}
	}
	
	// media actual
	// valor posiçao zero
	// novo valor
	
	private float currentAverage;
	private boolean start = true;
	//private float newValue;
	private float removedValue;
	
	// SUB average = ((average * nbValues) - value) / (nbValues - 1);
	// ADD average = average + ((value - average) / nbValues)
	
	/*
	public float calculateAverage(IPowerSample[] powerSamples, PowerMetricType metric) {
		//float newAverage = 0.0f;
		//float a;
		if(start == false) {
			// remove
			currentAverage = ((currentAverage * powerSamples.length) - removedValue) / (powerSamples.length - 1);
			// add
			currentAverage = currentAverage + (( powerSamples[powerSamples.length - 1].getMetricValue(metric) 
					- currentAverage) / powerSamples.length);
			removedValue = powerSamples[0].getMetricValue(metric);
			//newAverage = calculateAverage(powerSamples, metric);
			//currentAverage = newAverage;
		}
		else {
			currentAverage = calculateAverage_2(powerSamples, metric);
			removedValue = powerSamples[0].getMetricValue(metric);
			start = false;
		}
		return this.currentAverage;
	}*/
	
	protected float calculateAverage(IPowerSample[] powerSamples, PowerMetricType metric) {
		float average = 0.0f;
		
		for(int i = 0; i < powerSamples.length; i++) {
			average += powerSamples[i].getMetricValue(metric);
		}

		return average / powerSamples.length;		
	}
	
	public float calculateStandardDeviation(IPowerSample[] powerSamples, PowerMetricType metric, float average) {
		float variance 	= 0.0f;
		//float average 	= calculateAverage(powerSamples, metric);
		
		for(int i = 0; i < powerSamples.length; i++) {
			variance += Math.pow(powerSamples[i].getMetricValue(metric) - average, 2);
		}
		
		return (float) Math.sqrt( variance / (powerSamples.length - 1) );
	}
	
	public float calculateStandardDeviation_2(IPowerSample[] powerSamples, PowerMetricType metric) {
		float variance 	= 0.0f;
		float average 	= calculateAverage(powerSamples, metric);
		
		for(int i = 0; i < powerSamples.length; i++) {
			variance += Math.pow(powerSamples[i].getMetricValue(metric) - average, 2);
		}
		
		return (float) Math.sqrt( variance / (powerSamples.length - 1) );
	}
	
	protected abstract DetectionStatistic getDetectionStatistics(IPowerSample[] samples);

	@Override
	final public void run() {
		this.updateBuffer();
		try {
			loadBuffer();
			while(canQuit() == false) {
				//System.out.println(this.getClass().getSimpleName());
				this.out.put( this.getDetectionStatistics( this.buffer ) );
				this.updateBuffer(); 
			}
			
			System.out.println("quit mf quit");
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Gracefuly quiting: " + this.getClass().getSimpleName());
			//this.out.mustQuit = true;
			this.out.quit();
		}	
	}
	
	//////////////
/*	
	public class DetectionStatistic {
		public IPowerSample sample;
		//public float detectionStatistic;
		public double detectionStatistic;
		public Map<String, Object> data;

		public DetectionStatistic(IPowerSample sample, double detectionStatistic, Map<String, Object> data) {
			this.sample = sample;
			this.detectionStatistic = detectionStatistic;
			this.data = data;
		}
		
	}*/

}
