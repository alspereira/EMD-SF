package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics;

import java.util.Map;

import emdsf.Core.PowerComputation.DTO.IPowerSample;

public class DetectionStatistic {
	public IPowerSample sample;
	//public float detectionStatistic;
	public double detectionStatistic;
	public Map<String, Object> data;

	public DetectionStatistic(IPowerSample sample, double detectionStatistic, Map<String, Object> data) {
		this.sample = sample;
		this.detectionStatistic = detectionStatistic;
		this.data = data;
	}
}