package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.APowerSampleDTO;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.APowerEventDetector;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.EventLookup.AEventLookup;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.EventLookup.MaximaEventLookup;
import util.visualization.DetectorChart;

public class ED_Pereira2014 extends APowerEventDetector implements Runnable {
	
	public static String NAME = "Pereira_2014_Maxima";
	
	private int		samplingFrequency;		
	private int		detectionWindowSizeBefore; 	// used to be known as preEventWindow
	private int 	detectionWindowSizeAfter;	// used to be known as postEventWindow
	
	private int 	beforeEventWindowSize; 	// default is 3
	private int 	afterEventWindowSize;   // default is 3 
	private float 	thresholdValue; 		// default is 0.75 of the appliance with less power consumption
	
	
	public ED_Pereira2014(int samplingFrequency, int detectionWindowSizeBefore, int detectionWindowSizeAfter, 
			int beforeEventWindowSize, int afterEventWindowSize, float thresholdValue, 
			ControlledArrayBlockingQueue<IPowerSample> powerSamples_IN) {
		
		
		this.samplingFrequency			= samplingFrequency;
		this.detectionWindowSizeBefore	= samplingFrequency * detectionWindowSizeBefore;
		this.detectionWindowSizeAfter	= samplingFrequency * detectionWindowSizeAfter;
		this.beforeEventWindowSize		= samplingFrequency * beforeEventWindowSize;
		this.afterEventWindowSize		= samplingFrequency * afterEventWindowSize;
		this.thresholdValue				= thresholdValue;
		this.powerSamples_IN 			= powerSamples_IN;
		
		int bsize = this.beforeEventWindowSize + this.detectionWindowSizeBefore + /*this.samplingFrequency + */
				this.detectionWindowSizeAfter + this.afterEventWindowSize;
		buffer = new IPowerSample[bsize + 1];
		
		// create a default event lookup
		super.setEventLookup(
				new MaximaEventLookup(this, 0, 0, this.samplingFrequency, 0f,this.detectionWindowSizeBefore,this.detectionWindowSizeAfter ));
		
	}
	
	private boolean canQuit() {
		boolean canQuit = true;
		int minSize 	= 1 + beforeEventWindowSize + detectionWindowSizeBefore + /*samplingFrequency + */ detectionWindowSizeAfter + afterEventWindowSize; 
		//System.out.println("min size: " + minSize);
		// If there are still samples in the main queue or there are still enough samples in the buffer to proceed the tread cannot quit
		if(this.powerSamples_IN.size() > 0 || super.samplesInBuffer >= minSize /*|| this.buffer.size() >= minSize*/ ) {
			//System.out.println("size of the power samples queue: " + this.powerSamples_IN.size());
			canQuit = false;
		}
			
		
		return canQuit;
	}
	
	private IPowerSample[] getDetectionWindow() {
		IPowerSample[] dw 		= new APowerSampleDTO[ detectionWindowSizeBefore + detectionWindowSizeAfter + 1];		
		System.arraycopy(buffer, beforeEventWindowSize, dw, 0, dw.length);
		return dw;
	}
	
	private float[] calculateEventLikelihood(IPowerSample[] samples, PowerMetricType metric) {
		IPowerSample[] sw;
		sw = Arrays.copyOfRange(samples, 0, detectionWindowSizeBefore);
		float avgDWBefore = super.calculateAverage(sw, metric);
		
		sw = Arrays.copyOfRange(samples, detectionWindowSizeBefore + 1, detectionWindowSizeBefore + detectionWindowSizeAfter + 1);
		float avgDWAfter = super.calculateAverage(sw, metric);
		float avgDiffDW = avgDWAfter - avgDWBefore;
		float eLikelihood = 0.0f;
		float stDevDW = super.calculateStandardDeviation(samples, metric);
		
		if(Math.abs(avgDiffDW) >= thresholdValue) {
			// there is a good chance of an event happening here, let us see the likelihood
			
			eLikelihood = (float) ((avgDiffDW) / (Math.pow(stDevDW, 2)));
			eLikelihood = eLikelihood * (float) (Math.abs(samples[detectionWindowSizeBefore].getMetricValue(metric) - ((avgDWBefore + avgDWAfter)*0.5)));	
			
			
		}		
		//return eLikelihood;
		float[] out = {avgDWBefore, avgDWAfter, Math.abs(stDevDW), Math.abs(eLikelihood)};
		return out;
	}
	
	@Override
	public void run() {
		
		float eLikelihood = 0.0f;
		IPowerSample[] samples;
		
		this.loadBuffer();
		
		// initialize eventLookup
		new Thread(this.eventLookup).start();
		
		while(mustQuit == false || canQuit() == false) {
			samples = getDetectionWindow();
			
			float[] res = calculateEventLikelihood(samples, PowerMetricType.REAL_POWER);
			
			//eLikelihood = calculateEventLikelihood(samples, PowerMetricTypeEnum.REAL_POWER)[3];
			
			
			this.eventLookup.addLikelihoodSample(samples[detectionWindowSizeBefore], res[3], res);
			//System.out.println("go and update the buffer");
			super.updateBuffer();
			//System.out.println("done updating the buffer. size: " + samplesInBuffer);
		}
		
		// quit event lookup
		//this.eventLookup.mustQuit = true;
		//this.powerSamples_IN.mustQuit = true;
		
		System.out.println("gracefully quiting " + this.getClass().getName());
		this.eventLookup.mustQuit = true;
		this.eventLookup.likelihoodSamples_IN.mustQuit = true;
	}

}
