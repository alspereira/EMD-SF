package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.EventLookup;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.IPowerEventDispatcher;

public abstract class AEventLookup implements Runnable { 
	
	protected IPowerEventDispatcher eventDispatcher;
	public ControlledArrayBlockingQueue<LikelihoodSample> likelihoodSamples_IN;
	protected LikelihoodSample[] buffer;
	
	protected int samplesInBuffer = 0;
	
	public volatile boolean mustQuit = false;
	
	public void addLikelihoodSample(IPowerSample sample, float eventLikelihood, 
			float[] data) {
		try {
			likelihoodSamples_IN.put(new LikelihoodSample(sample, eventLikelihood, data));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void notifyDispatcher(LikelihoodSample[] likelihoodSamples, int index, float ds, float[] data) {
		IPowerSample[] powerSamples = new IPowerSample[likelihoodSamples.length];
		//Iterator<LikelihoodSample> it = likelihoodSamples.iterator();
		int loopIndex = 0;
		while( loopIndex < likelihoodSamples.length ) {
			powerSamples[loopIndex] = likelihoodSamples[loopIndex].sample;	
			loopIndex++;
		}
		this.eventDispatcher.dispatachPowerEvent(powerSamples, index, ds, data);
	}
	
	protected void loadBuffer() {
		LikelihoodSample likelihoodSample;
		int c = 0;
		while(c < buffer.length && mustQuit == false) {
			likelihoodSample = likelihoodSamples_IN.controlledPoll();
			if(likelihoodSample != null) {
				buffer[c++] = likelihoodSample;
				this.samplesInBuffer ++;
			}
		}
	}
	
	// removes the first and adds a new one
	protected void updateBuffer() {	
		System.arraycopy(buffer, 1, buffer, 0, buffer.length - 1);
		this.samplesInBuffer --;
		
		LikelihoodSample likelihoodSample = likelihoodSamples_IN.controlledPoll();
		if(likelihoodSample != null) {
			buffer[buffer.length - 1] = likelihoodSample;
			this.samplesInBuffer ++;
		}
	}
	
	
	abstract protected boolean canQuit();
	
	// maybe this should go out of here
	protected class LikelihoodSample {
		IPowerSample sample;
		float eventLikelihood;
		float[] data;
		
		public LikelihoodSample(IPowerSample sample, float eventLikelihood, float[] data) {
			this.sample = sample;
			this.eventLikelihood = eventLikelihood;
			this.data = data;
		}		
	}
}