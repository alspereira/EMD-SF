package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation.ADetectionActivation;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DetectionStatistic;

public class ED_Pereira2014_Maxima extends ADetectionActivation {
	
	private int precision;
	private int edge;
	private float threshold;
	
	public ED_Pereira2014_Maxima(ControlledArrayBlockingQueue<DetectionStatistic> in, 
			int samplesBeforeEvent, int samplesAfterEvent,
			int precision, float threshold) {
		
		super(in,  new DetectionStatistic[samplesBeforeEvent + samplesAfterEvent + 2 * precision + 1]);
		this.precision 	= precision;
		this.edge 		= samplesBeforeEvent + precision;
		this.threshold 	= threshold;
	}

	@Override
	protected Activation activate(DetectionStatistic[] statistics) {
		boolean isMaxima = true;
		Map<String, Object> data = new LinkedHashMap<String,Object>();

		double ds = statistics[edge].detectionStatistic;
		double dsLeft, dsRight;
		
		data.put("detection_statistic", ds);
		data.put("edge", edge);
		ds = Math.abs(ds);
		if(ds > threshold) {
			for(int i = 1; i < this.precision - 1; i++) {
				dsLeft = Math.abs( statistics[edge - i].detectionStatistic );
				dsRight = Math.abs( statistics[edge + i].detectionStatistic );
				if(  (ds < dsLeft) || (ds < dsRight) )  {
					isMaxima = false;
					break;
				}
			}
		}
		else {
			isMaxima = false;
		}	
		return new Activation(isMaxima, data);
	}
}
