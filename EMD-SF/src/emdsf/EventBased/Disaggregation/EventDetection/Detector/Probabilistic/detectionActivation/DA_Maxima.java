package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation;

import java.util.LinkedHashMap;
import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation.ADetectionActivation;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DetectionStatistic;

public class DA_Maxima extends ADetectionActivation {
	
	private int precision;
	private int edge;
	private float threshold;
	
	public DA_Maxima(ControlledArrayBlockingQueue<DetectionStatistic> in, 
			int samplesBeforeEvent, int samplesAfterEvent,
			int precision, float threshold) {
		
		super(in,  new DetectionStatistic[samplesBeforeEvent + samplesAfterEvent + 2 * precision + 1]);
		this.precision 	= precision;
		this.edge 		= samplesBeforeEvent + precision;
		this.threshold 	= threshold;
	}

	private boolean hasMaxima(DetectionStatistic[] helpers) {
		boolean up = true;
		boolean down = true;
		// Should also check if the size of the array is odd!
		int center = edge;
		
		
		for(int i = center; i > 0; i --) {
			if(Math.abs( helpers[center].detectionStatistic ) < Math.abs( helpers[i-1].detectionStatistic )) {
				up = false;
				break;
			}
		}
		for(int i = center; i < helpers.length - 1; i ++) {
			if(Math.abs( helpers[center].detectionStatistic ) < Math.abs( helpers[i+1].detectionStatistic )) {
				down = false;
				break;
			}
		}
		return (up && down);
	}
	@Override
	protected Activation activate(DetectionStatistic[] statistics) {
		boolean isMaxima = true;
		Map<String, Object> data = null; // = new LinkedHashMap<String,Object>();

		double ds = statistics[edge].detectionStatistic;
		double dsLeft, dsRight;
		
		//data.put("detection_statistic", ds);
		//data.put("edge", edge);
		ds = Math.abs(ds);
		
		//if(ds > threshold)
		//	isMaxima = this.hasMaxima(statistics);
		//System.out.println(this.precision);
		
		
		if(ds > threshold) {
			for(int i = 1; i < this.precision - 1; i++) {
				dsLeft = Math.abs( statistics[edge - i].detectionStatistic );
				dsRight = Math.abs( statistics[edge + i].detectionStatistic );
				if(  (ds < dsLeft) || (ds <= dsRight) )  {
					isMaxima = false;
					break;
				}
				 data = new LinkedHashMap<String,Object>();
				 data.put("detection_statistic", ds);
				data.put("edge", edge);
			}
		}
		else {
			isMaxima = false;
		}
		
		
		return new Activation(isMaxima, data);
	}
}
