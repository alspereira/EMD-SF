package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation;

import java.util.Map;

public class Activation {
	boolean activate;
	Map<String, Object> data;
	
	public Activation(boolean activate, Map<String, Object> data) {
		this.activate = activate;
		this.data = data;
	}
}
