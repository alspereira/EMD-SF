package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.IPowerEventDispatcher;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DetectionStatistic;
import emdsf.EventBased.Disaggregation.EventDetection.Event.IPowerEventListener;
import emdsf.EventBased.Disaggregation.EventDetection.Event.PowerEvent;

public abstract class ADetectionActivation implements IPowerEventDispatcher, Runnable {
	
	private ControlledArrayBlockingQueue<DetectionStatistic> in;
	
	private List<IPowerEventListener> powerEventListeners  = new ArrayList<IPowerEventListener>();
	
	private DetectionStatistic[] buffer;
	
	private int samplesInBuffer = 0;
	
	public volatile boolean canQuit = false;
	
	public ADetectionActivation() {
		
	}
	
	public ADetectionActivation(ControlledArrayBlockingQueue<DetectionStatistic> in, 
			DetectionStatistic[] buffer) {
		this.in = in;
		this.buffer = buffer;
		///System.out.println("ADA " + this.buffer.length);
	}

	@Override
	public void addPowerEventListener(IPowerEventListener listener) {
		this.powerEventListeners.add(listener);
	}

	@Override
	public void removePowerEventListener(IPowerEventListener listener) {
		this.powerEventListeners.remove(listener);
	}

	@Override
	public void dispatachPowerEvent(IPowerSample[] powerSamples, int index, float ds, float[] data) {	
		// this is only here for compatibility
	}
	
	public void dispatchPowerEvent(IPowerSample[] powerSamples, Map<String, Object> data) {
		PowerEvent pe = new PowerEvent(this, powerSamples, data);
		Iterator<IPowerEventListener> it = powerEventListeners.iterator();
		while(it.hasNext()) {
			it.next().onEventDetected(pe);
		}
	}
	
	protected boolean canQuit() {
		boolean q = false;

		if(canQuit == true && (in.size() == 0 || this.samplesInBuffer > this.buffer.length) ) {
			//System.out.println("size " + (in.size() > 0));
			//System.out.println("buffer " + (this.samplesInBuffer > this.buffer.length));
			//System.out.println("variable " + (canQuit == false)) ;
			q = true;
		}
		//else {
		//	System.out.println("else");
		//}
		q = false;
		return q;
	}
	
	protected void loadBuffer() {
		DetectionStatistic detectionStatistic;
		int c = 0;
		while(c < buffer.length && canQuit() == false) {
			detectionStatistic = in.controlledPoll();
			if( detectionStatistic!= null ) {
				buffer[c++] = detectionStatistic;
				this.samplesInBuffer ++;
			}
		}
		
		//System.out.println("done loading bufer");
	}
	
	protected void updateBuffer() {
		System.arraycopy(buffer, 1, buffer, 0, buffer.length - 1);
		this.samplesInBuffer --;
		
		//System.out.println("will update da buffer");
		DetectionStatistic detectionStatistic = in.controlledPoll();
		if(detectionStatistic != null) {
			buffer[buffer.length -1] = detectionStatistic;
			this.samplesInBuffer ++;
			//System.out.println("added sample to da buffer");
		}
		
		//System.out.println("done updating buffer");
	}
	
	private final void handleData(DetectionStatistic[] statistics) {
		Activation a = this.activate(statistics);
		if( a.activate == true ) {
			IPowerSample[] powerSamples = new IPowerSample[statistics.length];
			int loopIndex = 0;
			while( loopIndex < statistics.length ) {
				powerSamples[loopIndex] = statistics[loopIndex].sample;	
				loopIndex++;
			}
			this.dispatchPowerEvent(powerSamples, a.data);
		}
	}
		
	protected abstract Activation activate(DetectionStatistic[] statistics);
	
	@Override
	public final void run() {
		
		try {
			this.loadBuffer();
			while( canQuit() == false) {
				this.handleData( this.buffer );
				this.updateBuffer();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Gracefuly quiting: " + this.getClass().getSimpleName());
		}
	}
	
	public class Activation {
		boolean activate;
		Map<String, Object> data;
		
		public Activation(boolean activate, Map<String, Object> data) {
			this.activate = activate;
			this.data = data;
		}
	}

}
