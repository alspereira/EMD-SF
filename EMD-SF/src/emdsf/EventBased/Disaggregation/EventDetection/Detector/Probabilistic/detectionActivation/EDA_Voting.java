package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionActivation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.DetectionStatistic;

public class EDA_Voting extends ADetectionActivation {
	
	private int[] votes;
	private int minVotes;
	private int votingWindowSize;	
	private int edge;
	
	public EDA_Voting(ControlledArrayBlockingQueue<DetectionStatistic> in, 
			int samplesBeforeEvent, int samplesAfterEvent,
			int votingWindowSize, int minVotes) {
		super(in, new DetectionStatistic[samplesBeforeEvent + Math.max(samplesAfterEvent, votingWindowSize) + 1]);
		this.edge 				= samplesBeforeEvent;
		this.minVotes 			= minVotes;
		this.votingWindowSize 	= votingWindowSize;
		this.votes 				= new int[this.votingWindowSize + 1];
	}
	
	private Integer[] findMaximaIndex(DetectionStatistic[] statistics) {
		double maximum;
		double ds_abs;
		List<Integer> maximumIndex = new ArrayList<Integer>();
		
		int step = edge;
		Integer[] out = new Integer[0];
		
		while(step < statistics.length) {
			//System.out.println("step :" +step);
			//System.out.println(statistics.length);
			ds_abs = Math.abs(statistics[step].detectionStatistic);
			//if(Math.abs(statistics[step].detectionStatistic) > 0) {
			if(ds_abs > 0) {
				//maximum = Math.abs(statistics[step].detectionStatistic);
				maximum = ds_abs;
				maximumIndex.add(step);
				step ++;
				for(int i = step; i < statistics.length; i++) {
					// new maximum
					ds_abs = Math.abs(statistics[i].detectionStatistic);
					//if(Math.abs(statistics[i].detectionStatistic) > maximum) {
					if(/*ds_abs > 0 &&*/ ds_abs > maximum) {
						maximumIndex.clear();
						maximumIndex.add(i);
						//maximum = Math.abs( statistics[i].detectionStatistic );
						maximum = ds_abs;
					}
					//else if(Math.abs(statistics[i].detectionStatistic) == maximum) {
					else if(/*ds_abs > 0 &&*/ ds_abs == maximum) {
						// just another maximum
						maximumIndex.add(i);
					}
					//step ++;
				}
				step = statistics.length; // break the while
				out = new Integer[maximumIndex.size()];
			}
			else {
				step++; // check next index to see if greater than zero
			}
		}
		return maximumIndex.toArray(out);
	}

	@Override
	protected Activation activate(DetectionStatistic[] statistics) {
		boolean isEvent = false;
		Integer[] max;
		Map<String, Object> data = new LinkedHashMap<String,Object>();
		
		
		max = findMaximaIndex(statistics);
		//System.out.println("max length: " + max.length);
		for(int m = 0; m < max.length; m++) {
			votes[max[m] - edge] += 1;
		}
		
		// find a vote in first position
		if(votes[0] > minVotes) {
			data.put("detection_statistic", statistics[0].detectionStatistic);
			data.put("votes", votes[0]);
			data.put("minVotes", minVotes);
			data.put("edge", edge);
			isEvent = true;
			//System.exit(-1);
		}
		
		// shift votes to the left
		System.arraycopy(votes, 1, votes, 0, votes.length-1);
		votes[votes.length - 1] = 0;
		
		return new Activation(isEvent, data);
	}
}
