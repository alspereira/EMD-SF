package emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;

public class DS_LikelihoodPereira2015 extends ADetectionStatistics {

	private float threshold;
	private int edge;
	private PowerMetricType metric;
	
	public DS_LikelihoodPereira2015(ControlledArrayBlockingQueue<IPowerSample> in, int preEventWindowSize,
			int postEventWindowSize, float threshold, PowerMetricType metric) {	
		super(in, new IPowerSample[preEventWindowSize + postEventWindowSize +1]);
		this.edge 		= preEventWindowSize;
		this.threshold 	= threshold;
		this.metric		= metric;
	}

	@Override
	protected DetectionStatistic getDetectionStatistics(IPowerSample[] samples) {
		IPowerSample[] sw;	
		sw = Arrays.copyOfRange(samples, 0, edge);
		float avgBefore = super.calculateAverage(sw, metric);
		//float stdDevBefore = super.calculateStandardDeviation(sw, metric);
		float stdDevBefore = super.calculateStandardDeviation(samples, metric, avgBefore);
		
		sw = Arrays.copyOfRange(samples, edge + 1, samples.length);
		float avgAfter = super.calculateAverage(sw, metric);
		//float stdDevAfter = super.calculateStandardDeviation(sw, metric);
		float stdDevAfter = super.calculateStandardDeviation(samples, metric, avgAfter);
		
		float avgDiff = avgAfter - avgBefore;
		float stDev = super.calculateStandardDeviation_2(samples, metric);
		float ds = 0.0f;
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		data.put("stDevBefore", stdDevBefore);
		data.put("stDevAfter", stdDevAfter);
		data.put("stDev", stDev);
		data.put("avgBefore", avgBefore);
		data.put("avgAfter", avgAfter);
		data.put("avgDif", avgDiff);
		
		if( Math.abs(avgDiff) > this.threshold ) {	
			ds = (float) ((avgDiff) / (Math.pow(stDev, 2)));
			ds = ds * (float) (Math.abs(samples[edge].getMetricValue(metric) - ((avgBefore + avgAfter)*0.5)));	
		}
		return new DetectionStatistic(samples[edge], ds, data);
	}
}
