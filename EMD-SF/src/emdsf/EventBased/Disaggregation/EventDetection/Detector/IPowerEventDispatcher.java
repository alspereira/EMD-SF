package emdsf.EventBased.Disaggregation.EventDetection.Detector;

import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.EventBased.Disaggregation.EventDetection.Event.IPowerEventListener;

public interface IPowerEventDispatcher {
	
	public void addPowerEventListener(IPowerEventListener listener);
	public void removePowerEventListener(IPowerEventListener listener);
	public void dispatachPowerEvent(IPowerSample[] powerSamples, int index, float ds, float[] data);
	
}