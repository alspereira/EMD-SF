package emdsf.EventBased.Disaggregation.EventDetection.Detector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.APowerSampleDTO;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.EventLookup.AEventLookup;
import emdsf.EventBased.Disaggregation.EventDetection.Event.IPowerEventListener;
import emdsf.EventBased.Disaggregation.EventDetection.Event.PowerEvent;

public abstract class APowerEventDetector implements IPowerEventDispatcher {

	protected List<IPowerEventListener> powerEventListeners  = new ArrayList<IPowerEventListener>();
	
	protected ControlledArrayBlockingQueue<IPowerSample> powerSamples_IN;
	protected IPowerSample[] buffer;
	protected int bufferSize;
	public volatile boolean mustQuit = false;
	protected AEventLookup eventLookup;
	
	protected int samplesInBuffer = 0;
	
	@Override
	public synchronized void addPowerEventListener(IPowerEventListener listener) {
		this.powerEventListeners.add(listener);
	}
	
	@Override
	public synchronized void removePowerEventListener(IPowerEventListener listener) {
		this.powerEventListeners.remove(listener);
	}
	
	@Override
	public void dispatachPowerEvent(IPowerSample[] powerSamples, int index, float ds, float[] data) {
		PowerEvent pe = new PowerEvent(this, powerSamples, index, ds);
		Iterator<emdsf.EventBased.Disaggregation.EventDetection.Event.IPowerEventListener> listeners = this.powerEventListeners.iterator(); 
		while(listeners.hasNext()) {
			(listeners.next()).onEventDetected(pe);
		}
	}
	
	
	public void setEventLookup(AEventLookup eventLookup) { // this needs to move somewhere else or be removed at all
		this.eventLookup = eventLookup;
	}
	
	protected void loadBuffer() {
		IPowerSample powerSample;
		int c = 0;
		while(c < buffer.length && mustQuit == false) {
			powerSample = powerSamples_IN.controlledPoll();
			if( powerSample!= null ) {
				buffer[c++] = powerSample;
				this.samplesInBuffer ++;
			}
		}
		//System.out.println("buffer loaded with: " + samplesInBuffer + " samples!");
	}
	
	protected void updateBuffer() {
		// i need to bring the new second, not just a new sample
		System.arraycopy(buffer, 60, buffer, 0, buffer.length - 60);
		this.samplesInBuffer -= 60;
		IPowerSample powerSample;
		int step = 60;
		while(step > 0 /*this.samplesInBuffer < buffer.length*/) {
			powerSample = powerSamples_IN.controlledPoll();
			if(powerSample != null) {
				buffer[buffer.length - step] = powerSample;
				//System.out.println("update buffer: " + (buffer.length - step));
				this.samplesInBuffer ++;
				step --;
			}
		}
	}
	
	protected void _updateBuffer() {
		System.arraycopy(buffer, 1, buffer, 0, buffer.length - 1);
		this.samplesInBuffer --;

		
		IPowerSample powerSample = powerSamples_IN.controlledPoll();
		
		if(powerSample != null) {
			buffer[buffer.length-1] = powerSample;
			this.samplesInBuffer ++;
		}
	}

	// HELPER METHODS
	
	public float calculateAverage(IPowerSample[] powerSamples, PowerMetricType metric) {
		float average = 0.0f;
		
		for(int i = 0; i < powerSamples.length; i++) {
			average += powerSamples[i].getMetricValue(metric);
		}

		return average / powerSamples.length;		
	}
	
	public float calculateStandardDeviation(IPowerSample[] powerSamples, PowerMetricType metric) {
		float variance 	= 0.0f;
		float average 	= calculateAverage(powerSamples, metric);
		
		for(int i = 0; i < powerSamples.length; i++) {
			variance += Math.pow(powerSamples[i].getMetricValue(metric) - average, 2);
		}
		
		return (float) Math.sqrt( variance / (powerSamples.length - 1) );
	}
}
