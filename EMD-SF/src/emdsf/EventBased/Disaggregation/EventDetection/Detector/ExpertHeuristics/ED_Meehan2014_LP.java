package emdsf.EventBased.Disaggregation.EventDetection.Detector.ExpertHeuristics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.APowerSampleDTO;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.APowerEventDetector;

/*
 * current RMS
 * window size (default: 4 seconds)
 * event threshold (default: 75% of smallest appliance)
 * previous event must not have occurred in the last three seconds
 * 
 * */

/**
 * 
 * @author lucaspereira
 * 
 * in: array blocking queue with power samples
 *
 */
public class ED_Meehan2014_LP extends APowerEventDetector implements Runnable {
	
	private int		samplingFrequency;		
	private int		detectionWindowSizeBefore; 	// default is 4 seconds
	private int 	detectionWindowSizeAfter;	// default is 4 seconds
	private int		averageWindowSizeBefore;	// default is 1 second
	private int		averageWindowSizeAfter;		// default is 1 second	
	private int 	minElapsedTime;			// default is 3 seconds
	private float 	thresholdValue; 		// default is 0.75 of the appliance with less power consumption
	private int 	beforeEventWindowSize; 	// default is 3
	private int 	afterEventWindowSize;   // default is 3 
	
	//private ControlledArrayBlockingQueue<APowerSampleDTO> powerSamples_IN;
		
	//private List<APowerSampleDTO> buffer;
	
	public volatile boolean mustQuit = false;
	
	public int gapBefore;
	public int beforeDWS;
	public int awsBefore;
	public int awsAfter;
	
	public int edge;
	
	public ED_Meehan2014_LP(int beforeDWS, int gapBefore, int awsBefore, int afterDWS, int gapAfter,
			int awsAfter, int minElapsedTime, float thresholdValue) {
		this.beforeDWS = beforeDWS;
		this.gapBefore = gapBefore;
		this.awsBefore = awsBefore;
		//this.afterDWS = afterDWS;
		//this.gapAfter = gapAfter;
		this.awsAfter = awsAfter;
		this.minElapsedTime = minElapsedTime;
		this.thresholdValue = thresholdValue;
		int bufferSize 	= Math.max(gapBefore + beforeDWS, awsBefore) + Math.max(awsAfter, gapAfter + afterDWS);
		buffer 			= new IPowerSample[bufferSize];
		if(bufferSize == 0)
			System.out.println("buffer size: " + bufferSize);
	}
	
	public void setPowerSamplesQueue_IN(ControlledArrayBlockingQueue<IPowerSample> queue) {
		this.powerSamples_IN = queue;
	}
	
	public IPowerSample[] getPreviousSampleWindow() {
		int fromIndex = Math.max(0, beforeDWS - awsBefore);
		int toIndex = fromIndex + awsBefore;
		//System.out.println("Previous sample: " + fromIndex + "  |  " + toIndex);
		return Arrays.copyOfRange(buffer, fromIndex, toIndex);
	}
	
	public IPowerSample[] getCurrentSampleWindow() {
		int fromIndex = Math.max(beforeDWS + gapBefore, awsBefore);
		int toIndex = fromIndex + awsAfter;
		//System.out.println("Current sample: " + fromIndex + "  |  " + toIndex);
		return Arrays.copyOfRange(buffer, fromIndex, toIndex);
	}
	
	public IPowerSample[] _getPreviousSampleWindow() {
		int fromIndex			= beforeEventWindowSize;
		int toIndex				= beforeEventWindowSize + averageWindowSizeBefore;
		//System.out.println(fromIndex);
		//System.out.println(toIndex);
		return Arrays.copyOfRange(buffer, fromIndex, toIndex);
	}
	
	public IPowerSample[] _getCurrentSampleWindow() {
		int fromIndex 			= beforeEventWindowSize + detectionWindowSizeBefore + averageWindowSizeBefore;
		int toIndex 			= fromIndex + averageWindowSizeAfter;
		return Arrays.copyOfRange(buffer, fromIndex, toIndex);	
	}
	
	private boolean canQuit() {
		boolean canQuit = true;
		int minSize 	= beforeEventWindowSize + detectionWindowSizeBefore + samplingFrequency + detectionWindowSizeAfter + afterEventWindowSize; 
		// If there are still samples in the main queue or there are still enough samples in the buffer to proceed the tread cannot quit
		//if(this.powerSamples_IN.size() > 0 || this.buffer.length >= minSize )
		//	canQuit = false;
		canQuit = false;
		return canQuit;
	}
	
	@Override
	public void run() {
		float 	beforeAverage 			= 0;
		float 	currentAverage 			= 0;
		long	lastEventTimestamp 		= 0l;
		long	currentSampleTimestamp 	= 0l;
		
		IPowerSample[] psw;
		IPowerSample[] csw;
		
		// load the buffer
		this.loadBuffer();
		
		while(mustQuit == false || canQuit() == false) {
			
			psw = getPreviousSampleWindow();
			csw = getCurrentSampleWindow();
			beforeAverage 	= calculateAverage(psw,PowerMetricType.REAL_POWER);			
			currentAverage 	= calculateAverage(csw,PowerMetricType.REAL_POWER);
					
			currentSampleTimestamp = csw[edge].getUnixTimestamp();
			
			if(Math.abs(currentAverage - beforeAverage)  > this.thresholdValue) {
				if((currentSampleTimestamp - lastEventTimestamp) > this.minElapsedTime) {
					lastEventTimestamp = currentSampleTimestamp;
					//System.out.println("event: " +  csw[0].getUnixTimestamp());
					this.dispatachPowerEvent((IPowerSample[]) buffer, 
							Math.max(this.beforeDWS + this.gapBefore, awsBefore ) + (edge), 
							0f, null);
				}
				/*else {
					System.out.println("And event was found at : " + csw[0].getUnixTimestamp() + 
							" with a time difference of :" + (currentSampleTimestamp - lastEventTimestamp) );
				}*/
				
			}
			//System.out.println("passagem no loop before update");
			updateBuffer();	
			//System.out.println("passagem no loop after update");
		}
		System.out.println("quiting this class: " + this.getClass().toString());
	}
}