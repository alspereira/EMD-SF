package emdsf.EventBased.Disaggregation.EventDetection.Detector.ExpertHeuristics;

import java.util.LinkedHashMap;
import java.util.Map;

public class Heuristic {

	public float beforeDWS, gapBefore, afterDWS, gapAfter, awsBefore, awsAfter, pThreshold, minElapsedTime;
	public int edge;
	
	public Heuristic(float beforeDWS, float gapBefore, 
			float afterDWS, float gapAfter, 
			float awsBefore, float awsAfter,
			float pThreshold, int edge, float minElapsedTime) {
		this.beforeDWS = beforeDWS;
		this.gapBefore = gapBefore;
		this.afterDWS = afterDWS;
		this.gapAfter = gapAfter;
		this.awsBefore = awsBefore;
		this.awsAfter = awsAfter;
		this.pThreshold = pThreshold;
		this.edge = edge;
		this.minElapsedTime = minElapsedTime;
	}
	
	public Map<String, Object> toMap() {
		Map<String, Object> m = new LinkedHashMap<String, Object>();
		m.put("beforeDWS", this.beforeDWS);
		m.put("gapBefore", this.gapBefore);
		m.put("afterDWS", this.afterDWS);
		m.put("gapAfter", this.gapAfter);
		m.put("awsBefore", this.awsBefore);
		m.put("awsAfter", this.awsAfter);
		m.put("pThreshold", this.pThreshold);
		m.put("edge", this.edge);
		m.put("minElapsedTime", this.minElapsedTime);
		return m;
	}
}
