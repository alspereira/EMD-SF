package emdsf.EventBased.Disaggregation.EventDetection.Detector.ExpertHeuristics;

import java.util.ArrayList;
import java.util.List;

import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.APowerSampleDTO;
import emdsf.Core.PowerComputation.DTO.IPowerSample;
import emdsf.Core.PowerComputation.DTO.PowerMetricType;
import emdsf.EventBased.Disaggregation.EventDetection.Detector.APowerEventDetector;

/*
 * current RMS
 * window size (default: 4 seconds)
 * event threshold (default: 75% of smallest appliance)
 * previous event must not have occurred in the last three seconds
 * 
 * */

/**
 * 
 * @author lucaspereira
 * 
 * in: array blocking queue with power samples
 *
 */
public class ED_Meehan2014_4 extends APowerEventDetector implements Runnable {
	
	private int		samplingFrequency;		
	private int		detectionWindowSizeBefore; 	// default is 4 seconds
	private int 	detectionWindowSizeAfter;	// default is 4 seconds
	private int		averageWindowSizeBefore;	// default is 1 second
	private int		averageWindowSizeAfter;		// default is 1 second	
	private int 	minElapsedTime;			// default is 3 seconds
	private float 	thresholdValue; 		// default is 0.75 of the appliance with less power consumption
	private int 	beforeEventWindowSize; 	// default is 3
	private int 	afterEventWindowSize;   // default is 3 
	
	private ControlledArrayBlockingQueue<APowerSampleDTO> powerSamples_IN;
		
	private List<APowerSampleDTO> buffer;
	
	public volatile boolean mustQuit = false;
	
	/**
	 * @param windowSize
	 * @param thresholdPercent
	 * @param smallestAppliancePower
	 */
	public ED_Meehan2014_4(int samplingFrequency, int detectionWindowSizeBefore, int detectionWindowSizeAfter, int averageWindowSizeBefore, 
			int averageWindowSizeAfter, int minElapsedTime, int beforeEventWindowSize, int afterEventWindowSize, float thresholdValue) {
		this.samplingFrequency			= samplingFrequency;
		this.detectionWindowSizeBefore	= samplingFrequency * detectionWindowSizeBefore;
		this.detectionWindowSizeAfter	= samplingFrequency * detectionWindowSizeAfter;
		this.averageWindowSizeBefore	= samplingFrequency * averageWindowSizeBefore;
		this.averageWindowSizeAfter		= samplingFrequency * averageWindowSizeAfter;
		this.minElapsedTime 			= samplingFrequency * minElapsedTime;
		this.beforeEventWindowSize		= samplingFrequency * beforeEventWindowSize;
		this.afterEventWindowSize		= samplingFrequency * afterEventWindowSize;
		this.thresholdValue				= thresholdValue;
		// initialize the samplesBuffer with enough samples to be able to return a nice window after the event
		// this array will be passed to the event handling function
		buffer = new ArrayList<APowerSampleDTO>(this.beforeEventWindowSize + this.detectionWindowSizeBefore + this.samplingFrequency + 
				this.detectionWindowSizeAfter + this.afterEventWindowSize);
	}
	
	/*private void loadBuffer() {
		APowerSampleDTO powerSample;
		for(int i = 0; i < buffer.size() && mustQuit == false; i++) {
			powerSample = powerSamples_IN.controlledPoll();
			if(powerSample != null)
				buffer.add(powerSample);
		}
	}*/
	
	/*// removes the first and adds a new one
	private void updateBuffer() {	
		this.buffer.remove(0); // check for index out of bounds
		
		APowerSampleDTO powerSample;
		powerSample = powerSamples_IN.controlledPoll();
		if(powerSample != null)
			buffer.add(powerSample);
	}*/
	
	public APowerSampleDTO[] getCurrentSampleWindow() {
		APowerSampleDTO[] csw 	= new APowerSampleDTO[ averageWindowSizeAfter ];
		int fromIndex 			= beforeEventWindowSize + detectionWindowSizeBefore;
		int toIndex 			= beforeEventWindowSize + this.detectionWindowSizeBefore + averageWindowSizeAfter;
		return buffer.subList(fromIndex, toIndex).toArray(csw);
	}
	
	public APowerSampleDTO[] getPreviousSampleWindow() {
		APowerSampleDTO[] psw 	= new APowerSampleDTO[ averageWindowSizeBefore ];
		int fromIndex			= beforeEventWindowSize - averageWindowSizeBefore;
		int toIndex				= beforeEventWindowSize;
		return buffer.subList(fromIndex, toIndex).toArray(psw);
	}
	
	private boolean canQuit() {
		boolean canQuit = true;
		int minSize 	= beforeEventWindowSize + detectionWindowSizeBefore + samplingFrequency + detectionWindowSizeAfter + afterEventWindowSize; 
		// If there are still samples in the main queue or there are still enough samples in the buffer to proceed the tread cannot quit
		if(this.powerSamples_IN.size() > 0 || this.buffer.size() >= minSize )
			canQuit = false;
		
		return canQuit;
	}
	
	@Override
	public void run() {
		float 	beforeAverage 			= 0;
		float 	currentAverage 			= 0;
		long	lastEventTimestamp 		= 0l;
		long	currentSampleTimestamp 	= 0l;
		
		APowerSampleDTO[] psw;
		APowerSampleDTO[] csw;
		
		
		// load the buffer
		this.loadBuffer();
		
		while(mustQuit == false || canQuit() == false) {
			
			psw = getPreviousSampleWindow();
			csw = getCurrentSampleWindow();
			beforeAverage 	= calculateAverage(psw,PowerMetricType.I_RMS);			
			currentAverage 	= calculateAverage(csw,PowerMetricType.I_RMS);
					
			currentSampleTimestamp = csw[0].getUnixTimestamp();
			
			if(Math.abs(currentAverage - beforeAverage)  >= this.thresholdValue) {
				if(currentSampleTimestamp - lastEventTimestamp >= this.minElapsedTime) {
					System.out.println("Event found at: " + csw[0].getUnixTimestamp());
					lastEventTimestamp = currentSampleTimestamp;
					this.dispatachPowerEvent((IPowerSample[]) buffer.toArray(), beforeEventWindowSize + detectionWindowSizeBefore, 
							0f, null);
				}
				else {
					System.out.println("And event was found at : " + csw[0].getUnixTimestamp() + 
							" with a time difference of :" + (currentSampleTimestamp - lastEventTimestamp) );
				}
			}
			
			updateBuffer();
		}
		System.out.println("quiting this class: " + this.getClass().toString());
	}

	
}
