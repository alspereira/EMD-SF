package emdsf.EventBased.Disaggregation.EventDetection;

import emdsf.EventBased.Disaggregation.EventDetection.Detector.Probabilistic.detectionStatistics.ADetectionStatistics;

public class DS_Builder {	
	private DS_Builder(Builder builder) {
		
	}
	
	public static class Builder {
		//private int samplesBeforeEvent;
		//private int samplesAfterEvent;
		private int preEventWindowSize;
		private int postEventWindowSize;
		
		private float threshold;
		
		public Builder(int preEventWindowSize, int postEventWindowSize, float threshold) {
			this.preEventWindowSize 	= preEventWindowSize;
			this.postEventWindowSize 	= postEventWindowSize;
			this.threshold 				= threshold;
		}
		
		public ADetectionStatistics build() {
			return null; 
			//new DS_Builder(this);
		}
		
	}
}
