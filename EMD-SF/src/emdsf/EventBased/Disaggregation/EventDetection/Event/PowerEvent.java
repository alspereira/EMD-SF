package emdsf.EventBased.Disaggregation.EventDetection.Event;

import java.util.EventObject;
import java.util.Map;

import emdsf.Core.PowerComputation.DTO.IPowerSample;

public class PowerEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8941937957009803836L;
	
	private IPowerSample[] powerSamples;
	private int index;
	private float ds;
	private Map<String, Object> eventData;

	public PowerEvent(Object source, IPowerSample[] powerSamples, int index) {
		super(source);
		this.powerSamples = powerSamples;
		this.index = index;
	}
	
	public PowerEvent(Object source, IPowerSample[] powerSamples, int index, float ds) {
		super(source);
		this.powerSamples = powerSamples;
		this.index = index;
		this.ds = ds;
	}
	
	public PowerEvent(Object source, IPowerSample[] powerSamples, Map<String, Object> eventData) {
		super(source);
		this.powerSamples = powerSamples;
		this.eventData = eventData;
		this.index = (int) eventData.get("edge");
	}
	
	public Map<String, Object> getEventData() {
		return this.eventData;
	}
	
	public IPowerSample[] getPowerSamples() {
		return this.powerSamples;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public float getDs() {
		return this.ds;
	}
	
	public long getPosition() {
		return this.powerSamples[this.index].getID();
	}
}