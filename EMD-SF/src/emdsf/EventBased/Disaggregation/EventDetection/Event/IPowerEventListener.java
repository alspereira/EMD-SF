package emdsf.EventBased.Disaggregation.EventDetection.Event;

public interface IPowerEventListener {
	public void onEventDetected(PowerEvent event);
}
