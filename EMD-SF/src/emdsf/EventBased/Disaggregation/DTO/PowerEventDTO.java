package emdsf.EventBased.Disaggregation.DTO;

import emdsf.Core.PowerComputation.DTO.IPowerSample;

public class PowerEventDTO implements IPowerEvent {
	
	private IPowerSample[] powerSamples;

	public PowerEventDTO(IPowerSample[] powerSamples) {
		this.powerSamples = powerSamples;
	}

	@Override
	public long getPosition() {
		return 0;
	}

	@Override
	public long getId() {
		// TODO Auto-generated method stub
		return 0;
	}
}