package emdsf.EventBased.Disaggregation.DTO;

public interface IPowerEvent {
	public long getPosition();
	public long getId();
}
