package emdsf.EventBased.Disaggregation.DTO;

public class DBPowerEventDTO implements IPowerEvent {
	
	private long position;
	private long id;

	public DBPowerEventDTO(long id, long position) {
		this.id = id;
		this.position = position;
	}

	@Override
	public long getPosition() {
		return position;
	}
	
	public long getId() {
		return id;
	}

}
