/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaEventing.interfaces;

/**
 *
 * @author magnus
 */
public interface EventExceptionHandler {
    public void handleException(Exception ex);
}
