/**
 * 
 */
package base;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author lucaspereira
 *
 */
public class ControlledArrayBlockingQueue<E> extends ArrayBlockingQueue<E> {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1368633769105260276L;
	
	public volatile boolean mustQuit = false; 

	public ControlledArrayBlockingQueue(int capacity) {
		super(capacity);
	}
	
	public void quit() {
		System.out.println("quiting this queue: " + this.getClass().getName());
		this.mustQuit = true;
	}
	
	public E controlledPoll() {
		//System.out.println("in");
		E e = null;
		try {
			e = this.poll(1000, TimeUnit.MILLISECONDS);
			if (e == null) {
				while(mustQuit == false) {
					e = this.poll(1000, TimeUnit.MILLISECONDS);
					if(e != null)
						break;
				}
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		return e;
	}
}
