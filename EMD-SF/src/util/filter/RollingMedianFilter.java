package util.filter;

import java.util.Arrays;
import java.util.Comparator;
import base.ControlledArrayBlockingQueue;
import emdsf.Core.PowerComputation.DTO.IPowerSample;

public class RollingMedianFilter implements Runnable {
	
	public ControlledArrayBlockingQueue<IPowerSample> rawSamples;
	public ControlledArrayBlockingQueue<IPowerSample> filteredSamples;
	
	private IPowerSample[] medianWindow;
	
	private int windowSize;
	
	public volatile boolean mustQuit = false;
	
	public RollingMedianFilter(int windowSize) {
		this.windowSize = windowSize;
		this.medianWindow = new IPowerSample[windowSize];
		this.filteredSamples = new ControlledArrayBlockingQueue<IPowerSample>(rawSamples.size());
	}
	
	public RollingMedianFilter(int windowSize, ControlledArrayBlockingQueue<IPowerSample> rawSamples) {
		this.windowSize = windowSize;
		this.rawSamples = rawSamples;
		this.medianWindow = new IPowerSample[windowSize];
		this.filteredSamples = new ControlledArrayBlockingQueue<IPowerSample>(5000);
	}
	private void updateMedianWindow() {
		IPowerSample[] aux = new IPowerSample[windowSize];
		
		for(int i = 1; i < windowSize; i ++)
			aux[i - 1] = medianWindow[i];
		
		try {
			aux[windowSize - 1] = rawSamples.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		medianWindow = aux;
	}

	private IPowerSample getMedian(IPowerSample[] data) {
		Arrays.sort(data, new Comp());
		
		return data[(data.length - 1) / 2];
	}
	
	public long id = 1l;
	

	@Override
	public void run() {
		
		for(int i = 0; i < windowSize; i ++) {
			try {
				medianWindow[i] = rawSamples.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(int i = 0; i < (windowSize - 1) / 2; i ++) {
			try {
				filteredSamples.put(medianWindow[i]);
				id++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		while( mustQuit == false ) {
			
			try {
				filteredSamples.put(getMedian(medianWindow.clone()));
				updateMedianWindow();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		this.rawSamples.mustQuit = true;
		System.out.println("gracefully quiting " + this.getClass().getName());
		
		
	}
	
	class Comp implements Comparator<Object> {
		@Override
		public int compare(Object arg0, Object arg1) {
			if(((IPowerSample)arg0).getRealPower() == ((IPowerSample)arg1).getRealPower())
				return 0;
			else if(((IPowerSample)arg0).getRealPower() > ((IPowerSample)arg1).getRealPower())
				return 1;
			else
				return -1;
		}
	}

}
