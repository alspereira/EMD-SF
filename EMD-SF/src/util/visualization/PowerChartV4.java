/**
 * 
 */
package util.visualization;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.ArrayBlockingQueue;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.gui.chart.axis.AxisLinear;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;

import javax.swing.JFrame;
import javax.swing.JLabel;

import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;



/**
 * @author lucaspereira
 *
 */
public class PowerChartV4 extends JFrame implements Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int chartSize;
	private int installation_id;
	private Chart2D pChart = new Chart2D();
	//private Chart2D hPChart = new Chart2D();
	//private Chart2D hQChart = new Chart2D();
	
	private ITrace2D pTrace;
	private ITrace2D qTrace;
	private ITrace2D iRMSTrace;
	private ITrace2D vRMSTrace;
	private ITrace2D sTrace;
	
	private ITrace2D pfTrace;
	
	@SuppressWarnings("unused")
	private ITrace2D[] pHarmonics;
	@SuppressWarnings("unused")
	private ITrace2D[] qHarmonics;

	
	AAxis yAxisCurrent;
	AAxis yAxisPower;
	
/*	private Color[] colors = {Color.RED, Color.BLUE, Color.GREEN, Color.CYAN, Color.MAGENTA, Color.ORANGE,
			Color.BLACK, Color.YELLOW, Color.PINK};*/

	
	public ArrayBlockingQueue<DAQSampleDTO<float[]>> samplesQueue;
	
	private void init() {
		pTrace = new Trace2DLtd(chartSize);
    	qTrace = new Trace2DLtd(chartSize);
    	iRMSTrace = new Trace2DLtd(chartSize);
    	vRMSTrace = new Trace2DLtd(chartSize);
    	sTrace = new Trace2DLtd(chartSize);
    	pfTrace = new Trace2DLtd(chartSize);
        
    	yAxisCurrent = new AxisLinear();
    	yAxisCurrent.getAxisTitle().setTitleColor(Color.GREEN);
    	yAxisCurrent.getAxisTitle().setTitle("Current (A)");
    	yAxisPower = new AxisLinear();
    	yAxisPower.getAxisTitle().setTitle("Power (W ! VAR ! VA) | Voltage (V)");
    	 
    	pChart.setAxisYLeft(yAxisPower);
        pChart.setAxisYRight(yAxisCurrent);
        
        
        AAxis xAxis = new AxisLinear();
        
        //xAxis.setFormatter(new LabelFormatterDate(new SimpleDateFormat("dd-MM-y  HH:mm:ss")));
        
        pChart.setAxisXBottom(xAxis);
        //xAxis.getAxisTitle().setTitle("Samples");
        
		yAxisCurrent.setRangePolicy(new RangePolicyMinimumViewport(
                new Range(-1, 2)));
		yAxisPower.setRangePolicy(new RangePolicyMinimumViewport(
                new Range(-1000, 2000)));
		

		pChart.addTrace(pTrace, xAxis, yAxisPower);
		pChart.addTrace(qTrace, xAxis, yAxisPower);

		qTrace.setColor(Color.RED);

		pTrace.setColor(Color.blue);

		
		pTrace.setName("Real Power (W)");
		qTrace.setName("Reactive Power (VAR)");


		
		this.getContentPane().add(new JLabel("Fundamental Frequency Powers"));
		this.getContentPane().add(pChart);

		this.setSize(400,300);
		this.setVisible(true);
	}
	
	public PowerChartV4(int maxChartSamples) {
        this.addWindowListener(
                new WindowAdapter(){
                    @Override
					public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
            );
        this.chartSize = maxChartSamples;
        
        init();
	}
	
	public PowerChartV4(int maxChartSamples, int installation_id, String door) {
        this.addWindowListener(
                new WindowAdapter(){
                    @Override
					public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
            );
        this.chartSize = maxChartSamples;
        this.installation_id = installation_id;
        this.setTitle("DB ID: " + installation_id + " Porta: " + door);
        init();
        
	}
	
	public void start() {
		Thread s = new Thread(this);
		s.start();
	}
	

	@Override
	public void run() {
		int step = 0;
		DAQSampleDTO<float[]> ps;
		
		while(true) {
			try {
				ps = samplesQueue.take();
				//System.out.println(ps.getSample().length);

				pTrace.addPoint(step, ps.getSample()[0]*20266.113);

				qTrace.addPoint(step, ps.getSample()[1]*20266.113);
				
				
				step ++;
				//System.out.println("step: " + step);
			} catch (InterruptedException e1) {
					e1.printStackTrace();
			}
		}
	}
}
