/**
 * 
 */
package util.visualization;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.ArrayBlockingQueue;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.ITracePainter;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.gui.chart.axis.AxisLinear;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.gui.chart.traces.painters.TracePainterDisc;
import info.monitorenter.util.Range;

import javax.swing.JFrame;

/**
 * @author lucaspereira
 *
 */
public class DetectorChart extends JFrame implements Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Chart2D pChart = new Chart2D();
	private ITrace2D pTrace;
	private ITrace2D lTrace;
	private ITracePainter tpC = new TracePainterDisc(4);
	//private ITracePainter tpV = new TracePainterDisc(4);
	
	public ArrayBlockingQueue<Double> detectionStatistics;
	public ArrayBlockingQueue<Double> powerMetric;
	
	AAxis yAxisDetectionStatistics;
	AAxis yAxisPowerMetric;
	
	public DetectorChart(int max) {
        this.addWindowListener(
                new WindowAdapter(){
                    @Override
					public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
            );
        
        detectionStatistics = new ArrayBlockingQueue<Double>(5000);
        powerMetric = new ArrayBlockingQueue<Double>(5000);
        lTrace = new Trace2DLtd(max);
        pTrace = new Trace2DLtd(max);
       
    	yAxisDetectionStatistics = new AxisLinear();
    	//lTrace.setTracePainter(tpC);
    	//pTrace.setTracePainter(tpV);

    	yAxisPowerMetric = new AxisLinear();
        pChart.setAxisYRight(yAxisDetectionStatistics);
        pChart.setAxisYLeft(yAxisPowerMetric);
        AAxis xAxis = new AxisLinear();
        pChart.setAxisXBottom(xAxis);

		
		pChart.addTrace(pTrace, xAxis, yAxisPowerMetric);
		pChart.addTrace(lTrace, xAxis, yAxisDetectionStatistics);
		
		yAxisDetectionStatistics.setRangePolicy(new RangePolicyMinimumViewport(
                new Range(-5, 5)));
		yAxisPowerMetric.setRangePolicy(new RangePolicyMinimumViewport(
                new Range(-200, 1000)));

		lTrace.setColor(Color.RED);
		pTrace.setColor(Color.BLUE);
		lTrace.setName("Detection statistics");
		pTrace.setName("Real Power (W)");
		this.getContentPane().add(pChart);

		this.setSize(400,300);
		this.setVisible(true);
	}

	public void start() {
		Thread s = new Thread(this);
		s.start();
	}


	@Override
	public void run() {
		double p;
		double l;
		int step = 0;
		while(true) {
			try {
				l = detectionStatistics.take();
				p = powerMetric.take();
				
				lTrace.addPoint(step, l);
				pTrace.addPoint(step, p);
				

				step ++;
				
				if(step % 11700 == 0)
					Thread.sleep(10000);
				
				
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
		}	
	}
}
