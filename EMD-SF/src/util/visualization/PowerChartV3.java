/**
 * 
 */
package util.visualization;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.ArrayBlockingQueue;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.gui.chart.axis.AxisLinear;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;

import javax.swing.JFrame;
import javax.swing.JLabel;

import emdsf.Core.PowerComputation.DTO.IPowerSample;



/**
 * @author lucaspereira
 *
 */
public class PowerChartV3 extends JFrame implements Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int chartSize;
	private int installation_id;
	private Chart2D pChart = new Chart2D();
	//private Chart2D hPChart = new Chart2D();
	//private Chart2D hQChart = new Chart2D();
	
	private ITrace2D pTrace;
	private ITrace2D qTrace;
	private ITrace2D iRMSTrace;
	private ITrace2D vRMSTrace;
	private ITrace2D sTrace;
	
	private ITrace2D pfTrace;
	
	@SuppressWarnings("unused")
	private ITrace2D[] pHarmonics;
	@SuppressWarnings("unused")
	private ITrace2D[] qHarmonics;

	
	AAxis yAxisCurrent;
	AAxis yAxisPower;
	
/*	private Color[] colors = {Color.RED, Color.BLUE, Color.GREEN, Color.CYAN, Color.MAGENTA, Color.ORANGE,
			Color.BLACK, Color.YELLOW, Color.PINK};*/

	
	public ArrayBlockingQueue<IPowerSample> samplesQueue;
	
	private void init() {
		pTrace = new Trace2DLtd(chartSize);
    	qTrace = new Trace2DLtd(chartSize);
    	iRMSTrace = new Trace2DLtd(chartSize);
    	vRMSTrace = new Trace2DLtd(chartSize);
    	sTrace = new Trace2DLtd(chartSize);
    	pfTrace = new Trace2DLtd(chartSize);
        
    	yAxisCurrent = new AxisLinear();
    	yAxisCurrent.getAxisTitle().setTitleColor(Color.GREEN);
    	yAxisCurrent.getAxisTitle().setTitle("Current (A)");
    	yAxisPower = new AxisLinear();
    	yAxisPower.getAxisTitle().setTitle("Power (W ! VAR ! VA) | Voltage (V)");
    	 
    	pChart.setAxisYLeft(yAxisPower);
        pChart.setAxisYRight(yAxisCurrent);
        
        
        AAxis xAxis = new AxisLinear();
        
        //xAxis.setFormatter(new LabelFormatterDate(new SimpleDateFormat("dd-MM-y  HH:mm:ss")));
        
        pChart.setAxisXBottom(xAxis);
        //xAxis.getAxisTitle().setTitle("Samples");
        
		yAxisCurrent.setRangePolicy(new RangePolicyMinimumViewport(
                new Range(-1, 2)));
		yAxisPower.setRangePolicy(new RangePolicyMinimumViewport(
                new Range(-1000, 2000)));
		
        //for(int i = 0; i < maxHarmonics; i++) {
        	//hPChart.addTrace(pHarmonics[i]);
        	//hQChart.addTrace(qHarmonics[i]);
        //}
		
		pChart.addTrace(pTrace, xAxis, yAxisPower);
		pChart.addTrace(qTrace, xAxis, yAxisPower);
		
		//pChart.addTrace(iRMSTrace, xAxis, yAxisCurrent);
		//pChart.addTrace(vRMSTrace,xAxis, yAxisPower);
	
		//pChart.addTrace(sTrace, xAxis, yAxisPower);
		
		//pChart.addTrace(pfTrace, xAxis, yAxisCurrent);
		
		qTrace.setColor(Color.RED);
		//iRMSTrace.setColor(Color.GREEN);
		pTrace.setColor(Color.blue);
		//vRMSTrace.setColor(Color.MAGENTA);
		//sTrace.setColor(Color.CYAN);
		//pfTrace.setColor(Color.PINK);
		
		pTrace.setName("Real Power (W)");
		qTrace.setName("Reactive Power (VAR)");
		//iRMSTrace.setName("Current (A)");
		//vRMSTrace.setName("Voltage (V)");
		//sTrace.setName("Aparent Power (VA)");
		//pfTrace.setName("Power Factor");
		
		//this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		
		this.getContentPane().add(new JLabel("Fundamental Frequency Powers"));
		this.getContentPane().add(pChart);
		//this.getContentPane().add(new JLabel("Harmonic Powers - Real"));
		//this.getContentPane().add(hPChart);
		//this.getContentPane().add(new JLabel("Harmonic Powers - Reactive"));
		//this.getContentPane().add(hQChart);

		this.setSize(400,300);
		this.setVisible(true);
	}
	
	public PowerChartV3(int maxChartSamples) {
        this.addWindowListener(
                new WindowAdapter(){
                    @Override
					public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
            );
        this.chartSize = maxChartSamples;
        
        init();
	}
	
	public PowerChartV3(int maxChartSamples, int installation_id, String door) {
        this.addWindowListener(
                new WindowAdapter(){
                    @Override
					public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
            );
        this.chartSize = maxChartSamples;
        this.installation_id = installation_id;
        this.setTitle("DB ID: " + installation_id + " Porta: " + door);
        init();
        
	}
	
	public ArrayBlockingQueue<emdsf.Core.PowerComputation.DTO.IPowerSample> getPowerSamplesQueue() {
		if(this.samplesQueue == null)
			this.samplesQueue = new ArrayBlockingQueue<emdsf.Core.PowerComputation.DTO.IPowerSample>(50000);
		
		return this.samplesQueue;
	}
	
	public void start() {
		Thread s = new Thread(this);
		s.start();
	}
	

	@Override
	public void run() {
		int step = 0;
		IPowerSample ps;
		//System.out.println("start power chart");
		while(true) {
			try {
				//System.out.println("take sample");
				ps = samplesQueue.take();
				//System.out.println("start paint");
				//pTrace.addPoint(ps.getTimestamp(), ps.getRealPower());
				pTrace.addPoint(step, ps.getRealPower());
				//System.out.println(ps.getTimestamp());
				//qTrace.addPoint(ps.getTimestamp(), ps.getReactivePower());
				qTrace.addPoint(step, ps.getReactivePower());
				//iRMSTrace.addPoint(ps.getTimestamp(), ps.getIRMS());
				//vRMSTrace.addPoint(ps.getTimestamp(), ps.getVRMS());
				
				//if(ps.getRealPower() < 0) 
				//	System.err.println("shit is happening " + ps.getID() +  " " + ps.getRealPower());
				//sTrace.addPoint(ps.getTimestamp(), ps.getApparentPower());
				//sTrace.addPoint(step, 0);
				//pfTrace.addPoint(step, ps.getRealPowerA()/(ps.getVRMS()*ps.getIRMS()));
				//pfTrace.addPoint(ps.getTimestamp(), ps.getPowerFactor());
				
				//for(int i = 0; i < pHarmonics.length; i++) {
				//	pHarmonics[i].addPoint(step, ((RawPowerSample)ps).getHarmonicPower(i).realPower);
				//	qHarmonics[i].addPoint(step, ((RawPowerSample)ps).getHarmonicPower(i).reactivePower);
				//}
				
				step ++;
				//System.out.println("step: " + step);
			} catch (InterruptedException e1) {
					e1.printStackTrace();
			}
		}
	}
}
