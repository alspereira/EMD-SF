package util.database.jdbc;

import java.sql.SQLException;

public class MySQLJDBCPooledConnectionDriver extends JDBCPooledConnectionDriver {
	
	private static MySQLJDBCPooledConnectionDriver INSTANCE = null;
	
	private static final String driver = "com.mysql.jdbc.Driver";
	private static final String url  = "jdbc:mysql://localhost/SustDataED_LQ";
	private static final String user = "lucas";
	private static final String password = "1243";
	
	public synchronized static MySQLJDBCPooledConnectionDriver getInstance(String url) throws InstantiationException {
		if(INSTANCE == null) {
			try {
				INSTANCE = new MySQLJDBCPooledConnectionDriver(url);
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | SQLException e) {
				e.printStackTrace();
				throw new InstantiationException();
			}
		}
		return INSTANCE;
	}
	
	public synchronized static MySQLJDBCPooledConnectionDriver getInstance() throws InstantiationException {
		if(INSTANCE == null) {
			try {
				INSTANCE = new MySQLJDBCPooledConnectionDriver();
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | SQLException e) {
				e.printStackTrace();
				throw new InstantiationException();
			}
		}
		return INSTANCE;
	}

	private MySQLJDBCPooledConnectionDriver(String url) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		super(driver, url, user, password);
	}
	
	private MySQLJDBCPooledConnectionDriver() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		super(driver, url, user, password);
	}

}
