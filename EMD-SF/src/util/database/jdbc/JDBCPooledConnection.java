package util.database.jdbc;

import java.sql.*;

public class JDBCPooledConnection {

    private JDBCConnectionPool pool;
    private Connection conn;
    private boolean inuse;
    private long timestamp;

    public JDBCPooledConnection(Connection conn, JDBCConnectionPool pool) {
        this.conn=conn;
        this.pool=pool;
        this.inuse=false;
        this.timestamp=0;
    }

    public synchronized boolean lease() {
       if(inuse)  {
           return false;
       } else {
          inuse=true;
          timestamp=System.currentTimeMillis();
          return true;
       }
    }
    
    public boolean validate() {
    	try {
            conn.getMetaData();
        }catch (Exception e) {
        	return false;
        }
    	return true;
    }

    public boolean inUse() {
        return inuse;
    }

    public long getLastUse() {
        return timestamp;
    }

    public void returnConnection() {
        pool.returnConnection(this);
    }
    
    public Connection getConnection() {
        return conn;
    }

    protected void expireLease() {
        inuse = false;
    }
}
