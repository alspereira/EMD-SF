package util.database.jdbc.dao;

import java.sql.SQLException;

import util.database.jdbc.JDBCPooledConnection;
import util.database.jdbc.MySQLJDBCPooledConnectionDriver;

public abstract class AbstractDAO {
	protected JDBCPooledConnection pooledConn = null;
	protected int autoIncKeyFromAPI = -1;
	
	protected void connect(String url) throws SQLException, InstantiationException {
		pooledConn = MySQLJDBCPooledConnectionDriver.getInstance(url).getPooledConnection();
	}
	
	protected void connect() throws SQLException, InstantiationException {
		pooledConn = MySQLJDBCPooledConnectionDriver.getInstance().getPooledConnection();
	}
}
