package util.database.jdbc.dao.evaluation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import util.database.dto.DetectionEvaluationTestDTO;
import util.database.dto.DetectionPowerEventDTO;
import util.database.dto.ExtendedDetectionPowerEventDTO;
import util.database.jdbc.JDBCPooledConnection;
import util.database.jdbc.MySQLJDBCPooledConnectionDriver;



public class DetectionPowerEventDAO {
	private JDBCPooledConnection pooledConn = null;
	private int autoIncKeyFromAPI = -1;
	
	private static String table_name = "detection_power_event";
	
	private final String insertQuery = "INSERT INTO " + table_name + " VALUES (?, ?, ?, ?, ?)";
	private final String readByEvaluationAndTestIds = "SELECT * FROM " + table_name + " WHERE evaluation_id = ? and test_id = ?";
	private final String countQuery = "SELECT COUNT(*) FROM ( SELECT DISTINCT test_id  FROM " + table_name + " ) as T";
	
	private void connect() throws SQLException, InstantiationException {
		
		pooledConn = MySQLJDBCPooledConnectionDriver.getInstance().getPooledConnection();
	}
	
	public int count(String table_name) {
		ResultSet rs;
		PreparedStatement st;
		int out = 0;
		String countQuery = "SELECT COUNT(*) FROM ( SELECT DISTINCT test_id  FROM " + table_name + " ) as T";
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(countQuery);
			rs = st.executeQuery();
			rs.next();
			
			out = rs.getInt(1);
			//while(rs.next()) {
			//	
			//}
			
			rs.close();
			st.close();
		}
		catch(SQLException | InstantiationException e) {
			e.printStackTrace();
		}		
		finally {
			pooledConn.returnConnection();
		}
		
		return out;
	}
	
	public int create(int evaluation_id, long test_id, long position, String detail) {
		ResultSet rs;
		PreparedStatement st;
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			st.setNull(1, 1);
			st.setInt(2, evaluation_id);
			st.setLong(3, test_id);
			st.setLong(4, position);
			st.setString(5, detail);
			
			if(st.executeUpdate() != 0) {
				rs = st.getGeneratedKeys();
				if(rs.next())
					autoIncKeyFromAPI = rs.getInt(1);
				else
					System.out.println("Not able to get last insert id...");
				
				rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			st.close();
		} catch (SQLException | InstantiationException e) {
			e.printStackTrace();
		} finally {
			pooledConn.returnConnection();
		}
		return autoIncKeyFromAPI;
	}
	
	// load by evaluation and test id
	public List<DetectionPowerEventDTO> readByEvaluationAndTestIds(int evaluation_id, int test_id) {
		ResultSet rs;
		PreparedStatement st;
		List<DetectionPowerEventDTO> out = new ArrayList<DetectionPowerEventDTO>();
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(readByEvaluationAndTestIds);
			st.setInt(1, evaluation_id);
			st.setInt(2, test_id);
			rs = st.executeQuery();
			
			while(rs.next()) {
				out.add(new DetectionPowerEventDTO(rs.getLong(1), rs.getInt(2), 
						rs.getInt(3), rs.getLong(5) , rs.getString(5)));
			}
			
			rs.close();
			st.close();
		}
		catch(SQLException | InstantiationException e) {
			e.printStackTrace();
		}		
		finally {
			pooledConn.returnConnection();
		}
		
		return out;
	}
	
	public List<DetectionPowerEventDTO> readAll(String table_name, int test_id) {
		ResultSet rs;
		PreparedStatement st;
		List<DetectionPowerEventDTO> out = new ArrayList<DetectionPowerEventDTO>();
		
		String q = "SELECT * FROM " + table_name + " WHERE test_id = ?";
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(q);
			st.setInt(1, test_id);
			rs = st.executeQuery();
			
			while(rs.next()) {
				out.add(new DetectionPowerEventDTO(rs.getLong(1), rs.getInt(2), 
						rs.getInt(3), rs.getLong(4) , rs.getString(5)));
			}
			
			rs.close();
			st.close();
		}
		catch(SQLException | InstantiationException e) {
			e.printStackTrace();
		}		
		finally {
			pooledConn.returnConnection();
		}
		
		return out;
	}
	
	public List<DetectionPowerEventDTO> readAllExtended(String table_name, int test_id, String and) {
		ResultSet rs;
		PreparedStatement st;
		List<DetectionPowerEventDTO> out = new ArrayList<DetectionPowerEventDTO>();
		ExtendedDetectionPowerEventDTO edpeDTO;
		
		String q = "SELECT * FROM " + table_name + " WHERE test_id = ? AND " + and + " ORDER BY position ASC";
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(q);
			st.setInt(1, test_id);
			rs = st.executeQuery();
			
			while(rs.next()) {
				edpeDTO = new ExtendedDetectionPowerEventDTO(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getLong(4) , rs.getString(5));
				edpeDTO.delta_111 = rs.getDouble(6);
				edpeDTO.delta_010010010 = rs.getDouble(7);
				edpeDTO.delta_10101 = rs.getDouble(8);
				edpeDTO.delta_101 = rs.getDouble(9);
				edpeDTO.ds = rs.getDouble(10);
				 
				out.add(edpeDTO);
			}
			
			rs.close();
			st.close();
		}
		catch(SQLException | InstantiationException e) {
			e.printStackTrace();
		}		
		finally {
			pooledConn.returnConnection();
		}
		
		return out;
	}
	
	public void update(String table_name, double delta1, double delta2, double delta3, double delta4, double ds, long id) {
		ResultSet rs;
		PreparedStatement st;
		
		String u = "UPDATE " + table_name + " SET delta_111 = ?, delta_010010010 = ?, delta_10101 = ?, delta_101 = ?, ds = ? WHERE id = ?";
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(u);
			st.setDouble(1, delta1);
			st.setDouble(2, delta2);
			st.setDouble(3, delta3);
			st.setDouble(4, delta4);
			st.setDouble(5, ds);
			st.setLong(6, id);
			
			int res = st.executeUpdate();
			
			if(res != 0) {
				//System.out.println(res);
				//rs = st.getGeneratedKeys();
				//if(rs.next())
				//	autoIncKeyFromAPI = rs.getInt(1);
				//else
				//	System.out.println("Not able to get last insert id...");
				
				//rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			//rs.close();
			st.close();
		}
		catch(SQLException | InstantiationException e) {
			e.printStackTrace();
		}		
		finally {
			pooledConn.returnConnection();
		}
		
	}
}
