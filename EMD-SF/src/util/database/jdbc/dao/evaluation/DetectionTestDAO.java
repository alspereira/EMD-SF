package util.database.jdbc.dao.evaluation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import util.database.dto.DetectionEvaluationTestDTO;
import util.database.jdbc.JDBCPooledConnection;
import util.database.jdbc.MySQLJDBCPooledConnectionDriver;

public class DetectionTestDAO {
	private JDBCPooledConnection pooledConn 	= null;
	private int autoIncKeyFromAPI 				= -1;
	
	private static String table_name 			= "detection_test";
	
	private final String insertQuery 			= "INSERT INTO " + table_name + " VALUES (?, ?, ?)";
	private final String readByTestTypeQuery 	= "SELECT * FROM " + table_name + " WHERE type_id = ?";
	
	private void connect() throws SQLException, InstantiationException {
		pooledConn = MySQLJDBCPooledConnectionDriver.getInstance().getPooledConnection();
	}
	
	// should make this with a dto
	public int create(int type_id, String detail) {
		ResultSet rs;
		PreparedStatement st;
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			st.setNull(1, 1);
			st.setInt(2, type_id);
			st.setString(3, detail);
			
			if(st.executeUpdate() != 0) {
				rs = st.getGeneratedKeys();
				if(rs.next())
					autoIncKeyFromAPI = rs.getInt(1);
				else
					System.out.println("Not able to get last insert id...");
				
				rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			st.close();
		} catch (SQLException | InstantiationException e) {
			e.printStackTrace();
		} finally {
			pooledConn.returnConnection();
		}
		return autoIncKeyFromAPI;
	}
	
	public List<DetectionEvaluationTestDTO> readByTestType(int type_id) {
		ResultSet rs;
		PreparedStatement st;
		List<DetectionEvaluationTestDTO> out = new ArrayList<DetectionEvaluationTestDTO>();
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(readByTestTypeQuery);
			st.setInt(1, type_id);
			rs = st.executeQuery();
			
			while(rs.next()) {
				out.add(new DetectionEvaluationTestDTO(rs.getLong(1), rs.getInt(2), rs.getString(3)));
			}
			
			rs.close();
			st.close();
		}
		catch(SQLException | InstantiationException e) {
			e.printStackTrace();
		}		
		finally {
			pooledConn.returnConnection();
		}
		
		return out;
	}
}
