package util.database.jdbc.dao.evaluation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import util.database.jdbc.JDBCPooledConnection;
import util.database.jdbc.MySQLJDBCPooledConnectionDriver;

public class DetectionEvaluationDAO {
	private JDBCPooledConnection pooledConn = null;
	private int autoIncKeyFromAPI = -1;
	
	private static String table_name = "detection_evaluation";
	
	private final String insertQuery = "INSERT INTO " + table_name + " VALUES (?, ?, ?, ?, ?)";
	
	private void connect() throws SQLException, InstantiationException {
		
		pooledConn = MySQLJDBCPooledConnectionDriver.getInstance().getPooledConnection();
	}
	
	// should make this with a dto
	public int create(long creationTimestamp, int algorithm_id, int dataset_id, String detail) {
		//System.out.println("Event at " + pe.installation_id + " delta: " + pe.deltaP);
		ResultSet rs;
		PreparedStatement st;
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			st.setNull(1, 1);
			st.setTimestamp(2, new Timestamp(creationTimestamp));
			st.setInt(3, algorithm_id);
			st.setInt(4, dataset_id);
			st.setString(5, detail);
			
			if(st.executeUpdate() != 0) {
				rs = st.getGeneratedKeys();
				if(rs.next())
					autoIncKeyFromAPI = rs.getInt(1);
				else
					System.out.println("Not able to get last insert id...");
				
				rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			st.close();
		} catch (SQLException | InstantiationException e) {
			e.printStackTrace();
		} finally {
			pooledConn.returnConnection();
		}
		return autoIncKeyFromAPI;
	}	
}
