package util.database.jdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import util.database.jdbc.JDBCPooledConnection;
import util.database.jdbc.MySQLJDBCPooledConnectionDriver;

public class PowerSampleDAO {

	private JDBCPooledConnection pooledConn 	= null;
	private int autoIncKeyFromAPI 				= -1;
	
	private static String table_name 			= "power_sample";
	
	private final String insertQuery 			= "INSERT INTO " + table_name + " VALUES (?, ?, ?, ?, ?)";
	private final String insertQuery_full 		= "INSERT INTO " + table_name + " VALUES (?, ?, ?, ?, ?, ?, ?)";
	private final String insertQuery_full_2 	= "INSERT INTO " + table_name + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private void connect() throws SQLException, InstantiationException {
		pooledConn = MySQLJDBCPooledConnectionDriver.getInstance().getPooledConnection();
	}

	public long create(long unix_timestamp, double p, double q) {
		ResultSet rs;
		PreparedStatement st;
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			st.setNull(1, 1);
			st.setLong(2, unix_timestamp);
			st.setTimestamp(3, new java.sql.Timestamp(unix_timestamp));
			st.setDouble(4, p);
			st.setDouble(5, q);
			
			if(st.executeUpdate() != 0) {
				rs = st.getGeneratedKeys();
				if(rs.next())
					autoIncKeyFromAPI = rs.getInt(1);
				else
					System.out.println("Not able to get last insert id...");
				
				rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			st.close();
		} catch (SQLException | InstantiationException e) {
			e.printStackTrace();
		} finally {
			pooledConn.returnConnection();
		}
		return autoIncKeyFromAPI;
	}
	
	public long create(long unix_timestamp, double p, double q, double v, double i) {
		ResultSet rs;
		PreparedStatement st;
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(insertQuery_full, Statement.RETURN_GENERATED_KEYS);
			st.setNull(1, 1);
			st.setLong(2, unix_timestamp);
			st.setTimestamp(3, new java.sql.Timestamp(unix_timestamp));
			st.setDouble(4, p);
			st.setDouble(5, q);
			st.setDouble(6, v);
			st.setDouble(7, i);
			
			if(st.executeUpdate() != 0) {
				rs = st.getGeneratedKeys();
				if(rs.next())
					autoIncKeyFromAPI = rs.getInt(1);
				else
					System.out.println("Not able to get last insert id...");
				
				rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			st.close();
		} catch (SQLException | InstantiationException e) {
			e.printStackTrace();
		} finally {
			pooledConn.returnConnection();
		}
		return autoIncKeyFromAPI;
	}
	
	public long create(long unix_timestamp, double p, double q, double v, double i, double v_total, double i_total) {
		ResultSet rs;
		PreparedStatement st;
		
		try {
			connect();
			st = pooledConn.getConnection().prepareStatement(insertQuery_full_2, Statement.RETURN_GENERATED_KEYS);
			st.setNull(1, 1);
			st.setLong(2, unix_timestamp);
			st.setTimestamp(3, new java.sql.Timestamp(unix_timestamp));
			st.setDouble(4, p);
			st.setDouble(5, q);
			st.setDouble(6, v);
			st.setDouble(7, i);
			st.setDouble(8, v_total);
			st.setDouble(9, i_total);
			
			if(st.executeUpdate() != 0) {
				rs = st.getGeneratedKeys();
				if(rs.next())
					autoIncKeyFromAPI = rs.getInt(1);
				else
					System.out.println("Not able to get last insert id...");
				
				rs.close();
			}
			else
				System.out.println("Did not insert...");
			
			st.close();
		} catch (SQLException | InstantiationException e) {
			e.printStackTrace();
		} finally {
			pooledConn.returnConnection();
		}
		return autoIncKeyFromAPI;
	}
}
