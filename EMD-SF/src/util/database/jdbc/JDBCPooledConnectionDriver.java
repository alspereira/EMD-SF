package util.database.jdbc;

import java.sql.*;
import java.util.*;
import java.util.logging.Logger;


public class JDBCPooledConnectionDriver implements Driver {

    public static final String URL_PREFIX = "jdbc:jdc:";
    private static final int MAJOR_VERSION = 1;
    private static final int MINOR_VERSION = 0;
    private JDBCConnectionPool pool;

    public JDBCPooledConnectionDriver(String driver, String url, 
                                 String user, String password) 
                            throws ClassNotFoundException, 
                               InstantiationException, IllegalAccessException,
                                SQLException
    {
    	System.out.println("creating a driver");
        DriverManager.registerDriver(this);
        Class.forName(driver).newInstance();
        pool = new JDBCConnectionPool(url, user, password);
        System.out.println("done creating driver");
    }
    
    public int getLeaseCount() {
    	return pool.getLeaseCount();
    }
    
    public JDBCPooledConnection getPooledConnection() throws SQLException {
    	//System.out.println("lease count: " + getLeaseCount());
    	//System.out.println("pool size: " + pool.connections.size());
    	return pool.getConnection();
    }
    
    @Override
    public Connection connect(String url, Properties props) 
                                       throws SQLException {
       if(!url.startsWith(URL_PREFIX)) {
             return null;
        }
        return pool.getConnection().getConnection();
    }
    
    @Override
    public boolean acceptsURL(String url) {
        return url.startsWith(URL_PREFIX);
    }
    
    @Override
    public int getMajorVersion() {
        return MAJOR_VERSION;
    }

    @Override
    public int getMinorVersion() {
        return MINOR_VERSION;
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String str, Properties props) {
        return new DriverPropertyInfo[0];
    }

    @Override
    public boolean jdbcCompliant() {
        return false;
    }

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
}
