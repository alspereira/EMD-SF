package util.database.jdbc;

import java.sql.*;
import java.util.*;

class ConnectionCleaner extends Thread {
	 private JDBCConnectionPool pool;
	 private final long delay = 100;

	ConnectionCleaner(JDBCConnectionPool pool) {
	    this.pool=pool;
	}
	
	public void run() {
	    while(true) {
	       try {
	          sleep(delay);
	       } catch( InterruptedException e) { }
	       pool.cleanConnections();
	    }
	}
}

class ConnectionReaper extends Thread {

    private JDBCConnectionPool pool;
    private final long delay= 100;//300000;

    ConnectionReaper(JDBCConnectionPool pool) {
        this.pool=pool;
    }

    public void run() {
        while(true) {
           try {
              sleep(delay);
           } catch( InterruptedException e) { }
           pool.cleanConnections();
        }
    }
}

public class JDBCConnectionPool {

   public List<JDBCPooledConnection> connections;
   private String url, user, password;
   final private long timeout=60000;
   private ConnectionCleaner cleaner;
   private ConnectionReaper reaper;
   final private int poolsize=1;
   
   private int leased = 0;

   public JDBCConnectionPool(String url, String user, String password) {
      this.url = url;
      this.user = user;
      this.password = password;
      connections = new ArrayList<JDBCPooledConnection>();
      //reaper = new ConnectionReaper(this);
      //cleaner = new ConnectionCleaner(this);
      //reaper.start();
      //cleaner.start();
   }
   
   /*public synchronized void cleanConnections_old() {
	   while(connections.size() > poolsize) {
	    	  try {
	    		  if(!connections.get(poolsize - 1).inUse()) {
	    			connections.get(poolsize - 1).getConnection().close();
	  				connections.remove(poolsize-1); 
	  				System.out.println("cleaned connection: " + connections.size() + " at " +
	  				Calendar.getInstance().getTime().toString());
	    		  }
			} catch (SQLException e) {
				e.printStackTrace();
			}    
	   }
   }*/
   
   public void cleanConnections() {
	   synchronized(connections) {
		   Iterator<JDBCPooledConnection> connlist = connections.iterator();
		   JDBCPooledConnection conn;
		   
		   while((connlist != null) && (connlist.hasNext())) {
			   conn  = (JDBCPooledConnection)connlist.next(); 
			   if(conn.inUse() == false && connections.size() > poolsize) {
				   try {
					
					//System.out.println("will remove a connection");
					connlist.remove();
					//removeConnection(conn);
					//connections.remove(conn);
					conn.getConnection().close();
				   } catch (SQLException e) {
					System.err.println("erro ao limpar a cone�ao");
					//e.printStackTrace();
				   } 
			   }				   
		   }   
	   }
   }

   public synchronized void reapConnections() {
	  long stale = System.currentTimeMillis() - timeout;
	  Iterator<JDBCPooledConnection> connlist = connections.iterator();
	
	  while((connlist != null) && (connlist.hasNext())) {
		  JDBCPooledConnection conn = (JDBCPooledConnection)connlist.next();
		  if((conn.inUse()) && (stale > conn.getLastUse()) && 
		                                    (!conn.validate())) {
			  try {
				  connlist.remove();
				  conn.getConnection().close();
				  //removeConnection(conn);
				  System.out.println("just reaped a connection");
			  } catch (SQLException e) {
				  e.printStackTrace();
			  }
		  }
	  }
   }

   public synchronized void closeConnections() {
        
      Iterator<JDBCPooledConnection> connlist = connections.iterator();

      while((connlist != null) && (connlist.hasNext())) {
          JDBCPooledConnection conn = (JDBCPooledConnection)connlist.next();
          removeConnection(conn);
          connlist.remove();
      }
   }

   private synchronized void removeConnection(JDBCPooledConnection conn) {
       connections.remove(conn);
   }

/*   public synchronized JDBCPooledConnection getConnection() throws SQLException {
       JDBCPooledConnection c;
       for(int i = 0; i < connections.size(); i++) {
           c = (JDBCPooledConnection)connections.get(i);
           if (c.lease()) {
        	  leased ++;
              return c;
           }
       }

       Connection conn = DriverManager.getConnection(url, user, password);
       c = new JDBCPooledConnection(conn, this);
       c.lease();
       leased ++;
       connections.add(c);
       return c;
  }*/
   
   public JDBCPooledConnection getConnection() throws SQLException {
	   synchronized(connections) {
	       JDBCPooledConnection c;
	       for(int i = 0; i < connections.size(); i++) {
	           c = (JDBCPooledConnection)connections.get(i);
	           if (c.lease() && c.validate()) {
	        	  leased ++;
	        	  //System.out.println("using a pooled connection. Size = " + connections.size() );
	              return c;
	           }
	       }
	
	       Connection conn = DriverManager.getConnection(url, user, password);
	       c = new JDBCPooledConnection(conn, this);
	       c.lease();
	       leased ++;
	       connections.add(c);
	       //System.out.println("using a new connection. Size = " + connections.size() );
	       return c;
	   }
  }

   public /*synchronized*/ void returnConnection(JDBCPooledConnection conn) {
      conn.expireLease();
      leased --;
   }
   
   public int getLeaseCount() {
	   return leased;
   }
}
