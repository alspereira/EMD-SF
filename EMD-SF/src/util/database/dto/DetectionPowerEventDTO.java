package util.database.dto;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import emdsf.EventBased.Disaggregation.DTO.IPowerEvent;

public class DetectionPowerEventDTO implements IPowerEvent {
 
	public long id;
	public int evaluation_id;
	public int test_id;
	public long position;
	public String detail;
	
	public DetectionPowerEventDTO(long id, int evaluation_id, int test_id, long position, String detail) {
		this.id = id;
		this.evaluation_id = evaluation_id;
		this.test_id = test_id;
		this.position = position;
		this.detail = detail;	
	}
	
	public JSONObject getDetails() {
		return (JSONObject) JSONValue.parse(this.detail);
	}

	@Override
	public long getPosition() {
		return this.position;
	}

	@Override
	public long getId() {
		return this.id;
	}
}
