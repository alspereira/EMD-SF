package util.database.dto;

public class DetectionEvaluationTestDTO {

	public long id;
	public int type_id;
	public String detail;
	
	public DetectionEvaluationTestDTO(long id, int type_id, String detail) {
		this.id = id;
		this.type_id = type_id;
		this.detail = detail;
	}

}
