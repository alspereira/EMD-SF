package util.database.dto;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class ExtendedDetectionPowerEventDTO extends DetectionPowerEventDTO {
 
	public long id;
	public int evaluation_id;
	public int test_id;
	public long position;
	public String detail;
	
	public double delta_111;
	public double delta_010010010;
	public double delta_10101;
	public double delta_101;
	public double ds;
	
	public ExtendedDetectionPowerEventDTO(long id, int evaluation_id, int test_id, long position, String detail) {
		super(id, evaluation_id, test_id, position, detail);
		this.id = id;
		this.evaluation_id = evaluation_id;
		this.test_id = test_id;
		this.position = position;
		this.detail = detail;	
	}
	
	public JSONObject getDetails() {
		return (JSONObject) JSONValue.parse(this.detail);
	}
	
}
