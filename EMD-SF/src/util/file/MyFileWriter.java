package util.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class MyFileWriter {
	
	private Path path;
	private File file;

	public MyFileWriter(String filePath, String fileName) throws IOException {
		file = new File(filePath + fileName);
		
		
		if(!file.exists())
			file.createNewFile();
		
		path = Paths.get(file.getAbsolutePath());
		
	}
	
	
	public void write(String text) throws IOException {
		synchronized(path) {
			Files.write(path, text.getBytes(), StandardOpenOption.APPEND);
		}
	}
}