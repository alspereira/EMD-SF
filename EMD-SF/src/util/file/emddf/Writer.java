package util.file.emddf;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import base.ControlledArrayBlockingQueue;
import emddf.file.SURFFile;
import emddf.file.SURFFileDescr;
import emdsf.Core.DataAcquisition.DTO.DAQSampleDTO;

public class Writer implements Runnable {
	
	private File	file_OUT;
	private SURFFile	SURF_file_OUT;
	private SURFFileDescr	SURF_descr_OUT;

	private ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> source;
	
	public long unixTimestamp = 0l;
	public long calculatedUnixTimestamp = 0l;
	
	//private long maxFileSizeInSamples;
	private long maxFileSizeInMilliseconds;
	
	public volatile Boolean finished;
	
	private float[] calibrationConstants;
	
	public String path_prefix = "";
	
	
	// file size is samples refers to the number of samples of one channel
	public Writer(ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> source, double sampleRate, int bitsPerSample, int channels,
			long maxFileSizeInSamples) {
		
		this.source 					= source;
		this.maxFileSizeInMilliseconds 	= (long) ((maxFileSizeInSamples / sampleRate) * 1000);
		//this.maxFileSizeInSamples 		= maxFileSizeInSamples;
		this.finished					= false;
		
		SURF_descr_OUT 					= new SURFFileDescr();
		SURF_descr_OUT.rate 			= sampleRate;
		SURF_descr_OUT.bitsPerSample 	= bitsPerSample;
		SURF_descr_OUT.channels			= channels;
		SURF_descr_OUT.type				= SURFFileDescr.TYPE_WAVE;
		if(bitsPerSample <= 16)
			SURF_descr_OUT.sampleFormat = SURFFileDescr.FORMAT_INT;
		else
			SURF_descr_OUT.sampleFormat = SURFFileDescr.FORMAT_FLOAT;
		
		// set the callibration constants that are mandatory! TODO: make this default to 1 in surf descriptor
		
		this.calibrationConstants = new float[channels];
		for(int i = 0; i < channels; i++)
			this.calibrationConstants[i] = 1;
		
		SURF_descr_OUT.SURF_channel_calibration = calibrationConstants;
		SURF_descr_OUT.SURF_sample_rate = (float) sampleRate;
	}
	
	public Writer(ControlledArrayBlockingQueue<DAQSampleDTO<float[]>> source, double sampleRate, int bitsPerSample, int channels,
			long maxFileSizeInSamples, float[] calibrationConstants) {
		this(source, sampleRate, bitsPerSample, channels, maxFileSizeInSamples);
		this.calibrationConstants = calibrationConstants;
	}
	
	// this cannot be set once the system is running
	// need to check this in code
	public void setUnixTimestamp(long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
		this.calculatedUnixTimestamp = unixTimestamp;
	}
	
	private void updateCalculatedUnixTimestamp() {
		calculatedUnixTimestamp = calculatedUnixTimestamp + maxFileSizeInMilliseconds;
	}
	
	private float[][] MultiChannelSample2MultiChannelArray(DAQSampleDTO<float[]> mcs) {
		float[] samples = mcs.getSample();
		float[][] mca = new float[samples.length][1];
		
		for(int i = 0; i < samples.length; i ++)
			mca[i][0] = (float) samples[i] / calibrationConstants[i];
		
		return mca;
	}
	
	@Override
	public void run() {
		int fileCount = 1;
		//long sampleCount = 0;
		DAQSampleDTO<float[]> tempFrames;
		int tempFramesLength;
		//boolean hasFirst = false;
		boolean hasInitialTimestamp = false;
		//long currentTimestamp;
		
		
		while(hasInitialTimestamp == false) {
			tempFrames = source.peek();
			if(finished && source.isEmpty()) // not processing the last sample
				return;
			if(tempFrames != null) {
				unixTimestamp 			= tempFrames.getUnixTimestamp();
				calculatedUnixTimestamp = unixTimestamp + maxFileSizeInMilliseconds;
				hasInitialTimestamp 	= true;
			}
		}
		
		while(finished == false || source.isEmpty() == false) {
			try {
				file_OUT 				= new File(
						path_prefix +
						unixTimestamp + "_" + 
						calculatedUnixTimestamp + "_" + 
						(calculatedUnixTimestamp - unixTimestamp) + "_" + 
						(maxFileSizeInMilliseconds - (calculatedUnixTimestamp - unixTimestamp)) + "_" + fileCount + ".wav");
				
				SURF_descr_OUT.file 	= file_OUT;		
				SURF_file_OUT 			= SURFFile.openAsWrite(SURF_descr_OUT);
				
				do { 
					tempFrames = source.poll(2000, TimeUnit.MILLISECONDS);		
					if(finished && source.isEmpty())
						return;
					if(tempFrames != null) {
						unixTimestamp = tempFrames.getUnixTimestamp();
						tempFramesLength = 1;
						SURF_file_OUT.writeFrames(MultiChannelSample2MultiChannelArray(tempFrames), 0, 
								tempFramesLength);
					}
				} 
				while(unixTimestamp <= calculatedUnixTimestamp);
				
				// closes this file and in the loop another one will be created
				SURF_file_OUT.cleanUp(); 
				// update the calculated unix timestamp
				updateCalculatedUnixTimestamp();
				// increment the number of files created so far
				fileCount ++;
				
			} catch (IOException | InterruptedException e) {
				System.out.println("some issue here");
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
}