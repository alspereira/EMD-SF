package util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import base.ControlledArrayBlockingQueue;

public class PublishSubscribe<T> implements Runnable {
	
	public ControlledArrayBlockingQueue<T> in;
	public List<ControlledArrayBlockingQueue<T>> subscribers;
	
	public volatile boolean canQuit = false;
	
	public PublishSubscribe() {
		this.subscribers 	= new ArrayList<ControlledArrayBlockingQueue<T>>();
	}
	public PublishSubscribe(ControlledArrayBlockingQueue<T> a_source) {
		this();
		this.in			= a_source;
	}
	
	public void subscribe(ControlledArrayBlockingQueue<T> queue) {
		synchronized(subscribers) {
			subscribers.add(queue);
		}
	}
	
	public void unsubscribe(ControlledArrayBlockingQueue<T> queue) {
		synchronized(subscribers) {
			subscribers.remove(queue);
		}
	}

	@Override
	public void run() {
		Iterator<ControlledArrayBlockingQueue<T>> it;
		//System.out.println(in.size());
		while(canQuit == false || in.size() > 0) {
			
			it = subscribers.iterator();
			synchronized(subscribers) {
				T t = in.controlledPoll();
				//if(t.getClass().getSimpleName().equals("PowerSampleDTO"))
				//	System.out.println("inside: " + in.size() + " " + t.getClass().getSimpleName());
				
				if(t != null) {
					while( it.hasNext() )
						try {
							it.next().put(t);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
				}
			}
		}
		// this thread will end, therefore we must inform all the subscribed queues about that
		it = subscribers.iterator();
		synchronized(subscribers) {
			it = subscribers.iterator();	
			while( it.hasNext() ) {
				//it.next().mustQuit = true;
				it.next().quit();
				System.out.println("unsubscribe pub sub queue");
			}
		}
		System.out.println("before quit:" + in.size());
		System.out.println("Gracefuly quiting: " + this.getClass().getSimpleName());
	}
}
